const { src, dest, parallel } = require('gulp');
const include = require('gulp-include');

function html() {
  return src('static/*')
    .pipe(include())
    .pipe(dest('dist'))
}

function dep() {
  return src(['dep/**/*'])
    .pipe(dest('dist/dep'));
}

function assets() {
  return src(['assets/**/*'])
    .pipe(dest('dist'));
}

function rules() {
  return src(['univers/**/*'])
    .pipe(dest('dist'));
}

exports.html = html;
exports.dep  = dep;
exports.assets  = assets;
exports.rules  = rules;
exports.default = parallel(html, dep, assets, rules);
