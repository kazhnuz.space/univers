<!-- docs/_sidebar.md -->

* [Espèces](bases/especes.md "Les espèces de Nouvelles Frontières")

* Histoire globale
  * [Partie 1 - Prémisses](histoire/partie1-premisses.md "Prémisses")
  * [Partie 2 - L'ère de l'Outreterre](histoire/partie2-outreterre.md "L'ère de l'Outreterre")
  * [Partie 3 - Révolutions](histoire/partie3-revolutions.md "Révolutions")
  * [Partie 4 - La confédération solaire](histoire/partie4-confederation.md "La confédération solaire")
