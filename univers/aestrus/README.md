# Æstrus

> Depuis les débuts, nous regardons avec envie les cieux. Notre limite. Nous savons que nous avons été amené dans ce monde, jadis. Mais nos origines restent encore un secret. Nous ne savons pas d'où nous venons, juste que ceci n'est pas notre Terre. Mais là-haut, dans ces îles, se trouvent peut-être toutes les réponses. Le secret de nos origines. Le plus grand des mystères.

Æstrus est un univers de soft science-fiction "furry" lumineux et coloré avec des animaux anthropomorphiques amusant dans un monde principalement technologique avec quelques touches de mystiques, inspiré de franchise telles que Sonic, Starfox et Megaman.

Ce monde est composé d'une planète éponyme ou vive les Aestriens, des animaux anthropomorphiques diverses et variés, utilisant des technologies avancée et généralement plus ou moins lié à l'ignum, un materiel étrange et source d'énergie puissante. Certaines êtres ont même la capacité de l'utiliser directement, tel que l'ancien et redouté clan Skelfing.

Dans les cieux se trouvent un archipel flottant mystérieux et convoité, au fond de la tempête qui ne s'arrête jamais, ou se trouverait les secrets de ce monde.

> Vous avez envie de découvrir tout les secrets de la planète Æstrus ? Regoignez les scientifiques et les aventuriers pour explorer les Cieux.
