# Province d'Alandale

Alandale est une province prospère au nord du continent *d'Anvêbres*. Souvent nommée la Province-Soeur d'Aymon, les relations entre les deux pays ont souvent participé à façonner leur histoire. Cette province à notamment souvent profité des relations commerciales entre Tubleh et les autres pays, passant souvent par la province.

La Province d'Alandale est une province riche, extrèmement riche en Ignum, et aux paysages variées. Dôté à la fois de plateau glacial, de grands espaces océaniques tempérés, et d'un désert, cette province possède de grands atouts.

## Sinbade, capitale d'Alandale

Sindabe est une immense ville portuaire, situé sur les côtes sud-ouest de la province, faisant du commerce à la fois avec Tubleh et les royaumes à l'ouest du continent d'Etherial, et par les voix terrestres avec les parties moins dangereuses du Désert de Zircon et avec le plateau du Kab.

La ville est située au sud-ouest des plateau du Kab, à la limite avec le désert de Zircon, et commerce avec le reste du pays par de grande ligne de voie ferrée traversant le plateau, notamment la citadelle d'Heliodore.

Ville au climat chaud, elle est également un lieu de tourisme important, démontrant la richesse de la culture Alandalienne.

## Plateau de Kab

Les plateau de Kab sont des grands plateau semi-aride froid où peu de chose pousses, formant le territoire de Kab. Ces terres sont surtout connus pour les citadelles qui s'y trouvent, et pour être l'endroit où l'on peut rencontrer de nombreuses créatures dangereuse.

Ces terres représentent le nord-ouest de la province sont peu habitée, à l'exception des côtes ouest. De nombreuses voies ferrées relient les différentes villes et citadelles, notamment à la capitale.

### Citadelle d'Héliodore

La citadelle d'Heliodore est l'une des nombreuses citadelle se trouvant dans le plateau du Kab, format historique des populations de ces terres difficiles. Cependant, celle-ci est la plus grande, s'étant successivement construite sur une petite montagne.

Si elle n'est pas la capitale du pays, sa puissance et son rayonnement en ont toujours fait un lieu important, notamment de par son positionnement stratégique entre la capitale et l'ouest tempéré du pays : tout le commerce entre Sinbade et les territoires de l'est, tout aussi riche, passaient par cette route.

Cette citadelle est notamment occupée par les Jardins d'Heliodore, un lieu important de la philosophie Alandienne, ou s'est déroulé notamment l'*Age d'Or Alandaliens*, une grande époque ou les mathématique, les arts et la philosophie ont prospéré. C'est aussi un grand lieu de la médecine mondiale, et ce depuis des siècles.

Cette Citadelle a resisté à l'Empire Skelfing durant toute la période ou celui-ci avait le contrôle sur la province.

## Désert de Zircon

Ce désert est un immense désert de sable séparant la région d'Alandale avec celle de Tubleh.

Cette région est connue pour une immense activité électromagnétique provoquée par de nombreux alliage d'ignum présent dans le sol, dont l'origine reste encore mystérieuse. Cette grande étendue désertique subit de ce fait des orages secs perpetuels, le rendant très difficilement praticable.

Ce désert est selon la légende là où la protectrice Zircon se serait entraînée à faire tomber les plus grands éclairs, jusqu'à ce qu'ils restent éternels.
