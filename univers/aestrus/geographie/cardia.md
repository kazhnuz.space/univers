# Province de Cardia

La Province de Cardia, anciennement *péninsule des cités-état* est une province puissante et moderne tournant autour de la cité état de Cardia, plus grande et plus puissante cité d'Aestrus. Composé de plusieurs grandes cité-état désormais vassalle de Cardia, cette province est un coeur économique d'Aestrus, et un espace industrialisé et urbain - plus de 90% de la population vis en ville.

Pendant longtemps un espace non-unifié, les cités ont formé une fédération après la fin de l'Empire Skelfing. Cependant, même sous cette forme, les cités états gardent une grande rivalité, et de nombreux conflits - cependant non-militaire aujourd'hui - peut éclater entre elles.

À l'exception de la capitale Cardia, les trois principales cités sont Bellicosa, Volpe et Sepolcro. La province adopte un système fédéral sans capitale, composé de cinq composant : Cardia, Volpe, Sepolcro, Bellicosa et Ilade.

## La cité de Cardia

La cité de Cardia est la plus récente des villes de la régions, ou tout du moins récente sous sa forme actuelle. Ancienne petite ville mineure de la province qui gardait son indépendance en payant des tribus aux autres cités, cette ville a été choisie par le Magister pour devenir sa capitale, afin de ne pas donner une trop grande puissance à l'une des trois villes rivales.

Ville moderne et très dense, elle a eut le droit à une immigration très forte, ce qui lui a permis de se constituer en ville d'importance considérable, avec un dynamisme incroyable. Pour certains ville dynamique, vivante et source d'espoir, pour d'autre il s'agit d'une ville sans aucune histoire, et qui ne sert que les intérêt de Magister.

Cardia reste neutre dans toute les affaires des trois autres grandes villes, ne jouant que sporadiquement le rôle d'arbitre quand cela affecte son propre territoire. Mais hors de cela, elle laisse les autres villes relativements tranquille.

Cette ville possède de nombreux centres de recherches et de travaux sponsorisé par le Magister. C'est ici que se trouve Irae Corporation, ainsi que le quartier générale des Troupes du Magister.

## La cité de Volpe

La république de Volpe fut jadis la plus riche et la plus puissante des cités états de la péninsule. C'était également la cité états possédant le plus de cités vassales, ainsi qu'un empire économique puissant. Cependant, l'Empire Skelfing provoqua le démantèlement de cette puissance, qui reste aujourd'hui toujours un centre financier important, mais sans sa puissance de jadis.

Dirigée par un Capitaine et fortement influencée par l'oligarchie de la ville, Volpe à toujours composé son armée faible par des formidables agences d'espionnages, auquels les autres pays font régulièrement appels. Tout le monde sait qu’ils avaient forcément des espions de Volpe quelque part dans leur territoire, mais personne ne sait qui ils sont.

## La cité de Sepolcro

La sapiocratie de Sepolcro est une ex-cité état, grandement dirigée par une aristocratie lié au niveau d'étude, dirigé par un conseil des sages. La ville décrète la sagesse comme condition la plus importante de l'humain, et l'unique moyen d'atteindre le bonheur. La contemplation philosophique et le débat sont érigée en chose la plus importante.

Le nom de "Sepolcro" désigne une réalité cependant plus sombre : cette ville serait née de la mort. Cette ville est l'une des grandes rivales de Volpe, cherchant notamment à avoir une forte influence culturelle sur le monde entier.

À Sepolcro, le savoir est pouvoir. C'est pourquoi les gardiens de la bibliothèque de Sepolcro sont souvent des serviteurs spéciaux nommé Oikeus, ayant été conditionné mentalement pour ne ressentir ni envie de dépasser autrui, ni envie de violence. Cette ville est l'un des centre de l'art secret de *l'esprit*.

## La cité de Bellicosa

La cité de Bellicosa est une cité état guerrière et fortement militarisée, dirigée par un conseils de militaire venant des deux armées de la ville : les **chevaliers** et la **fantassins**, qui dans Bellicosa ont chacun leur commandement distinct du à une longue tradition. La rivalité entre ces deux corps guerriers a façonnée toute l'histoire idéologique de la ville, qui a toujours hésité entre l'idéal héroïque des chevaliers, ou la protection rationnel des fantassins.

La ville est une ville pétrie d'honneur, où généralement il y a peu de place pour ceux qui ne veulent pas se battre. Tout les membres de la ville doivent participer au combat et savoir manier les armes.

Cité rivale de Sepolcro et Vulpe, cette ville est connue pour être la ville d'origine du clan Skelfing. Redevenu indépendante depuis la fin de l'Empire, elle contient cependant de nombreux nostalgies de cette époque, ou elle jouissait d'une place de choix.

## Archipel d'Ilade

L'Archipel d'Ilade est un ensemble d'ancien territoire conquis par Volpe, qui ont obtenu leur indépendance de la ville mais ont décidé de faire partie de l'union de Cardia lors de la fondation de celle-ci. Faisant désormais jeu égal avec leurs anciens "maitre", leur relation ont beaucoup d'effet sur la géopolitique de la région.

### Ilade

L'ile principale. Si elle est sous contrôle du Peuple de l'Eau, cette île est en vérité hautement multiculturelle. Il s'agit d'une île fortement forestière, avec deux villes principale Ilade et Estia. Cette île est fortement surveillée par la guilde de l'Eau, et est surtout connue pour être l'une des plus grandes forces commerciales avec Volpe.

### Iskaia

Iskaia est sans doute la plus connue des îles d'Ilade, à part peut-être l'île éponyme. Pourquoi est-elle connue ? Parce qu'elle est la plus grande île de la piraterie ! En effet, c'est sur cette île - et l'archipel éponyme, composé en grande partie d'île couvertes de mangrove - que se retrouvent une grande partie des pirate de l'archipel.

### Amëia

Amëia est une petite île balnéaire, connue à la fois pour ses plages et pour son architecture ancienne extrèmement bien conservée.

Elle est la plus grande d'un petit ensemble nommé Lupaïa, un archipel rempli de ruines anciennes. Les plus ancienne sont les *ruines de la louve rouge*, connue pour la "malédiction" qui s'y trouverait.
