# La Province de Dingir

La province de Dingir est une immense province froide et eneigée couvrant le nord du continent d'Etherial.

Cette province est connue pour avoir été le centre terrestre des Seraphim durant l'ère de la première civilisation, et sera l'un des premiers territoires controllé par l'Empire Skelfing. C'est une région qui a fortement perdu en importance durant l'époque actuelle, ce qui est en grande partie la source de la montée de la Hiérocratie dans ces terres.

La province contient de nombreuses cité ancienne, et garde cependant une culture très riche, malgré son manque de considération par les autres provinces.

## Ilu, capitale ancienne et respectée

Ilu est la capitale officielle de la province. Composée de nombreuse couche de ruine, on parle même des "cinq Ilu" pour parler des différentes couches de civilisation que la ville est connue. Il y a en effet deux ville antique, pré et post-Seramphim, "l'ancienne ville", ou la Ilu médiévale, la Ilu Impériale et la Ilu moderne. Ces différentes couches de villes sont en grande partie ce qui fait la richesse de la ville, à la fois culturellement… et économiquement : c'est la ville la plus visité du monde.

La Ilu moderne est en vérité une autre ville qui a été depuis absorbée par Ilu avec l'agrandissement des deux ville, devenu aujourd'hui un quartier moderne et des affaires, avec des gratte-ciel, des stations d'envol pour. Contrairement à Aymon, Dingir ne tente pas d'éviter les constructions trop moderne, n'ayant ce genre de restriction que dans les anciennes-villes.

Les souterrains d'Ilu également illustres, connue pour avoir été un grand lieu du banditisme à une époque. Il est l'un des rares endroit ou un pacte existe entre les Loyaux et les Hydres, n'ayant pas l'autorisation de se battre à cette endroit, ou avec toute autre factions non-autoritaire. Mais même sans cette loi, pour allez combattre quelque chose, il faut réussir à le trouver…

### Bâtiments anciens d'Ilu

Le *Temple de l'Envol* est le dernier temple de ce type existant encore aujourd'hui, et se situe dans la Ilu Séraphim. Ce temple servait aux séraphim à pouvoir rejoindre les cieux célèste. Pillé à de mainte reprise, il ne reste que des traces des mécanismes utilisées, et est fortement étudié pour cela. Ce temple a été vu différemment suivant les dinastie, soit comme un "parjure témoignant notre laideur" par les civilisations critiquant la première grande civilisation, soit comme "les derniers reste de la plus grande civilisation" par ceux pro-Seraphim. Il n'a cependant jamais été détruit par ces premiers, mais plutôt utilisé à des fins de propagandes.

## Bahal

Bahal est une région et une ville éponyme, situé dans les grandes plaines enneigées de la région, ou se trouvent de nombreuses ruines anciennes, qui contrairement aux bâtiments anciens de Ilu sont souvent peu entretenus.

La région est un ensemble de grandes plaines enneigée et secoué par des tempêtes de neiges, c'était un lieu relativement pauvre, composé de petit territoires locaux, sous la protection de la province, jusqu'à ce qu'y démarre la hiérocratie, dans l'ancien Grand-Temple de la ville de Bahal. Cet aujourd'hui un lieu que la province à du mal à contrôler du à la présence du grand-ordre.

La ville de Bahal est la capitale de cette région, et est une ville labyrinthique, aux bâtiments anciens et préservés.

Parmi les autres villes de la région se trouve, Nippur une petite ville fortement sacrée pour les anciens cultes de la région, qui possède la particularité d'être également une ville interdite à la résidence permanente, a part pour des eunuques et serviteurs d'anciennes grandes familles dingiriennes, généralement vendus à la religion afin d'habiter la ville. Elle contient de nombreux temples et constructions secrètes, et ses mystères ne sont véritablement connus que par ses habitants...

Dans cette région, on trouve également le **Labyrinthe des Glace**, un immense dédale composé de roches glacées, construit dans une ancienne carrière. Ce Labyrinthe est creusé directement dans la paroie, et recèle en son sein un secret lié au Protecteur Perle.

## Les montagnes de Thermona

La chaîne de montagne de Thermona est une immense chaîne de montagne qui recouvre à la fois le nord de Dingir, reliant la région à la réagion d'Alandale. Elle recouvre tout le pôle nord d'Aestrus. Recouvert par une immense calotte glacière, cette chaine de montagne est un lieu extrêmement difficile à vivre, où très peu de peuple se trouve. Il descend sous forme de haut plateaux dans les continents d'Anvêbres et d'Etherial, formant un lieu plus vivable, mais ou se trouve en grande majorité des peuples nomades.

Une grande partie est dirigée par Dingir, et dans cette partie se trouve la prison d'Oxyda, très ancienne et entourée de ruine. Elle est toujours en activités aujourd'hui, et est notamment utilisée par la Hiérocratie.

## Ile de Lok

L'île de Lok est une île pauvre et glaciale se trouvant entre les continent d'Anvêbres et Etherial. Elle est le dernier vestige de l'ancien empire Skelfing, un puissant empire né durant l'époque post-classique (de la réunion de trois territoires dirigé par la famille Skelfing), et ayant envahi presque la totalité du monde durant cette ère. Après la guerre. Après avoir été défait, il a réussi à subsisté sur cette île, qui était avant un petit gouvernat de l'Empire.

La capitale de Lok est Fyrst, et sa ville la plus peuplée Lok, cependant la famille impérial n'y vivait pas, vivant plutôt à cause de l'incident de l'assassinat d'Hetjul (évenement ayant conduit progressivement à la chute de l'Empire) dans la Citadelle Skelfing dans les terres gelées de Lok.

Aujourd'hui, l'ancien reste de l'Empire Skelfing n'est plus qu'une dépendance de la Province de Dingir, après la mort du roi Hetjul de la main de la Hiérocratie lors des attaques simultanée de la Hiérocratie et du Magister.
