# La Province d'Aymon

La province d'Aymon (ou dans son nom officiel "Province des Deux-Royaume") est une province présente sur toute la péninsule nord éponyme. Cette province est la fusion de deux anciens royaume, le royaume d'Arete et le royaume d'Aymon, après une guerre entre les deux suite à la dislocation de l'empire Skelfing.

Si officiellement les deux régions de la province sont égales, dans les faits Aymon a pris contrôle d'Arete, et d'autres territoires depuis.

La province d'Aymon à longtemps été l'une des plus puissantes province depuis la chute de l'Empire Skelfing, mais elle a perdu en puissance avec la *révolution de l'Ignum* et la montée en puissance de la province de Cardia, et surtout de sa capitale éponyme.

## Aymon historique

L'Aymon historique, ou "coeur" est la partie la plus importante et riche de la province d'Aymon. Il s'agit d'un ancien royaume, dirigé depuis plusieurs siècles par la famille d'Aymon, en majorité rurale et champêtre qui vit surtout de l’agriculture mais également de quelques mines au nord du pays. Si officiellement, aucun des roi n'a encore de véritable pouvoir ni rôle dans la nouvelle province, la famille conserve le titre de "rois historiques d'Aymon", et garde une importance culturelle.

Aujourd'hui encore, il conserve un caractère très traditionnel, ainsi que des rancoeur ancestrales et complexe entre les différentes villes, et la noblesse y garde une importance considérable. Une grande partie du royaume est encore boisé, mais les technologies se sont fortement installée dans les grandes villes, qui sont désormais proches de celles qu'on peut trouver à Cardia, si ce n'est avec des coeurs traditionnels importants.

Les plus grandes villes du royaume d'Aymon sont Erles (capitale du royaume, ville connue pour ses nombreuses horloges gigantesque), Arthuria l'Antique (une très ancienne cité, plus grande ville du royaume), Linnes (une citadelle ancienne fortifiée, refuge historique des rois d'Aymon lors des invasions, aujourd'hui capitale militaire), la cité portuaire de Maupertuis qui est le plus grand port commercial du royaume, ancienne ville fondée par des navigateurs de Volpe, et la grande ville étudiante de Sonate, connues pour ses nombreuses bibliothèques.

De nombreuse villes de l'époque Ébenienne en ruine y subsistent, tel que l'île d'Ether (située proche des côtes Aretiennes) et la ville de Stylène. Ces ruines sont de grands cites touristiques, et revêtes encore aujourd'hui une importance considérable.

## Région d'Arete

La région d'Arete (toujours aujourd'hui controlé par le royaume d'Aymon) est un territoire froid et sec de l'ouest de la pénisules. Cette région est historiquement plus industrialisée que le royaume d'Aymon de par les débuts d'industrialisation de l'Empire de Skelfing, et de part le fait qu'après l'annexion au royaume d'Aymon le royaume à concentré ses effort industrielle dans sa nouvelle région.

Arete est un territoire dont l'histoire compliquée à laissée au moins trois capitale, la citée ancestrale éponyme d'Arete est l'ancienne capitale datant de l'époque ou Arete était une royaume indépendant en grande partie conseillé par la guilde de Choris, jusqu'à son premier effondrement. Durant la période Skelfing, le duché puis province impériale d'Arete a pris comme capitale la cité de Koiné. Cette ville a été délaissé après l'annexion au profit de la plus grande cité de la région, Banausos, ville nouvelle qui bénéficiait des grands gisement d'ignum du nord d'Arete. Cette histoire fait d'Arete une région multiculturelle.

Dans les terres les plus occidentales d'Arete se trouve de grandes fosses formant la Vallée des Esprit. Dans cette vallée, datant de l'ère de la première civilisation, dans la ville de Sophos, se trouve les ruines de la Tour de la Mémoire.
