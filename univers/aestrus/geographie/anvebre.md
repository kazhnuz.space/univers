# Le sous-continent d'Anvêbres

Le sous-continent d'Anvêbres est un sous continent à l'ouest des plaines d'Io, au climat plutôt chaud, allant du sec (au centre du continent) au tropical (sur les côtes sud). C'est sur ce continent que se trouve les deux principaux centres technologiques d'Æstrus, la capitale du Magister, et l'île d'Enoa, grand centre d'innovation technologiques. Les deux grandes nations du sud d'Anvêbres (Andale et Tubleh) sont indépendante du Magsiter.

## L’Ancien-Royaume de Tubleh

L'Ancien-Royaume de Tubleh est un endroit singulier sur plusieurs aspects. Premier état indépendant d'Aestrus, état avec la plus grande population de Gigantiens, et l'unique état sans aucune guilde ni dieux. Ils ont été souvent vu pour cela comme des "sans-dieux", ne respectant pas la déesse Hesper.

Les Tublehtohl sont un peuple fier et puissant, pratiquant en grande partie *l'art de la force*. Il n'a jamais été envahi, et son territoire n'a jamais changé depuis l'ère pré-classique : en effet, il est aussi anciens que la Hierocratie. Il a combattu tout les grands empires qui se sont porté à ses frontières et ont tenté de le conquérir, mais reste majoritairement pacifiste. Tubleh est un royaume fier et avec un fort sens de l'identité

Leur Capitale est Abaton. La cité reste cependant en grande partie interdite aux non-tublehtohl. Quant à la cours royale, elle est interdite à toutes autres personnes que la famille royale et leurs serviteurs, afin d'évité toute possibilité de tentation de prise de pouvoir et d'établissement d'autre dynastie.

Ce royaume australe est puissant et à la particularité d’être à la fois isolationiste sur certains aspects, mais de former des alliances protectrices avec les autres états. C’est ce royaume qui a empêché jadis l’empire Skelfing de tout envahir son sous-continent, et qui a notamment libéré la seigneurie Alandale. Pour beaucoup d'autres région, son ancienneté et ses différences avec ce qu'ils connaissent en fait une terre auréolée de mystère.

Cette terre est à l'origine du culte du grand prophète, qui serait "tombé de nul-part" et aurait enseigné la souffrance né de la mort de la déesse Styx.

### Le culte du Grand-Prophète

Le culte du Grand-Prophète est le plus récent des cultes, et est particulier parce qu'il s'agit du seul culte dirigé directement envers la déesse Hesper, où plus précisément un culte visant à la compassion envers la souffrance causée par la mort de Styx. En cela, ce culte est également souvent nommé "culte de Styx". D'après les légendes de ce culte, la tristesse de Styx se trouveraient partout dans le monde à travers le Styx.

Ce culte met en avant la douleur de la grande déesse, qui voit son monde constamment attaqué par les guerres et la haine. Pour lui, tout les Æstriens sont enfant de la déesse, et chaque guerre est un blasphème quand elle n'est pas justifiée. Il possède un second aspect : l'organisation des défense du monde.

En effet, le grand prophète à fait une prophétie à l'origine de ce culte : qu'un jour, les dieux de l'extérieur reviendront et attaquant l'oeuvre d'Hesper, et qu'il était du devoir des habitants de ce monde de le protéger en signe de respect pour la déesse.

Ce culte est principalement présent en Andale et dans le royaume de Tubhel.

### Jungle de Thot

La jungle de Thot est une jungle épaisse qui recouvre tout l'ouest de Tubleh. Cette région de Tubleh contient de nombreuses tribues plus nomade que dans les villes. La jungle est cependant en grande partie un sanctuaire, au centre se trouvant le temple qui sert de tombeau au grand-prophète.
