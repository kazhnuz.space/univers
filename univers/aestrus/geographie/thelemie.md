# Province de Thélèmie

La Province de Thélémie est un regroupement de duchés semi-indépendant recouvrant tout le centre et le sud du continent d'Etherial. Il était originellement contrôlé par un Saint-Duc, élu par les différents nobles de Thélémie à chaque mort du précédant. Pays fortement religieux, on y trouve une forte influence de la Hiérocratie. Il était nommé avant l'invasion Skelfing et la transformation en Province le Saint-Pays, et la dénomination province est restée lors de l'ère du Magister.

Le système de Saint-Duc a été restauré à la libération, jusqu'à la mort du dernier Saint-Duc des mains de *Zéphyr* lors de la *révolution des sapins*. Depuis, le système est devenu un système d'assemblée élue par le peuple, et les nobles ont perdus bien du pouvoir.

Contrairement à d'autres provinces anciennement féodales comme Aymon, la Thélémie souffre d'un manque technologique, qui se rattrape ces dernières années. De plus, il y a de forte disparité entre les régions de la province, notamment de par la proximité de Ermengarde et Lothaire, les deux capitales (économique et réligieuse) du pays. D'autres villes secondaires, telles que Théodat et Gapt, s'occupent de tenter de dynamiser les autres territoire thélèmiens.

## Ermengarde et Lothaire

La région d'Ermengarde et Lothaire est une région au nord-est de la Thélémie, formant une péninsule entre Dingir et la Bicéphalie, faisant partie avec cette première de l'ancienne zone d'influence de la première grande civilisation. Cette région est la plus riche et la plus puissante de Thélémie, et l'un de ses grands centre.

Ermengarde est la capitale religieuse du pays, une grande ville religieuse, ou se trouve une grande partie du clergé Thelemiens. C'est ici que se trouve le *Temple de la Vie*, construction ancienne dont on raconte que les souterrains recèle une des clefs pour accéder au *Cieux Radiant*

Lothaire est la capitale politique et économique du pays. Grande ville maritime, cette ville est composée d'une ancienne ville, où se trouve tout les centres d'influences politiques du pays, et la nouvelle ville où se trouve tout les centres d'influences économiques. Lothaire est entourée d'une mégapole immense, l'une des plus grandes du monde.

## La Grande-Plaine

Les grandes plaines sont d'immense plaines herbonneuses recouvrant une grande partie du centre de Thélémie. Région rurale, considérée souvent comme le grenier d'Aestrus, mais aussi peuplée de nombreuses petites forêt, il s'agit d'une région fortement traditionnelle, ou la culture thélémienne est mixée avec de nombreuse croyances plus anciennes, avec une religieusité souvent considérée comme moins "pure" que celle d'Ermengarde.

La capitale de cette région est Théodat, une ville de taille moyenne, ayant pas mal grandis avec le début d'exode-rural lié à la modernisation.

Cette région a subit les assaut d'un mystérieux *empoisonneur* (souvent soupçonné comme étant du peuple Toxis) lors des derniers temps de la royauté.

## Smarg

Smarg est la région au sud de la province, connue pour ses grandes forêts et sont climat un peu plus chaud et humide. La capitale de cette région est la ville de Gapt.

Le coeur de cette région est recouvert par une immense forêt, situé en plein dans une veine ignienne. Cette forêt semble s'être constituée durant l'ère pré-classique, parce qu'elle recouvre de nombreuse ruines des Séraphim, ce qui laisse supposé qu'elles ont existé avant son apparition. Peu de trace existe de l'apparition de cette forêt, même dans les territoires voisins. Une grande quantité d'énergie ignienne circule dans les arbres, lors donnnant des capacités extraordinaires. Ces arbres sont d'une certaine manière une version "végétale" des adonaïs.

Cette foret est l'endroit où se cache les Toxis, la guilde du poison. Les Toxis sont vu comme les protecteurs de la forêt, connaissant tout ses secrets.

## La Bicéphalie

La Bicéphalie est la grande île au sud de la Thélémie, connue pour le désordre politique qu'il y a régné pendant des siècles. Son nom vient de son système d'héritage complet : en effet, la Bicéphalie possède des loi faisant qu'il y a toujours deux ducs simultanément sur le territoire bicéphalien. Ce n'est pas que le territoire est découpé en deux influence politique, non. C'est qu'il y a deux souverain sur *exactement le même territoire*.

Officiellement, cette loi devrait permettre une gouvernance plus sage, la compétition des deux souverains les forçant à avoir les grâces du peuples en faisant le plus possible, à être honnêtes et travailleurs afin d'être le plus appréciés... Dans les faits, cette compétitions les a rendu prêt à tout pour gagner, faisant les pires crasses, trahisons, manipulations. L'histoire bicéphaliennes est pleines de coups bas d'un souverains sur l'autre.

Cependant, depuis l'ère contemporaine, le passage à la démocratie parlementaire à diminué fortement l'influence et le pouvoir des deux souverains... cela n'empêche pas la rivalité de continuer à exister. Elle s'exprime cependant surtout aujourd'hui sur des concours, des luttes de gloire, et la présence constante de deux équipes dans tout les défis sportifs qui existent.

Et beaucoup de débats sur lequel est le meilleurs dans les diners familiaux.
