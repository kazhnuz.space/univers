# L'Océan Intérieur

L'Océan Intérieur est le plus petit des deux océan de Aestrus, composé en grande partie des mers à l'intérieur de l'arc de cercle formé par le contient de Cardia. Cette océan comporte quelques iles, majoritairements controlés par un pays unique, la *Fédération des Comptoirs*, une fédération de pays indépendants, sous le contrôle des Navigateurs de l'Eau.

Cette fédération est indépendante dans la guerre entre le Grand-Ordre de Bahal et le Magister.

## Le territoire maritime d'Uraziel

Le territoire maritime d'Uraziel est un petit territoire costale situé vers les montagnes de Thermona, entre les continents de Io et d'Etherial. Ce territoire a été colonisé par les **navigateur de l'eau**, qui en a fait le centre de la guilde-peuple. C'est un endroit difficile d'accès pour les navigateurs qui n'ont pas de pouvoir sur l'eau.

Mélange de peuples, de religions et de coutumes, mais grandement peuplé par les membres de la guilde de l'eau, sa capitale est la cité flottante de Zhûria.

Ce pays est à l'origine de la fédération des comptoirs.

## L'Ile d'Ënoa

Aussi nommée *île du fer*, l'île d'Ënoa est une grande île située à l'est du royaume d'Alandale, au large du continent d'Anvêbres. Cette île était surtout prisée pour ses mines de métaux, et pour ses nombreuses veines stygiennes. C'est ici que s'est former la guilde du Métal. Cette ville a considérablement grimpée en importance lors de la révolution industrielle.

Sa capitale et plus grande ville est nommée Fer, en hommage à la déesse éponyme, et est la ville considérée comme la plus technologiquement avancée de tout Æstrus.

Anciennement comptoir commercial de Volpe, elle est devenu un membres de la Fédération des Comptoirs lors de l'invasion de cette ville par Volpe.
