# Les æstrien⋅nes

La planète Æstrus est peuplée d'animaux anthropomorphiques, parlant marchant comme les humains, mais ayant quand même quelques caractéristiques venant de leur "espèce animale".

Les lions seront souvent fort et costaud, pareil pour des créatures comme les éléphants ou les rhinocéros, d'autres seront plus faible mais rapide comme les lapins, les souris ou les libellules. Les animaux aériens peuvent voler, les animaux à branchies peuvent respirer sous l'eau. L'échelle de taille est plus grande que des humains, mais restent plus limité que chez les animaux "classiques", puisqu'elle est généralement entre 1m et 2m80.

De plus, des clichés ont tendances à exister proche de ceux de fiction que l'ont peut trouver chez nous (les renards sont manipulateur, les vautour sont capitalistes, etc).
