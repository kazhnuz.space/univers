# Élysiens

Les Élysiens (aussi surnommé les *Célestes*) sont le nom donné à une civilisation avienne ayant vécu sur les terres célestes, il y a de cela des siècles. Les Élysiens étaient les disciples des dieux avant le départ de Hesper et la division des dieux.

Ils utilisèrent les savoirs de Styx, dernière fille de la déesse Hesper afin d'utiliser la magie pure, sans avoir à la transformer de manière élémentaire (une technologie qui ne sera véritablement maitrisé à nouveau que dans les époques modernes).

## Origine et fonctionnement.

Les élysiens ont été fondée par la déesse Hesper. À l'origine, il ne s'agissait pas vraiment d'une civilisation différente de la civilisation "terrestre", les deux vivaient en harmonie, et les échanges étaient nombreux. Ils étaient dirigée par un conseil de 12 philosophes-rois, dirigés par le peuple.

La déesse Styx présidait le conseil, et était d'une certaine manière la monarque de cette civilisation. La civilisation mettait en valeur avant tout la sagesse et le partage, et luttait avant tout contre la guerre.

Leur rôle n'était pas de diriger le monde, mais plus de contrôler l'aspect "technique" du monde. Il était une forme de "conseil technique" de l'univers créé par la déesse hesper, et les Terres Célèste étaient en quelque sorte l'espace administration de ce monde. Plus que des dirigeant du monde, ils en était des techniciens.

Les élysiens vénéraient la déesse Hesperus et la déesse Styx comme des forces absolue du monde, tandis que les seizes dieux étaient plus vu comme des êtres d'exceptions admirables, des formes de héros.

## La séparation

La civilisation élysienne changea radicalement lors de la mort de la déesse Styx, assassinée dans sa petite maison.

Plusieurs théories existent quand à sa mort : certaines accusent un membres des peuples de la surface, jaloux de la puissance de la civilisation, tandis que d'autres accusent un des philosophes-roi, voir un ou plusieurs des frères et sœurs de la déesse. Après la chute de l'Empire Skelfing, la rumeur comme quoi l'assassin auraient été un ancêtre des Skelfing se forma, mais n'a que peu de valeur historique.

Ce fut vers ce moment que des mouvements pro-céleste commencèrent à se former dans cette civilisation, qui s'éloigna petit à petit des peuples terrestres. Ce phénomène fut notamment du à un effet concrêt de la mort de Styx : il fut désormais impossible d'utiliser la magie pure pour les élysiens.

En effet, ce fut à cette période que la déesse Hesper se retira dans la lune pour y reposer éternellement, et que les 16 dieux relâchèrent leur regard sur le monde. La civilisation élysienne commencèrent alors à vraiment se considérer comme une "civilisation". Ceux qui avaient été le plus proche des dieux. Les plus sages, face à un peuple barbare.

Le premier changement fut de choisir petit à petit tout les philosophes roi parmi des habitants des Terres Célèste au lieu de les élirent parmis les membres du continent. Et petit à petit, ils commencèrent à gouverner les peuples des continents, devenant un véritable empire.

Puisque les "dieux" étaient parti, cela voulait dire qu'ils avaient gagné la place. Les philosophes-rois commencèrent à se voir comme les élus des dieux.

## L'effondrement des cieux.

Cependant, le Ciel devint de plus en plus jaloux de la Terre. En effet, pourquoi les terriens, qui étaient "bêtes" auraient les terres arables, les climats les plus diversifié par rapport au froid de l'archipel flottant ? De plus en plus, les cieux considérèrent comme "normal" d'exploiter les terres.

Mais même entre eux, la situation empira de plus en plus. De grande dynasties de philosophe roi se formèrent, avec de forte rivalité interne. Se voyant au dessus de tout, ils considéraient qu'ils n'avaient de compte à rendre à personne. Les cieux se déchirèrent en une grande guerre civile. L'utilisation d'arme magique venant de Fer provoqua la destruction progressive des cieux, et l'effondrement de la société des philosophes-roi.

Un seul survécu, et pris la gouvernance des continents, devenant le Basileus. Fasse à l'hybris de ses collègue, il changea radicalement le rapport à la magie de l'Empire Elysien, formant la Théurgie Célèste.

## La hiérocratie

En effet, les élysiens continuèrent en quelque sorte à exister, à travers le plus ancien des culte Hespérien encore existant : la Théurgie Célèste, une religion de repentance.

Selon cette religion, l'hybris que le monde gagna à cause de la présence des dieux est la cause de l'évenèment, et les céleste comme les terrestres étaient coupable de la mort de Styx, parce que tous avaient commis le péché responsable de l'évenement. Le chef de cette religion, le Basileus, est considéré officiellement comme le "dernier philosophe-roi".

Cela commence ce qu'on appelle l'époque "Hiérocratique" de la civilisation Élysienne, et sa fin. Avec les siècles, les caractéristiques de la civilisation Élysiennes, et de nombreux savoirs disparurent de la Hiérocratie, mais ce qui les tua vraiemnt furent leurs actions envers les autres peuples.

En effet, ce royaume était très dur, cherchant la repentance et voyant tout les habitants comme des pécheurs. Voyant ce qu'ils se passaient, les dieux commencèrent à confier leurs pouvoirs aux mortels, formant les guildes. Une grande guerre se produisit, les peuples dotés des pouvoirs des guildes réussissant à progressivement détruire ce qu'il restait de la civilisation, réduisant la Hiérocratie à Dingir, sans influence et appauvri.

## La théurgie célèste aujourd'hui

La théurgie est aujourd'hui une religion surtout présente sur le continent d'Etherial, dans les terres de Dingir et de Télémie. C'est également la religion de l'Empire de Skelfing, même si de grands différents idéologiques existent entre l'empire et la théurgies.

Ce culte à pour objectif de combattre l'hybris, et de cacher aux mondes les savoirs anciens de la civilisation élysienne, en grande partie perdus. Cette religion estime aujourd'hui que les Empire ne peuvent être éternel, et qu'il ne faut pas chercher à retrouver leurs savoirs.

La théurgies des celestes s'oppose activement à l'exploration des Ruines Elysiennes. La théurgie considère que les terres célèstes sont des terres sacrées devant rester inviolable, et leurs de par leur filiation.
