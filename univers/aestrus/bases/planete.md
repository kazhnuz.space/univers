# La planète Æstrus

La planète Æstrus est une petite planète habité par des puissances étranges, et peuplée principalement par diverses espèces ressemblant à des animaux anthropomorphiques nommées les Æstriens. Son climat est celui d'une planète vivable tempérée, proche de celui de notre Terre, composé de nombreux paysage incroyable. La population aestrienne n'est pas extrèmement élevée, et de nombreuses terres restent en parties sauvage, habité par d'ancienne relique et créatures.

![Forme des continents de la planète Æstrus](aestrus.png)

La plus grande particularité de cette planète est la présence de fragment de terres suspendue dans son atmosphère. Ces terres, nommées les *terres céleste* ou les *cieux radiants* par les habitants, contiennent de nombreux vestiges mystérieux et technologies difficiles à comprendre.

## Vie et organisation d'Aestrus

Aestrus est une planète qui a connu de nombreuses anciennes civilisations, dont certaine ont laissé des traces ayant toujours des effets de nos jours. En effet, les habitants actuels de la planète savent qu'ils restent de grand mystère que leur prédécesseur ont laissé, et de grand danger. Cela fait notamment qu'une partie de la planète est dangereuse, habitée par de puissance créatures bioméchanique nommée les *souverains*.

Cela fait que les habitants vivent majoritairement dans des villes - souvent fortifiée - et dans des terres alentours avec de nombreuses protections. De nombreuses zones sont déclarée "zones sauvages", de la présence des souverains, et sont surtout exploré par les aventuriers.

## Les continents d'Aestrus

La surface est l'endroit où vivent les différentes espèces d'Æstrus. Elle est constitué de nombreuses civilisations, qui ont grandement évoluée avec les siècles, se sont mélangées, attaquées, etc. Sa constitution politique est l'effet d'une lente évolution, et a été dominée par un empire, l'Empire Skelfing, jusqu'à il y a quelques décénnie. 

La surface de la planète est composée de deux continents principaux, ou parfois plus un continent et une grande île :

### Le continent des grandes terres

Le continent des grandes-terres constitue la grande majorité des terres de la planète, et peut être considéré comme constitué de trois sous-contient : Anvêbre, Etherial et Io (la conjonction des deux). Ces trois sous-continents sont relié entre eux par une immense chaine de montagne et de glacier, nommé les chaines de Thermona. La majorité des habitants de la planète vivent ici.

### Ur

L'**ile d'Ur**, plus petite et plus difficile d'accès est une petite île connue pour ses conditions difficiles et son éloignement du reste d'Æstrus.

## Les Cieux

Les Cieux sont un ensemble d'îles éparses, éparpillées dans toute l'athmosphère aestrienne en un ensemble de petit archipels flottant dans le ciel. La surface totale des cieux n'est pas très grande, formant à peine un quart de la surface du continent d'Ur.

Les cieux sont cependant particulièrement intéressant, étant ce qu'il reste de l'ancienne civilisation des Séraphim. Cela fait en effet que les îles sont pleines de technologies inconnues datant de leur age d'or. L'exploration des cieux commence à l'époque contemporaine par le Magister et d'autres groupes, se cantonnant au début surtout aux îles les plus proches du sol.

Le monde se retrouve dans une immense course pour le contrôle des cieux radiants depuis les premières explorations de quelques îles "limites". Découvrir ses secrets sont l'objectif de nombreux groupes.