# L'ignum

L'Ignum est une substence/énergie née des contradiction interne dans l'univers (phénomène nommé le "Paradoxe").

Il prend la forme d'une matière étrange, rare, aux propriété calorique (production d'énergie) et radioactive. Elle peut provoquer quelques phénomènes quantique (superposition d'état) ce à échelle macroscopique. L'ignium n'est pas une susbtance atomique (elle n'est pas de la matière ordinaire), mais à la particularité de pouvoir réagir avec des atomes de matières ordinaires.

Cette substance à eu plusieurs nom : non seulement l'ignum et parfois anciennement le "calorique". L'ignum a été découvert pour la première fois par la Première Civilisation mais surtout utilisée par la deuxième.

Certains êtres, les *Adonaï* sont naturellement capable d'utiliser l'Ignum.

## Les différentes formes de l'ignum

L'ignium peut exister sous des tas de formes, combinées ou non, mais que l’on classifie en deux catégorie : l'ignium *stable*, et l'ignium *lourd/corrompu* (bien plus radioactive et mutagène). Une grande partie de l'ignium lourd vient d’une catastrophe dont peu est connu, mais peut aussi être produit artificiellement en quantité plus réduite.

L'ignium possède différentes utilitée suivant sa forme :

- L'ignium stable pur possédant de forte propriété de génération et de stockage d'énergie est utilisé grandement pour créer des batteries à hautes performances, utilisée en grande partie pour allimenter les bases spatiales éloignée du soleil où les vaisseaux spaciaux. Le principe est qu'elles sont rechargées à l'aide de l'énergie solaire, puis peuvent tenir des temps records pour fournir ensuite de l'énergie. Les capacités de conservation d'énergie baisses spectaculairement sous forme d'alliage, mais les métaux et matériaux produit ont souvent des particularités magnétiques, voir supra-condutrice. En effet, il est possible avec de l'ignium de construire des supra-conducteur à température ambiante.

- L'ignium lourd ou corrompu pur est théorisé comme pouvant servir à construire des armes destructrices. C'est cette forme qui permet de produire des effets quantique à échelle macroscopique.

L'ignum est toujours solide.

## Courant tellurique et Adonaï

Les courants telluriques sont d'immenses courants d'énergies souterrains situé entre les veines igniennes. Cette énergie est utilisable pour produire de l'énerige civile, même si moins utilisable que les veines igniennes, étant très diluée.

Cependant, elles sont utilisable par des êtres vivants, les Adonaï, ayant la pouvoir de maitriser l'Ignum naturellement. Parmi les effets que les courants telluriques ont sur eux, il y a :

- Une regénération plus rapide des blessures, ainsi qu'un temps de sommeil moins important.
- La possibilité de capter et déplacer cette énergique, soit en secousses terrestres, soit en jet d'énergie.
- Une capacité à plonger dans les courants telluriques pour se déplacer.

La source exacte de la capacité des Adonaï à maitriser cette énergie est encore peu connue, mais semble être lié à des mutations génétiques. Certaines légendes disent qu'elles ne seraient pas naturelles...

## Les veines igniennes

Les veines igniennes sont la source de l’énergie utilisée dans les centrales igniennes du conglomérat Irae Corporation.  Il s’agit de zones d’émission spontanée d'énergie igniennes qui existent à quelques endroits de la planète, relativement rares mais trouvable sur tout les continents.  Même si ces veines sont rien face aux restes, il est possible de produire avec une énergie propre et durable avec.

Il est possible de produire de une énergie propre et durable à l’aide de ces failles. Quand elles ne sont pas sur-exploité (comme ce qui est arrivé dans un incident avec l’une des premières centrales igniennes), ces failles - sont contrairement à l'Ignum pur - plutôt docile à utiliser.

## L'ignum élémentaire

L'ignum élémentaire est une forme "rafinnée" de l'ignum, rendu plus stable mais moins "puissant". Ses pouvoirs sont transformée pour en faire. Sous cette forme, l'ignum peut produire un "effet élémentaire" différent suivant la manière dont il a été raffiné, et "l'élément" que cela à produit.

Si l'ignum élémentaire ne produit pas de "matière" à proprement parler, mais ont des effets sur son entourage (et parfois décharge sa propre matière), qui ont souvent donné l'impression qu'il pouvait en produire.

Le tableau des éléments est le suivant:

|"Élément"|Effet|
|:-----:|:---:|
| Végétal | Produit une lumière douce favorable à la photosynthèse (mais aussi au corps des animaux en fait). |
| Eau | Attire et condense l'eau autour de lui. |
| Glace | Absorbe la chaleur jusqu'à ne plus le pouvoir |
| Air | Produit de fort écarts de chaleurs autour de lui, provoquant des vents |
| Foudre | Génère de l'éléctricité |
| Feu | Produit de la chaleur, peut favoriser la formation de plasma. |
