# Le Styx

Le Styx, ou énergie stygiennes est l'une des formes d'énergie qui existe dans l'univers de Æstrus. Il s'agit d'une énergie puissante et instable, proche et dérivée de l'Ignum, qui est cependant plus volatile. Cette énergie est dangereuse et difficile à contrôler de par sa nature hautement corruptrices pour les organismes vivants. L'énergie stygienne directement peut en effet corrompre le corps et l'esprit.

Le Styx semblent provenir en majeure partie d'une dimension parallèle, dont les particularités sont encore peu connues. Les moyens d'accéder à cette énergie sont les **failles stygiennes**, des déchirurent dans le tissu de la réalité laissant fuiter de cette énergie.

## Effets

Le styx à un pouvoir corrupteur sur le corps et l'esprit. Mutagène et puissant, son simple contact peut provoquer des brulures, voir de la suffocation. A long terme et à forte dose, il peut avoir des effets transformateurs sur les êtres biologiques, à l'exceptions des guerriers stygiens. Les armes stygiennes sont de ce fait particulièrement dangereuse, mais sont souvent à trop faible concentration pour qe se produisent les effets les plus mutagènes.

Il affecte également l'esprit, pouvant amplifier l'anxiété, la colère, la peur, etc. Si ces effets semble "amplifier" plus que créer, il est à noter qu'ils ne sont pas spécialement plus potant chez les personnes plus sensibles, il semblerait que la sensibilité aux effets psychologique du styx soit sur un autre domaine que la sensibilité psychologique "habituelle".

Le maximum des effets mutagène du styx se trouve dans les *derniers souffles*, ou la personnes a vu l'enterieté de ses "cellules" remplacée par une copie stygienne. Composé de pur styx, ils n'ont cependant aucun contrôle, et deviennent généralement une forme exagéré d'eux-même. Cet effet est particulièrement rare cependant, et se produit par des expositions ne se produisant que dans des cas extrêmes.

## Formes naturelles

L'énergie stygienne existe sous plusieurs formes naturelles : liquide ou gazeuse. Contrairement à l'Ignum, elle n'a pas de forme solide, même sous des température extrèmement basse.

Cette énergie est nommée **brumes stygiennes** sous cette première forme, et est particulièrement dangereuse parce qu'elle peut s'insinuer partout, et affecter plus facilement les esprits. Les lieux ou la brume stygienne est présente sont souvent évité, de par leur dangerosité.

Sous la forme liquide, on parle alors de **sources stygiennes**, pouvant parfois même former des lacs ou des rivières.

Si elle n'a pas de forme solide, on peut en trouver mélangé à des solides, devenant alors des **fragment stygiens**.

## Les Guerriers Stygiens

Les Gerriers Stygiens, (dénommé originellement avec un nom de code CH-XXX) sont des êtres biotechnologique créé en série ayant le pouvoir d'être insensible aux effets du Styx, et de pouvoir le manipuler. Ils ont été créé à travers des manipulations utilisant le styx par la hiérocratie durant l'ère contemporaine, à la fois en réponse aux Adonaï et aux Protecteurs.

Pouvant être d'espèce différentes, ils se repèrent toujours par un pelage ou plumage aux teintes violettes sombres, avec des lignes blanche qui traverse leur corps. Ils ont le pouvoir d'utiliser l'énergie pure du styx.

Le premier d'entre eux est Charon (CH-000), un aigle arrogant, très fier de sa position de premier guerrier stygien. Il a une vision très négative de ses "clones" (même s'ils n'en sont techniquement pas), qu'il voit comme des copies inférieurs. Il ne vit que pour effectuer ses missions ayant été formaté comme une arme, et accorde énormément d'importance à son statut de "guerrier stygiens", avec une vision personnelle de la chose.

Ils sont créé à partir de conditions comparables à ceux d'apparition d'un dernier-souffle, mais avec un processus propre à la hiérocratie. D'une certaine manière, ils sont des version "stabilisée" de ce que pourrait être un dernier-souffle "biologique".
