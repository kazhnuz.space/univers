# Les arts secrets

Les arts secrets (ou arts interdits) sont quatre anciens arts surnaturels. Si leur nom secondaire est un peu incorrect (ils ne sont pas interdits partout), ils ont été et sont vus de manière négative dans de nombreux empires.

De nombreuses rumeurs ont circulé sur les praticiens de ces arts, notamment de par l'origine possible de leurs pouvoirs. Cependant, cela reste un mystère pour tout les non-initiés, mais la théorie la plus courante est que ce serait des forces existant naturellement chez les êtres vivants et qu'ils auraient appris à maitriser.

Les praticiens de ces arts se reconnaissent par des effets secondaires que ces arts laissent sur les yeux de ceux l'utilisant. L'invention des lentilles de contact ont grandement permis aux practiciens de ces arts de se cacher.

Ces arts sont en théorie naturellement possible pour tout les Aestriens, mais certain⋅e⋅s peuvent les apprendre naturellement plus facilement, pour des raisons encore peu expliquées.

## Toxis : L'art du poison

L'art du poison est un art ancienne, grandement présente dans la région de Smarg, qui permet aux personnes la pratiquant de synthétisé des produits, pouvant servir aussi bien de poison que de guérison.

La "guilde" lié à cela sont les toxis, dans la région citée plus haut. Elle est secrète, cachée dans la forêt pour ne pas être trouvée. Les personnes voulant l'apprendre doivent s'aventurer dans la forêt, et sont d'abord observé en secret des jours durant pour savoir s'ils sont digne de confiance.

Cet art laisse des cernes profond et distinct d'un violet sombre, à cause de la circulation des poisons dans le sang.

## Choris : L'art du son

L'art du son est un art permettant de produire et contrôler des ondes sonores, pouvant causer malaise aux ennemis, ou soignés les maux du cœurs. On raconte que la pratique musicale est l'une des principales occupations des membres de cette guilde, qui doivent la maîtriser du mieux possible, afin d'avoir le plus grand contrôle sur leur pouvoir. Les membres du son peuvent également entendre les plus petits son, ou cacher la plus grande des explosions.

Sa guilde, Choris, était originellement une académie profitant d'une relative autonomie dans le royaume d'Arete, mais a été interdite et persécuté par l'Empire Skelfing. De nos jour, cet art s'apprend de maître en élève, et ce sont les maîtres qui trouvent leur élève. On les trouve surtout dans les milieux les plus musicaux.

Cet art provoque une l'absence de pupille dans les yeux, ainsi qu'un iris extrêmement pale.

## Oeil : L'art de l'esprit

L'art de l'esprit est l'art de controler et projeter son esprit, ses émotions, et de comprendre celui des autres. La télépathie, la télékinésie et autre pouvoirs extra-sensoriels sont ce qu'apprennes les membres de cette art. Puissante, inconnue et considéré comme effrayante, elle a été l'une des plus grandes sources de questionnement après la découverte de l'Ur.

Sa guilde, Œil, est présente originellement dans ce continent, et revèle un voile de mystère à tout ceux qui voudrait la connaître. Une grande partie de ses actions sont cachée, et il faut y entrer par un protocole très stricte. Ils sont également peu en contact avec le reste de la population, pour éviter de trop lire dans leur pensées.

Cet art prend les yeux entièrement noir, avec une pupille violette.

## Tubleh : L'art de la force

L'art de la force est un art permettant de maitriser la puissance de son corps et de l'amplifier. Cet art demande une grande maitrise, et peut être dangereuse pour le propre corps de la personne l'utilisant. Une maîtrise de soi et un apprentissage est essentiel, alors. On raconte cependant qu'une personne utilisant bien se pouvoir doit se faire tirer dessus par plusieurs soldat à répétition avant de tomber.

Cet art est particulier, parce qu'il est celui le plus éloigné d'être un "art interdit" : il n'a pas de guilde, mais est un art de la civilisation de Tubleh. Elle est utilisée pour le combat, mais également pour la construction, les taches lourdes, etc.

Cet art provoque l'apparition d'une fine ligne noir au niveau de leurs yeux, entourant toute leur tête.
