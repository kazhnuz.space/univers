# Les secrets d'Aestrus

Si Aestrus est avant tout un monde de science fiction (même si de science fiction soft, ayant peu d'intérêt dans le fait de faire des sciences très "dures" et rigoureuses), l'un des principes de cette planète est d'être une terre de mystère et de secrets.

Cette planète est donc affectée par différents éléments, notamment lié à son élément central, *l'Ignum*. L'ignum est une matière énergétique, provenant du *Paradoxe*, ayant de nombreux effets. C'est ce qui entraîne l'existence de nombreux éléments "surnaturels" (ou "presque surnaturels") de l'univers, tel que les énergies dérivées de l'ignum (élémentaire, styx) ou les êtres pouvant l'utiliser (guerrier stygiens, protecteurs, adonai).

Une déesse, Hesper, est également fortement mentionnée dans les textes anciens d'Aestrus.
