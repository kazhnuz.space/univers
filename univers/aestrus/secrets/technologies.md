# Les technologies d'Aestrus

L'univers d'Aestrus contient un certain nombre de technologies basée sur ses différentes sources d'énergie, l'ignum et le styx. La première est largement plus utilisée, étant beaucoup plus contrôlable et de ce fait moins dangereuses. Une grande parties des avancées technologiques sont cependant sous le contrôle de Irae Corporation et des grandes factions militaires de la planètes.

Le niveau technologique d'Aestrus est légèrement inférieur au notre, mais avec quelques technologies spécifique à l'Ignum ou aux éléments de cet univers. La conquête spatiale y est cependant peu développé, et les machines volantes y sont plus rudimentaire.

## Transports

Comme de nombreuses autres technologies, les transports aestriens utilise l'ignum. Ces transports sont globalement proches des notres, avec des voitures, trains, etc. utilisant l'Ignum comme source d'énergie. Un domaine dans lequel Aestrus à de l'avance est les machines volantes, qui sont nombreuses, notamment du à l'envie d'accéder au Cieux Radiant. Cependant, la technologie n'est pas suffisante pour y accéder facilement, et cela reste souvent de grandes aventures.

Avec l'existence de créatures puissantes dans les territoires sauvages, le train est la principales manière de voyager entre les villes, les lignes de trains étant souvent protégée contre les dangers du dehors. Il existe également de petites machines volantes personnelles, utilisées par les aventuriers justement pour explorer les cieux radiants ou les grands territoires.

## Technologie élémentaires

L'ignum élémentaires sont la principale source d'énergie domestique sur Aestrus. Ils sont utilisé dans de nombreux domaines. Une partie sont en vente libre, et sont utilisé aux quotidiens pour de nombreux équipements ménagers : les cristaux de feu sont utilisé pour la cuisson et le chauffage, ceux d'eau pour la gestion du précieux liquide, ceux d’électricité pour faire fonctionner certains équipeents...

## Armes

Les armes les plus courantes dans le monde d'Æstrus sont à la fois les armes blanches, et les "armes à feu". Ces dernières sont souvent en vérité des armes à feu ignienne, utilisant des cristaux d'énergie pour tirer. 

La plupart utilisé des *rebus cristallins*, déchets impurs n'ayant pas d'effet élémentaires, mais largement moins cher que les cristaux élémentaires. Il existe également des armes élémentaires, plus chères, qui utilisent des cristaux élémentaires.
