# Le Magister

Le Magister est une force mondiale et armée, ayant pour objectif la supervision et la protection des nombreuses *Province* d'Aestrus. Il est cependant en partie indépendants de celle-ci, ayant son propre commandement, même si un *Conseil des Province* existe en son sein, pour garantir qu'il suit les objectif de la planète. Elle ne possède de ce fait pas vraiment de territoire, si ce n'est quelques îles militarisés et des bases partout dans le monde.

Tout les habitants de la planète sont ainsi à la fois habitants de leur province, et du Magister. Plus qu'une union des provinces, il est un pont et l'un des protecteurs de celle-ci.

## Les forces du Magister

Les armées du Magister sont la principale force militaire d'Aestrus, et ont comme rôle de protéger à la fois les intérêt du Magister, et les provinces alliées. Cette armée est dirigé par un Commandant.

L'armée est actuellement en grande partie dédiée à affronter l'Imperium, qui vise à conquérir le monde pour former un empire, mais n’arrive pas à le localiser et à lancer des attaques précises. L'inefficacité de GUN à vaincre cette menace est la source d'une méfiance chez certaines personnes, qui souhaiteraient un "gouvernement fort" mondial.

## Le projet Erinye

Le projet Erinye est un ancien projet abandonné du Magister. Ce projet dirigé par Ys Gradlon consistant en une nouvelle expérimentation sur des lasers à énergie ignique. Son but était que l’énergie serait envoyée depuis les centrale vers les satellites, qui la concentrerait en tir d’énergie ignique pouvant toucher le monde entier.

Le satellite principal retransmettrait l’énergie vers la batterie de satellites dispersés autour du globe, qui pourraient ensuite s’occuper de la concentrer et de faire le tir dans leur zone d’action. N’importe quel endroit du globe pourrait alors être touché par un tir d’énergie.

Il a été déclassifié il y a quelques années, et sa flotte de satellite est incomplète.
