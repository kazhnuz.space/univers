# Irae Corporation

La société Irae Corporation est l’un des principaux exploitant de l'énergie Ignienne. Holding co-détenu par des actionnaires privés et par le Magister, la société recherche dans le domaine des technologie lié à l'Ignum et de leur utilisation, à la fois en tant qu'énergie pure et que par les application de leur énergie raffinée.

L'entreprise est également l'une des principales sources de *technologies élémentaires*, qui utilisent les énergies élémentaires afin de pouvoir créer des technologies adaptées. Elle construit aussi bien des armes élémentaires, que des machines volantes...

## Les premiers projets

Ils utilisent notamment pour cela les thèses de Teal Ishtar sur ce type d’énergie, quand il était chercheur à l’université de Cardiopolis. La première centrale a été fondée sur l'Île Spectrale dans l'archipel Iladien. Cette île avait la particularité d'être le dernier territoire du peuple du Troisième Oeil, l'un des quatre peuples oubliés. Elle a été implémentée par la fille d'Ishtar, elle-même membre de ce peuple, sous la supervision d'Ys Gradlon, dirigeant des forces du Magister à l'époque.

Cependant, un incident mystérieux provoqua la disparition de la majorité du peuple, ainsi que du commandant Gradlon.

## Les centrales igniennes

Les centrales igniennes sont une source d’énergie de plus en plus présente sur Aestrus. Construite sur les veines igniennes et sont utilisées depuis environs trente ans (pour le marché publique) à cinquante ans (pour l'Armée Fédérale). L’énergie produite par les centrales igniennes revet deux forme : l’éléctricité, ou les fragment igniens, des batteries d'énergie notamment utilisé pour les vaisseaux et les robots de l'armée.

L’intérieur d’une centrale ignienne est cependant un endroit dangereux à cause de l’influence de l’énergie et de sa transformation en électricité ou en *Fagment Igniens*, et est réservé à des connaisseurs de l’énergie ignienne. Le cœur d’une centrale chaotique, nommé le *Noyaux Ignis*, est un endroit ou l’énergie circule suivant des patterns très précis.

C’est la deuxième source d’énergie la plus utilisée, après l’énergie thermique liquide produite par la société ThermoTopia, utilisée depuis à peu près la même période. Contrairement aux centrales ThermoTopia, les centrales igniennes ont le defaut d'être extrèmement limité en terme d'emplacement, ayant besoin de veines igniennes pour fonctionner.

## Les vaisseaux Irae

Les moteurs Irae sont la nouvelle génération de moteur aériens, permettant de créer des cargo céleste. Irae Corporation tire en effet une grande partie de ses profits de sa création de vaisseaux, qui seront rapidement utilisé par de nombreux groupes.

Ces vaisseaux sont cependant encore assez lent, et vu comme ayant de nombreux défauts à travers la simple utilisation de ces failles. C'est le débuts de leur utilisation par de nombreux groupe qui forcera les armées du Magister à investir dedans, faisant encore plus la richesse d'Irae.

Ils sont en grande conçu pour l'exploration future des Cieux Radiants.

## Les robots Irae

Irae produit également des robots de combats semi-autonome, pouvant être employé pour des rôles allant de l'arme de guerre au garde du corps robotique. Ces mécha servent notamment de milice dans la ville d'Irae, protégeant la capitale de l'empire financier.

Contrairement à ceux de Caesar, les robots d'Irae possède un design soigné avec de nombreuses lignes courbes, étant avant tout un produit.
