# La Hiérocratie

L'*Ordre Hiérocratique d'Æstrus*, (parfois appellé "hiérocratie" ou "Ordre de Bahal" selon la ville où il a été fondé) est un ordre militaire autoritaire pan-national, fondé à l'origine comme une "Église" mais ayant fortement dérivé plus vers une organisation religieuse puissante et militaire. Ils sont dirigé par un unique "Philosophe-Roi", reprenant le titre ancien de la civilisation Séraphim.

Leur idéologie est fondée sur l'idée de restaurer la *première grande civilisation*, et sur l'idée qu'ils seraient un "ordre véritable" obéissant à la déesse Hesper. Ils sont ennemis de toute les autres factions, mais avant tout du Magister et de l'Imperium.

La hiérocratie accorde beaucoup d'importance à la culture et aux belles lettres, continuant à promulguer des textes de règle dans des langues anciennes - même s'ils sont aussi traduit (mais sans que la traduction n'ai de vrai valeur légale).

## Objectifs

La Hiérocratie à pour objecctif de restaurer leur vision de la *première grande civilisation*, dont ils pensent reprendre le fonctionnement. Une grande partie de leur fonctionnement est fondé sur une vision éculée formée au siècle dernier de la vision Séraphim, reniée par les historiens.

Leur second objectif est la destruction ou des Adonaï et des Arts Secrets, vu comme une menace pour la déesse elle-même. Ils menent notamment une guerre sans merci contre les Skelfing, un des derniers grands clan d'Adonaï, et cherche fortement le *dernier prince Skelfing*.

Leur dernier objectif est l'ouverture et l'accès aux Cieux Radiants.

## Idéologie

Les croyances historico-religieuses de l'Ordre font partie intégrante de son fonctionnement. Celle-ci mélangent des avis plus générique avec des points de vue servant "d'explication" à certaines de leurs croisades. En règle générale, la hiérocratie possède une idéologie faisant de la "culture" (ancienne) et du respect du sacré la chose la plus importante.

La première est que les Séraphin dérivaient directement de la déesse Hesper, créatrice du monde, et que cette ancienne civilisation était la représentation. Pour eux, cet empire - méconnu - représente le monde tel qu'il était prévu pour être. De ce fait, pour eux, les civilisation suivantes étaient des civilisations barbares et sans cultures qui ne respectaient pas la vérité, et ont couvert de honte Æstrus, notamment de part leur absence de lien avec les terres célèstes.

De par la présence d'Adonaï dans les deux, ils considèrent aussi l'Empire Skelfing comme ayant été une résurrection (théorie souvent partagée par les Skelfing eux-même, souvent pour donner de la légitimité à leur régime) de la seconde civilisation, et le Magister comme étant une de la troisième à cause des centrales ignienne.

En second lieu, leur théorie classique sur la chute de cette empire est une *trahison de l'intérieur* par les arts secrets. De ce fait, ils ont une grande lutte contre les arts secrets et tout ce qu'ils considèrent comme des *menaces intérieures secretes* d'Æstrus qui complète les *menaces extérieures* que sont les Adonaï.

Leurs critiques contre les deux grandes autres puissance (Magister et Impérium) sont que ces premiers seraient des "ineptes" plongeant le monde dans leur chaos par leur modernité effréné, et que les second seraient des barbares sanguinaires incultes ne croyant qu'à la force brute et n'ayant aucune "vision de société".

## Technologie

Originellement opposé fortement aux technologie ignienne (notamment parce que venant de la seconde civilisation), ils ont commencé à s'y adapté, et ont même réussi à acquérir des scientifiques renommé, notamment qui leur permis de créer il y a 10 ans les *Guerrier Stygiens*, des êtres au pouvoirs exceptionnels.

Si l'aspect récent du Styx est source de division sur son utilisation, elle a fini par être accepté, et les guerriers stygiens font partie intégrante de la stratégie de la hiérocratie.
