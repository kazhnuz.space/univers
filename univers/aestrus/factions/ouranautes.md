# Les Ouranautes

Les ouranautes sont un groupe d'aventurieux qui utilisent les machines volantes pour traverser le ciels. Ils se sont formé en réponse à la naissance de la Hiérocratie et de l'Imperium, et sont en lutte active contre ces deux factions. Ils ont été fondée par la résistante Alicia Hestia, lorsque la Hiérocratie à attaqué son village.

Ils sont en combat principalement contre les deux factions cité plus haut, généralement plutôt en bon terme avec le Magister, et parfois se retrouve contre également les Grims, les Hydres ou les Loyaux.

## Fonctionnement

Les ouranautes ne sont pas vraiment une faction en tant que tel, plus une idéologie et un mouvement mondial, luttant contre l'idée de grande puissance qui gouverneraient toutes les provinces, voir même contre les provinces elle-mêmes, lors par exemple de la révolution thélémienne, provoquée notamment par les héros *Zéphyr*, *Mistral* et *Alyzée*. Ces groupes sont locaux, et communiquent grandement entre eux, formant des groupes de combattant/résistant locaux.

Les principaux chefs Ouranautes se réunissent généralement afin de tenter de prévoir des plan commun. Cependant, des tensions existent parfois fortement entre les groupes, leur mettant des bâtons dans les roues.
