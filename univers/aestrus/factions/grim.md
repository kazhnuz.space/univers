# Les Grims

Les Grims sont une organisation illégale fondée et tournant entièrement autour du Professeur Grimlock, un savant anciennement travaillant pour le Magister puis Irae Corporation. Cet æstriens singe agé à décidé un jour de créer sa propre faction et depuis combat tout les autres, de manière chaotique.

Ils ont organisé de nombreux coups d'éclats, notamment l'attaque de la bibliothèque de Sépulcre, n'avait jamais subit de vol auparavant, le coup qui les a fait connaitres. Ils pratiquent de nombreuses opérations illégales tels que l'attaque de lieux puissants et anciens, le vol d'objets puissants, et la contrebande de cristaux élémentaires. On raconte que de nombreux objets rare sur le marché noir finissent chez les Grims, et qu'ils commettent régulièrement des actes de pirateries.

Cependant, ils ne visent pas de but économique, et font ces actions uniquement pour obtenir des resources qui leur sont nécessaire, pour les plans du professeur. Pour agir, ils utilisent de nombreux moyens technologiques, et nombres d'entre eux sont des cyborgs.

Le but des Grims est de soutenir les plans du professeur Grimlock afin de trouver et d'accéder aux *Cieux Radiants*. Ils n'ont pas de QG principal, mais de nombreux bases volantes.

## Le professeur Grimlock

Savant spécialiste dans les différentes énergies traversant l'univers d'Æstrus, Grimlock est un expert en robotique et énergie ayant pour objectif de trouver les *Cieux Radiants*, cherchant notamment la gloire et la célébrité que lui apporterait un tel fait. Il est amateur des coup d'éclat, tel que lorsque ses troupes ont attaqué et détruit le plus grands robot de combat du Magister le jour même de sa mise en service.

Hautement confiant de lui, égocentrique et avec des airs de dandy, Grimlock est une personne se voulant rafiné et esthète, avec un gout pour la nourriture fine, la haute littérature et les musiciens classiques. Il n'a aucune honte de ses actions criminelles, et n'hésite pas à se mettre en avant, et même à faire des interview dans des journaux.

Ce savant singe est quelque peu agé, mais se décrit lui-même comme "dans la fleur de l'âge". S'il semble loufoque à première vue, il réussi à échapper aux forces du Magister depuis des années, et fait jeu égal avec les plus grandes puissances du monde. Il n'est pas un ennemi à prendre à les légères, on dit que tout ceux qui l'ont fait ne sont jamais ressorti de ses bases.

Son némésis est le chevalier libre *Zéphyr*, qui a défait plusieurs de ses plans. Cependant, le chevalier n'a jamais réussi à capturer Grimlock.

## Organisation

Les grims sont organisé en un grand nombre de *sbires*, très souvent des cyborgs ou utilisant des technologies avancée pour combattre et pour agir. Ils utilisent régulièrement également des véhicules de conceptions de Grimlock, et sont épaulée par une armée de drône de combat créé par le savant. Ils sont nombreux, et sont connus pour ne pas être très efficace en eux-même - voir même parfois plus des boulets qu'autre chose - mais efficace grâce à leurs nombre et aux stratégies du singe.

Cependant, leur admiration pour leur chef est sincère, et ils le voient comme un génie incontestable. Malgré leur faiblesse et le fait que la plusieurs de leurs initiatives personnelles ont été des échecs quelques peu ridicule, peu ont réussi à être capturé, étant à la fois des experts de la fuite, et aidé par quelques membres plus efficaces : Les généraux grims. Ceux-ci sont un petit nombre et sont des sbires d'élites, généralements plus dangereux.

Les grims forment généralement des équipages dans des machines volantes, avec un général et des sbires.

Ils sont dirigé par la Première Commandante de Grimlock, Kenta Llanthu. Loyale aux Grim, celle-ci s'assure avant tout de la sécurité des sbires. Elle est connue comme étant comme impitoyable au combat.

*On raconte qu'un être puissant, adonaï ou guerrier des ombres travaillerait pour les grims.*
