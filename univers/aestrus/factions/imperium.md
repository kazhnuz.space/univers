# L'Imperium

L'Imperium est une faction militariste expansionniste s'étant fondé après la *double attaque sur l'ile de Septentrion*, dernière terre des Skelfing, par hasard à la fois par le Magister et la Hiérocratie. L'idée de l'Imperium est que la protection du monde ne peut être fait que par une faction forte, et ils rejette la "faiblesse" du Magister pour ne pas prendre un contrôle directe des provinces.

L'Imperium est grandement composé à la fois d'ancien Magisterien ayant été condamné pour divers crimes (généralement des abus de pouvoir), de déçu du manque de résultat du Magister face à la Hiérocratie et de nostalgique de la "Grande Époque" de l'Empire Skelfing.

L'Imperium à comme figure de proue Fatir Skelfing, père biologique du prince Skelfing et reçoit l'aide du Professeur Elohim, qui expérimente pour amplifier les capacités des Adonaï.

## L'Empire de Skelfing

L'Empire de Skelfing était un puissant empire, fondé par Först Skelfing. Sa puissance s'est étendu durant toute l'ère post-classique, jusqu'à sa chute formant le début de l'ère contemporaine. L'Empire était un état puissance, autoritaire dirigé par un clan d'Adonaï nommé le clan skelfing, dont le chef était également Empereur.

L'Empire croyait que l'Empereur Skelfing serait celui qui ouvrirait les Cieux Radiant et permettrait aux peuples de profiter à nouveaux des trésors des Terres Célèstes.

La fin définitive de l'Empire se produisit lors de la bataille de Lok et de la mort du roi Hetjul, tué par un Guerrier Stygien de la Hiérocratie. Cependant, sur le corps de cet Empire naquit l'Imperium, parce que le père du prince Skelfing en fuite rejoigni d'autre groupe pour former une nouvelle puissance, capable de vaincre le Magister et surtout l'Ordre Hiérocratique.

## Le Professeur Elohim

Le Professeur Elohim est un ancien chercheur du magister, ancien élève du professeur Ishtar avec le professeur Grimlock. Elohim s'est penché en grande partie sur les Adonaï et leur capacité à utiliser l'ignum, cherchant des manières pour les obtenirs, tandis que son collègue s'intéressa à la robotique. Tandis que Grimlock fut baladé d'Irae au Magister, Elohim fit une carrière brillante et reconnue, mais sans avoir souvent la possibilité de mettre en pratique ses théories.

C'est pour cela qu'Elohim déserta, souhaitant rejoindre les Skelfing. Cependant, il fut refusé par le roi Hetjul, et se retrouva en fuite, poursuivi par le Magister et la Hiérocratie. Cependant, les choses changèrent après la double attaque, puisqu'ils fut rejoint par Fatir Skelfing, et les deux commencèrent leurs plans.

Le professeur Elohim construisit de nombreus sérum augmentant les pouvoirs des Adonaï, aidant fortement l'Imperium à monter vite en puissance.
