# Les hydres

Les hydres sont le nom donné à un ensemble de groupe de trafiquant et autres criminelles agissants sur Aestrus. Commettant de nombreux crimes, et échappant à toute conduites, ils sont ennemis de toutes les autres factions. Leur origine est floue, pouvant remonter au moins à l'ère de la seconde civilisation.

Ils sont particulièrement présent à Katra, métropole d'Aestrus la plus pauvre, ou ils causent de nombreux tors à la population, mais ils existent partout dans le monde. Ils ont notamment été en grande partie l'origine de la montée en Ur de l'Imperium, qui promettait de les combattre.

Les hydres sont divisée en deux grandes sous-divisions, les hydres *libres* ou *rebelles* et les hydres *loyales* obéissant au *prince des hydres* (surnommé "prince des loyaux" par certains groupes extérieurs, et "l'usurpateur" par les hydres libres/rebelles). Les premières forment une organisation décentralisée, refusant les titres et les rôles défini par les seigneurs des hydres, et optant plus pour une organisation décentralisée.

## Organisation

On nomme une *hydre* une faction des hydres, chacune ayant leur propre commandement, et pouvant tisser ses propres relations avec les autres groupes, formant souvent un maillage complexe. Une partie des hydres obéïssent au prince des hydres, et leur commandant ont donc des titres offert par le seigneurs, et sont souvent en compétitions pour atteindre. Certains groupes peuvent passer de *rebelles* à *loyaux* suivant le prince des hydres et suivant leurs groupes. Parfois, c'est même la rivalité entre deux groupes qui peuvent les faire changer de camps, pour affronter leur ennemi de manière plus efficace.

Les hydres vivent via un code de conduite leur permettant d'éviter de se faire la guerre. S'il y a eu par le passé des groupes d'hydres tentant d'y désobéïr, leur massacres par les autres groupes unifié à réussi à faire naitre une peur que cela se reproduise. Même le prince des hydres doit s'y plier, sachant qu'un adversaire pourrait facilement utiliser une désobéïssance au code pour se faire des alliers et prendre le pouvoir.

Composé en grande partie de personnes en quête de richesses, ils pratiquent de nombreux échanges de membres familiaux afin d'assurer la création d'un tissus cohérant. Vivant dans l'ombres, ils pratiquent de nombreux trafiques d'objets et substances illégales, n'hésitant pas à manipuler et capturer les plus jeunes.

Ils ont leur propre réseaux de communication.

## Le Prince des Hydres

Le prince des hydres est un rôle contesté, de dirigeant des hydres. On estime qu'environs 40% des hydres lui obéïssent. Le prince et sa cour sont le plus proche d'une organisation centralisée des hydres, mais même les hydres loyales gardent une grande marge de manoeuvre. De plus, il est possible d'être un loyal des hydres, voir d'avoir un titre chez les loyaux sans véritablement faire partie d'une hydre.

Il fourni les titres suivants forment la hiérarchie suivant : Duc (dirige un continent), Marquis (dirige une province), Comte (dirigine une zone), Vicomte (Dirige une ville), ainsi que Baron et Banneret (généralement les simples "chefs" ou membres important d'une hydre). Les membres "normaux" sont des chevaliers. S'il est possible de monter dans la hiérarchie, c'est très difficile pour les simples chevalier, et c'est généralement plus de naissance. Les membres plus gradés, eux, peuvent plus facilement changer de titre au gré des préférence du Prince.

Les hydres loyales ont des membres dans de nombreuses industries, et pratiquent fortement la corruptions pour parvenir à leur fin. Leurs actions vont aussi bien dans l'assassinat que dans le vols, et ils est possible pour des puissants d'acheter leurs services… ou leur protections.
