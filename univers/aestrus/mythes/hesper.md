# La déesse Hesper

Hesper est selon les légendes, la déesse-créatrice de la planète Æstrus, et est considérée comme une déesse bienveillante et protectrice.

On raconte que ses pouvoirs et sa sagesse sont infinie, et que le but des philosophes-roi est d'atteindre ne serait-ce qu'une partie de sa sagesse. Tout les cultes la mette au centre du panthéon des dieux.

## Origine du monde

D'après la légende, la déesse Hesper aurait créé le monde après avoir quitté un monde de chaos et de haine, composé de dieux maléfiques se faisant la guerre entre eux. Selon d'autre légendes, elle serait simplement venue sur Æstrus, et aurait créé la vie dedans. Selon d'autre encore, elle aurait eu pour tâche de la part des dieux extérieurs de le conquérir, et aurait été émue par la beauté de la vie et aurait décidé de protéger ce monde.

Lassée de la guerre et de la haine, elle aurait décidé de permettre aux peuple sle bonheurs et la protection face aux mondes extérieurs. Elle aurait conçu alors la planète Æstrus pour être ce havre de paix.

Elle aurait selon certaine légende fondé une civilisation, nommée aujourd'hui la civilisation des Séraphin, qui devait être une civilisation de paix et de partage, ainsi que les Terres Célèste. 12 Philosophes-Rois (parfois Hesper étant selon certaine légende la première philosophe-reine) devraient-être élus, et aider depuis les Cieux le monde à évoluer. Selon d'autre légende, c'est au contraire après son départ que cette civilisation se serait formée, et selon d'autre encore les deux étaient en guerre.

Le nombre de différente version font qu'on ne sait pas aujourd'hui ce qu'était exactement la véritable Hesper à la source de toute ces légendes.

## Influence sur les religions

La création du monde par Hesper, la peur des "dieux de l'extérieur" et son départ pour la lune sont parmi les principes fondateurs de toute les religions du monde Aestriens et forme le canon commun des croyances Aestriennes, même si certains de leurs points sont interprété différemment.

En effet, tout les cultes d'Aestrus sont plus ou moins lié à la vie de la déesse. Cependant, bien des cultes ont une vision différente d'elle, et de son départ - presque autant que pour la civilisation des Séraphin.

C'est en son nom que se bat la hiérocratie.
