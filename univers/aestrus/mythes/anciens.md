# Les Anciennes Civilisation

Des anciennes civilisations hautement avancée ont traversé Aestrus, et ont laissé des traces encore de nos jours. Ces anciennes civilisations ont souvent pris la forme de "grand empire", et sont souvent tombé de par leur volonté de tout contrôler, ou de part leurs oppressions toujours plus fortes.

## La première Civilisation : Les Séraphim

La première civilisation antique (-6 000 ans) est encore peu étudiée, parce qu'une partie de leurs créations ont disparu du fait de l'érosion, et que le reste serait dans les Cieux Radiants. Ils sont connus surtout dans les légendes, parce qu'une grande partie de leurs créations ont disparu du fait de l'érosion. Ils sont nommé les "Séraphim", les "Célèste" ou les "Premiers" dans les légendes des provinces Æstriennes.

Peu est cependant connu sur eux, si ce n'est qu'ils auraient été les premiers à vénérer l'ancienne déesse Hépérus, voir selon certaines légendes qu'ils en auraient été des contemporains (voir selon d'autre qu'Hesper aurait été une Séraphime). Selon les légendes, ils auraient soit été à l'origine de la création des Cieux Radiant soit les auraient découvert. Parmi les autres éléments de légendes, ils auraient été dirigé par 12 philosophes-rois. Ils sont supposé avoir été également les constructeurs de nombreuses technologies mystérieuses, tel que les rares et encore mystérieuses Portes Radiantes, et des créatures biotechnologique tels que les *Reflets*, des organismes artificiels dédiés à faire des tâches ou combattre, ou les *Souverains*, d'immense machine bioméchanique se trouvant dans les grandes terres sauvages de la planètes.

Ils se seraient effondré il y a 6000 ans, pour des raisons encore inconnus - et aucune légende ne semblent se recouper sur cette notion, avec presque autant d'hypothèse que de légende.

La Hiérocratie se réclame des Séraphim, à travers un régime aristocratique et théologique qu'ils imaginent comme celui de Séraphim, bien que peu existe sur cette notion si ce n'est l'existence du concept de philosophe-roi.

## La seconde civilisation : Les Grand-Rois d'Ur

La quatrième civilisation antique (-3500 ans) est la civilisation qui est à l'origine du continent d'Ur. Ils auraient existé sur le continent de Ur et auraient été grandement dirigé par les Adonaï.

Culture militaristique et puissante, l'utilisation des Adonaï en faisaient une puissance redoutable, capable de déchaîner sa colère partout dans le monde. Ils ont également grandement utilisé comme serviteur ou armes les êtres ayants des dons pour les *arts secrets*.

L'effondrement de cette civilisation s'est faite en deux temps, dans une période ou se produisaient de nombreuses révoltes d'esclaves. En effet, durant cette période, la population d'Adonaï a décru d'un coup, lors de la guerre contre les Protecteur (des êtres à haute énergie doté de pouvoirs incroyables) à provoqué la mort de nombreux Adonaï. Cela a permis la *révolte des arts secrets*, qui termineront d'effriter le pouvoir de la civilisation, provoquant la chute après que les révoltes se déplacèrent vers tout Astrus.

C'est la civilisation antique aestrienne ayant laissé le plus de ruine et de traces.

## La troisième civilisation : Érèbe  

La troisième civilisation antique (-2500 ans) est la civilisation d'Erebe, à l'origine de nombreuses constructions exploitant les veines ignienne. Capable d'exploiter cette énergie, ils auraient cependant été avant tout une civilisation secrète et refusant d'utiliser leur technologie pour aider les autres peuples.

Leur déclin aurait commencé suite à l'apparition du Styx dans leur méchanismes d'exploitation des veine Stygienne. Certaines personnes racontent même que ce serait leurs expérimentation qui ont provoqué l'apparition du Styx sur Aestrus. De ce fait, les décendant d'Érébien ont souvent eu mauvaise réputation, étant vu comme des menteurs cachotiers pouvant provoquer la fin du monde.

Si presque tout les sites Érébiens se sont éteint il y a de cela 2300 ans, il est à noté qu'un à survécu prêt de 1000 ans à l'apogée, étant encore en activité il y a de cela 1800 ans, d'après des textes de l'époque.

Cette civilisation aurait tenté de produire les premières machine volante pour retrouver les cieux radiants, mais ne les aurait jamais atteint selon les légendes.

## La quatrième grande civilisation ?

La notion de "quatrième grande civilisation" est une notion qui est réclamée par la plupart des grands groupes ayant des vues sur le contrôle de la planète.

En effet, les Skelfing réclament que l'Empire Skelfing, qui a contrôlé une grande partie de la planète il y a de ça 300 ans serait la quatrième grande civilisation. Le Magister estime cependant que la civilisation moderne et technologique actuelle serait la quatrième grande civilisation, tandis que la Hiérocratie estime qu'ils vont faire revivre la première - qui sera du coup aussi la quatrième.
