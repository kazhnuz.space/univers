# Les protecteurs

Les protecteurs sont huit créatures capable de maîtriser naturellement l'ignum, le transformant sans effort en énergie élémentaire utilisable. Ils sont chacun d'une espèce différente. Leur corps est semblable aux aestriens "normaux", mais possède de nombreux cristaux élémentaires incrusté en eux, souvent vu comme la source de leur pouvoirs. Ils ne sont pas sujets au vieillissement.

Selon certains, ce serait des sortes de "dieux" secondaires après la déesse hesper. Selon d'autres légendes, ce serait d'anciens êtres normaux ayant acquis des pouvoirs extraordinaires. Cependant, peu de gens peuvent dire les avoir vu, et ce qu'ils sont exactement reste un mystère.

## Aigue-Marine

**Aigue-Marine** est la protectrice de l'eau, une Léviathan forte et impétieuse.

On raconte que sa personnalité changea du tout au tout après la fin de la première civilisation. D'une protectrice sérieuse et calme, elle devint agitée, aventurière. On raconte que sa vie est une grande fuite en avant, cherchant toujours plus l'adrénaline.

Elle peut être clémente, mais sera impitoyable avec ceux se jouant d'elle.

## Smargdite

**Smargdite** est la protectrice de la nature et des végétation, une *biche blanche* aux pouvoirs de vie.

Elle est la protectrice des jeunes adolescents et adultes, et notamment de ceux ne rentrant pas dans les cases de genre ou de tout autre règle arbitraire par des tyrans. On raconte qu'elle apprécie beaucoup les gens, et qu'elle possède de nombreux⋅es suiveur⋅euses.

On raconte qu'elle aurait fondée la guilde après avoir sauvé des jeunes filles et garçons d'un roi tyran cruel, forçant tout les enfants des rivaux à servir sa cours. Elle aurait fait du jeune prince du tyran son premier disciple, et détrôna ensuite le roi quand celui-ci, furieux de cette humiliation, tenta de la tuer elle et son fils.

Il n'y eu ensuite plus jamais de roi sur le territoire, qui devint le territoire de Smarg en son honneur.

La protectrice enseigna à ses disciples le respect de la nature, teinté de méfiance envers ses caprices. Elle apprend à être humble comme manière d'être libre.

## Zircon

**Zircon** est la protectrice de la foudre, une oiseau-tonnerre à la visée redoutable.

Indépendante et nihiliste, elle pense que le monde n'a pas de sens, qu'aucune autorité n'est fondée en soi, mais qu'il faut s'organiser en groupe pour pouvoir lui donner un sens et vivre libre.

Elle est camarade de beuverie avec Grenat.

## Grenat

**Grenat** est le protecteur du feu, un phénix aux ailes flamboyantes.

Brûlant d'honneur et de fierté, il est un combattant intrépide qui constamment ne cherche que le combat. Pour lui, le duel honorable est le niveau ultime d'honneur d'un guerrier, et il se serait batut avec la plupart des autres protecteurices au moins une fois.

Il parle des combats qu'il a gagné comme qu'il a perdu, mais jamais de ceux contre Aigue-Marine et Turquoise.

Fer aurait refusé le combat.

## Fer

**Fer** est la protectrice du métal, une dragonne dont les écailles sont plus solide que le plus dur des métaux.

Forgeronne émérite, elle est à l'origine de bien des objets anciens du quotidien, et de nombreuses de ses armes anciennes parsèment le monde.

Elle encourage à l’innovation et à la création, mais peut avoir tendance à ne pas considérer les conséquences d'un objet, le voyant plus comme un "défi technique" que comme un vrai outil.

## Silice

**Silice** est le protecteur de la terre, un lindworm/dreki, vivant dans les profondeur et sortant pour se battre.

Protecteur guerrier mais rationnel, il voit la guerre comme une science. Il critique fortement les héros trop valeureux qui agissent seul, et pense que c'est de la puissance du groupe. Sérieux et autoritaire, il est en rivalité cependant avec le protecteur Grenat. On dit que leurs débats ennuient les autres protecteurs.

Pour Silice, la guerre n'est pas un art mais une science. Il n'y a pas de justice dedans : juste des intérêts à protéger et défendre.

## Turquoise

**Turquoise** est le protecteur du vent, une hypogriffe à la dexterité incroyable.

Selon la légende, admiratif d'une grande danseuse, il aurait pris à sa demande son apparence physique à sa mort et aurait continué éternellement de faire vivre sa danse, vivant parmis les humains. Ce protecteur androgyne est le protecteur des marginaux et des gens différents et qu'il attire la malchance aux tyrans et aux puissants.

Il est également le protecteur qui invite à briser les frontières et les tabous inutiles, afin de vivre sa vie pleinement et de la manière la plus artistique aussi.

## Perle

**Perle** est le protecteur des glaces, criosphinx aux pouvoirs apportant le froid et la maladie.

On raconte qu'il était un protecteur puissant et respecté, mais que les événements lié à la chute de la première civilisation l'eurent plongé dans une telle détresse qu'il est partie dans les montagnes de Thermona de chagrin.

Perle est l'incarnation du froid, dévorant et dangereux, mais qui aussi qui rafraîchi face aux flammes trop puissantes.
