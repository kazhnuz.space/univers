# Muspell la flamboyante

La première planète du système d'Ygg se remarque par son extrême proximité de son étoile. En effet, Muspell n’est situé qu’à 0.05 UA de son étoile. Elle est tellement proche d'Ygg qu'en plus de montrer toujours la même face à Ygg, elle est affectée par un volcanisme intense.

Aucun satellite n’orbite autour de cette planète, la moins massive du système d'Ygg. La planète est entièrement vide de vie.

## Description

Muspell possède une orbite synchrone à cause des forces de marrée, ce qui fait que c’est toujours la même face qui est montré au soleil. Cette force de marée, provoque aussi un volcanisme massif sur la face éclairée, avec la présence même de lac de magma par endroit.

Cette petite planète n’a également pas d’atmosphère. Cela, combiné aux écarts de températures entre les deux faces de la planète, en fait également une des planètes non-vivables, qui n’a jamais eut de civilisations, et qui la rendent difficile à utiliser et impossible à terraformer. Cependant, sa face gelée possède quelques reste d'usines d'énergie.

Une légende raconte que Muspell aurait été utilisé pour des condamnations à mort par les premières civilisation stellaire, mais aucune preuve n'a été apporté à cela.

C'est cependant une légende qui persiste dans les populations d'Ygg
