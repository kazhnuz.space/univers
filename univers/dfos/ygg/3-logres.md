# Logres la grande

Logres est la seconde planète gazeuse du système d'Ygg, et la plus volumineuse et massive. Logres possède quelques anneaux visibles. Sa couleur n’est pas uniforme, puisqu’il y a des bandes jaunes et quelques bandes un peu plus vertes, formant des volutes dans son atmosphère.

De par son status de plus grande des géantes gazeuse, et sa position de géante gazeuse « centrale », cette planète est souvent considérée comme la plus importante des géantes gazeuse.

Logres possède de très nombreux satellites, tous étant nommés avec des noms de régions de provinces de l’Empire Yggin. Les satellites principaux sont basés sur celles de la province d’Italie, et sont nommés (dans l’ordre de proximité avec la planète) : Peredur, Bors, Artus (qui est le plus gros satellites naturels de tout le système d'Ygg), Galaad, Nimue et Meliagaunt.

## Artus

Artus est le plus grand des satellite de Logres, et le plus habité ! Considéré généralement comme le satellite la plus multiculturelle de tout Ygg, on y trouve de toutes les espèce (sauf les Qryst, ne pouvant vivre sur une planète aussi chaude), et cette planète a été riche en vie et en histoire, évoluant énormément de par son histoire.

Cette planète est dirigée par une immense fédération.

Terres grandement tempérée et océaniques, composé de nombreux archipels, c'est également une terre de magie, ou la pratique surnaturelle a été fortement disciplinée.

## Peredur

Peredur est un satellite tempéré, composé de deux immenses continants aux fleuves nombreux. Extrèmement fertile, Peredur est surtout composé de grandes plaines cultivée et de forêt imposante.

Peredur est composée de nombreuses seigneuries.

Considéré comme le Grenier de Logres, ses exporations de nourritures sont sa plus grande source de revenu. Ancien astre Caith, ce satellite n'est cependant pas réellement indépendant, son gouvernement local étant sous le contrôle de Artus. Cependant, le manque de possibilité de se connecter font

## Bors

Bors à une température et une pression vivable, mais une atmosphère toxique et corrosive. Dépourvue de la moindre forme de vie hors des cités protectrices, et avec une atmosphère étrangement calme, ce est à l’origine de son surnom : « La Lune du Silence ».

On y trouve quelques cité, et Bors est en grande partie une terre d'ermitage. Bors est un territoire de Artus, n'ayant pas véritablement de gouvernement. Cependant, les ermites de Bors sont en grande partie tranquille.

On ne paie pas d'impot à Bors, mais on y a très peu accès aux biens des autres astres, et une loi locale y interdit l'accès à des comptes bancaires extérieurs.

## Galaad

L'un des deux Astres Elfiques (avec le satellite Hludana), Galaad est un autre satellite tempérée, même si plus froid que Artus.

Ses grandes forêts sont empruntes de magie et de mystère, et les elfes révèlent peu les secrets de leur planète. On raconte que le vaisseau ayant amené les humanoïdes dans le système d'Ygg y est enterré.

Galaad est composée en nombreuses seigneuries indépendantes, dirigée par un Grand-Roi elfique.

## Nimue

Nimue est un petit satellite tempéré, astre principale des Caith.

La planète est technologiquement moins évoluée que Artus, et est composé grandement de royaumes et tribus rivales. Elle est très riche en Adamant grâce à une forte connection au Wyrd, sans avoir les conditions extrême de certaines autres planètes. Composée de deux grandes îles principales (Prétus et Acrise), elle possède de nombreux phénomènes étrange lié au wyrd.

Terre de magie, elle est protégée par des Eirons, protecteur des Caith.

## Meliagaunt

Meliagaunt est un satellite riche en minéraux, même si son atmosphère et ses températures bien trop froides fait que la vie ne peut se trouver que dans d’immense villes-bulles, s’étendant sur des kilomètres. Le satellite est un astre froid et silencieux, mais où d’immense villes métalliques y sont visibles, reste de l'ancienne civilisation, avec des dômes et d’immenses tours reposant sur un socle d’acier de plusieurs étages.

Conçues pour conserver la chaleur, ces bases en font un satellite industrialisé, et dont la plus grande base a fini par devenir une véritable capitale. Sa population est grandement composé d'humains et de nains.

Les équipages y viennent souvent pour se ravitailler en technologie, Meliagaunt étant un rare lieu ou les technologies anciennes peuvent être reproduites.
