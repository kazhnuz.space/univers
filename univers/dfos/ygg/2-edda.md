# Edda la Magnifique

D’un vert sombre émeraude, Edda est la seconde planète du système d'Ygg, et la première géante Gazeuse. Sa composition est jovienne, mais les quelques écarts de composition sont la cause de sa couleur particulière, qui est à l’origine du surnom de Edda comme « la plus belle planète d'Ygg », ou alors de « joyaux d'Ygg ». Edda a de petits anneaux d'un bleu sombre, et une petite dizaine de satellite, dont seuls deux sont véritablement massifs.

Sa beauté fait que les satellites les plus proches sont une situations prisée pour les nobles et pour les équipages.

## Zone vivable constantinienne

Cette planète a la particularité d’avoir une zone découverte comme potentiellement vivable dans son atmosphère : La pression, la température y serait relativement proche de celle de la Terre et de Julius, cependant, son atmosphère y est toxique pour les humanoïdes. De plus, des îles flottantes s'y trouve, en suspensions dans l’atmosphère.

Cette zone a été colonisée à l’aide de nombreuses immenses colonies flottantes, reliées par des ponts servant à faire traverser un réseau de trains à vapeur, servant de moyen de locomotion principal, avec des bateaux volants et autres vaisseaux utilisant diverses méthodes pour flotter, ainsi que des ballons dirigeables. La plupart des intérieurs sont étanches et oxygénés, grace à des pompes allant puiser dans des poches d’oxygènes situées plusieurs kilomètres en dessous des colonies.

Les habitants sont des Eirons et c'est leur planètes principales. Ils peuvent y vivrent naturellement, mais les autres espèces doivent y avoir accès avec des masques à gaz.

## Nehalennia

Nehalennia est un satellite presque entièrement aquatique, recouvert par un immense océan unique, avec à la surface uniquement quelques îles artificielles (ou naturel, ancien pics de montagne) ainsi que des callote glacière aux poles de la planète. Cette planète était il y a des millions d'année une planète boule de neige, mais s'est réchauffée et arbore désormais un climat tempéré, presque tropical.

La planète possède quelques populations sur les îles artificielles, mais est surtout habité par une sous-espèce aquatique des eirons, en grande partie endémique à cette planète.

Les îles artificielle ou naturelle sont grandement couverte par des stations balnéaire.

## Hludana

Le satellite Hludana est un astre tempéré-chaud, mais humide avec de nombreuses forêts. Son climat va du tropical au méditérannéen, et il est composé de principalement d'île plus ou moins grandes, et de deux grands continents.

Ce satellite est un satellite riche en vie et diversifié, principalement habité par des elfes, au point ou il est considéré comme l'un des deux *Astres elfiques* et est l'un des grands lieu de cette civilisation. Des Caith y sont aussi fortement présents, et vivent soit dans les villes elfiques, soit en tant que population nomades ou petite communauté dans les nombreuses îles de la planète.

La planète est riche d'un fort commerce et un grand centre culturel, notamment dans les grandes villes elfiques. Hludana est composée en nombreuses seigneuries indépendantes, dirigée par un Grand-Roi elfique.

## Sandraudiga

Sandraudiga est un satellite aride, un immense désert de sable constituant une planète. Les pluies y sont rare, et les seules sources d'eaux sont les lacs dispersé à la surface, formant souvent de grande oasis et point de civilisation. On raconte que c'est la planète avec le moins de géographie, et que tout se ressemble sur cette grande boule de sable.

Les populations y sont grandement nomades, formant des tribues, se réunissant généralent en conseils tribaux. On y trouve de nombreux membres de la guilde des voyageurs.

Cependant, elle a quand même eu une histoire riche en vie, en civilisation et en cultures. C'est avec Baduhenna l'un des deux principal satellite Lycions.

## Baduhenna

Baduhenna est un satellite océanique et chaud, avec un immense continent. Connu pour ses pluies constantes et ses jungles épaisse, Baduhenna possède de nombreux temples anciens avec des motifs de Wyverns, laissant penser que c'est la première planète ou elles ont été domestiqué.

Très proche de sa planète, Baduhenna subit un fort volcanisme.

C'est en effet l'un des astres ou l'on trouve le plus de Wyverns, et les Wyverns considérée comme les plus exceptionnels viennent des immenses élvages existant dans les rares plaines et montagnes, notamment autour des volcans. C'est avec Sandraudiga l'un des deux principal satellite Lycions.

Les populations y forment des tribus (mais sont sédentaire contrairement aux tribues de Sandraudiga), se réunissant généralent en conseils tribaux. On y trouve de nombreux membres de la guilde des voyageurs.

## Gersemi

Le satellite Gersemi est un satellite rocheux et aride, ou souffle un vent sec et dangereux. Elle est très peu habitée, possède peu de resource naturelles, et n'a que peu d'intérêt stratégique.

Cependant, elle possède l'une des plus grandes réserve souterraine d'adamant liquide du système d'Ygg.

## Tot

Tot est l'ancienne des royaumes elfiques, avant Galaad et Hludana. Cependant, durant les grandes guerres suite à l'effondrement de la civilisation de Graal, la destruction des systèmes de protections automatisée du satellite les empêcha de voir un astéroïde qui percuta de plein fois le satellite, expulsant une partie de son athmosphère.

Les survivants durent fuir vers Galaad et Hludana, et furent en grande partie la source de la fin de la guerre.

Cette planète est en majorités déserte, cependant quelques habitant y vivent dans d'ancienne bases pressurisées.
