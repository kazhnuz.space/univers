# La ceinture de Hel

La ceinture de Hel est la dernière ceinture d’astéroïde, et englobe entièrement le système stellaire. Cette ceinture d’astéroïde est considéré comme la frontière extérieure d'Ygg.

## Gjöll

Gjöll est la dernière planète du système Solaire. Cette planète se démarque parce qu’au lieu d’être composé en grande partie de silice, elle est composée en grande partie de carbone. L’effet que cela fait et la présence potentiel de diamants – et d’une atmosphère composé en grande partie de monoxyde et dioxyde de carbone, ce qui la rend toxique en plus du froid extrême de cette planète et de la pression trop faible de son atmosphère.

Cela ne l’empêchera pas d’être source de moult convoitises de chercheurs de diamant, croyant y voir une potentielle source d’une fortune facile. Une très grande partie de ces prospecteurs finiront ruiné à vivre péniblement dans les quelques bases vivables de la planète, ou succomberont aux dangers de cette planète, tels que les nombreux terrains instables et fragiles.

Quelques traces indiquent une exploitation de la planète il y a plusieurs millions d’années.

## La tempète d'Astéroïde de Garm

La tempète d'Astéroïde est une série de plusieurs astéroïde en orbites les uns avec les autres, pouvant aller jusqu'à trois orbites consécutives. Ce véritable tourbillon de roche produit un endroit bien plus instable et difficile à y voler qu'à la normale.

De plus, c'est un endroit ou se trouve des poches d'hydrogènes en suspensions, pouvant affecter les appareils volant. Seuls des pilotes expérimentés peuvent y aller.

C'est ici que se trouve la Cour des Pirate, perché sur un ancien dragons, qui se nourri de l'hydrogène environnant, et une fois tout les 4000 ans, va faire un vol vers la ceinture intérieur pour se charger en lumière solaire.
