# Ceinture du Nibelung

> [!NOTE]
> Contrairement aux systèmes stellaire plus réaliste, les astéroïdes du Nibelung sont plutôt proche, ce qui est la raison pourquoi les nains peuvent aller en wyvern d'un astéroïde à l'autre.

Après ça se trouve la Nibelung, première ceinture d’astéroïde. Ce nom provient du nom latin du mur d’Hadrien. Il s’agit de la ceinture d’astéroïde entourant ce qui est généralement considéré comme la zone « centrale » du système d'Ygg.

Il y a trois planètes naine dans cette ceinture d’Astéroïdes, nommé selon trois des empereurs de l’année des quatre empereurs : Sigurd, Hagen et Kriemhild.

La Ceinture est sous le contrôle de l'union du Nibelung. Elle est habité par tout les peuples, mais contient une majorité de Nains. La Ceinture vit en grande partie de l'exploitation minière.

## Sigurd

Sigurd est la plus petite des planète de la Ceinture, mais la plus grande puissance du Nibelung : c'est en effet ici que se trouve la capitale éponyme du Nibelung. La planète est constitué en majorité de ville, souterraine et en surface, de fortification, et elle est réputée comme impossible à véritablement s'y repérer.

La petite planète est de ce fait presque une forteresse, et est réputée être imprenable. C'est ici que vit le Roi des Nains, figure religieuse importante de la mythologie naine.

En plus de sa situation, des siècles de sortilège défensifs participent à faire de la planète un astre sécurisé, et invincu dans toute son histoire. On raconte que sa force défensive est la raison pourquoi Sigurd ne possède qu'une armée composée de 300 personne pour tout l'astre.

## Hagen

La seconde de ces planètes, Hagen, n’est pas totalement sphérique, ayant plus la forme d’un immense ovale.

Cette planète a été grandement transformé par une puissante malédiction, et c'est ici qu'a été créé l'Anneau de Hagen, source de cette malédiction. On raconte qu'il a été forgé avec le coeur d'un Astéroïde sur lequel un dragon serait mort, et qu'il apporte la puissance à quiconque le porte, mais également lui fait subir un sort pire que la mort.

L'anneau semble en effet conférer la puissance de controler la corruption. Son créateur voulu l'utiliser pour prendre contrôle du Nibelung, mais ne put controler la puissance et fut réduit à l'état d'une Liche Corrompue, ne pouvant plus que garder, sans âme, l'astéroïde qui n'est plus que la tombe inabitable de cet anneau.

Hagen est en effet quasiment totalement déserte, à l’exception de la Liche de quelques créatures corrompue minérales dangereuses, et infestant les grottes et les anciennes mines de la planète.

Elle est mise en quarantaines, parce que vue comme trop dangereuses.

## 6.3 – Kriemhild

Kriemhild est une plaine naine, dont l'athmosphère épaisse lui permet d'avoir des température tempérée. Extrêmement fertile, elle est l'unique planète rurale de toute la ceinture.
