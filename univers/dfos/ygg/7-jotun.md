# Jötun

La dernière planète du système d'Ygg est une planète tellurique gelée est également la plus massive planète tellurique du système d'Ygg. Vue de l’extérieur, c’est une planète qui semble surtout blanches et noirs, dût à la composition de la glace présente en surface de la planète.

Jötun possède également la particularité d’avoir de grands anneaux, ce qui en fait une des rares planètes telluriques à en posséder.

Cette planète ne possède pas de satellite.

## Climat

Une des particularités de la planète est son climat : Si c’est en effet une géante de glace, sa surface possède comme le satellite Titan une grande présence d’hydrocarbure, qui fonctionnent comme le cycle de l’eau sur terre, s’évaporant, retombant en pluie et formant divers lacs à la surface de la planète, ce qui fait que le système météorologique n’est pas sans ressembler à celui de la terre – mais en bien plus froids. En plus de ce cycle d’hydrocarbure, un océan liquide existe sous l’épaisse couche de glace, dont les courants internes due à la chaleur interne de la planète et à la pression sont parfois la cause de formation de véritable volcans de glaces – des cryovolcans. Une des particularités de cette planète est la stabilité de ce climat :

En effet, Jötun aurait le même climat depuis quasiment 2 milliards d’années. Il y a très peu de variation de température, et cette planète ne semble pas avoir de climats. Son atmosphère est irrespirable, étant composé en majeure partie d’azote et de méthane. De plus, sa rotation est très lente, une journée Aurélienne étant plus longue qu’une année terrestre.

Jötun est également connu pour ses « Forêts de Glace », de grandes structures de formes de vies minérales non-mobiles (que l’on compare souvent à des plantes) et qui ont colonisé une très grande partie de la surface. Ces formes de vies minérale ont l’apparence d’immenses champignons de glaces qui auraient des centaines de petits cristaux de glaces suspendus sous leur chapeau. Cette planète est également habitée par des formes de vies minérales capable de se mouvoir, dont les parties inertes sont d’ailleurs souvent composées de glaces.
