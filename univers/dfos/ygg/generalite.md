# Système d'Ygg-Asaheim

> [!NOTE]
> Les conventions de nommage indiquée ici sont méta-narrative, un peu à la manière des planètes dans Starfox ayant des noms lié à Shakespear.

Le système d'Ygg-Asaheim est un système stellaire double, composé de deux étoiles, Ygg et Asaheim, séparée par environs 0,6 année lumière. Ce système est riche en de nombreuses matières instables, et est habité par les créatures interplanétaire nommée *dragons*, des créatures ayant la capacité de vivre dans le vide spatiale et de se nourrir d'hydrogène ou de l'énergie d'une étoile.

La première est une naine jaune autour de laquelle orbite six planète, la seconde une naine rouge autour de laquelle orbite deux planètes.

Les étoile la plus proche du système d'Ygg sont Albe-La-Rouge, une Naine Rouge situé à 3,28 année-lumière, L’Étoile-Des-Sabins, située à 4,30 année-lumière, Ravenne, située à 5,02 année-lumière, Carthago, situé à 5,23 année lumière, et Lugdunum, située à 5,97 année-lumière.

De forte puissance magiques existent dans ce système stellaire.

## Donnée générale d'Ygg

Le système d'Ygg est un système stellaire, composée d'une naine jaune faisant 1.1× la luminosité de notre soleil. Sa structure fait que les géantes gazeuses sont les planètes située en premier dans le système solaire, à l'exception de Muspell, une petite planète hautement volcanique.

Six planète et quatre planète naines orbitent autour d'Ygg, ainsi qu'une naine rouge, *Asaheim*.

Ygg est habité par de nombreuses espèces, et les satellites de ses deux géantes gazeuses sont dans la zone habitable et riches en vies.
