# Heorot

Heorot est la dernière planète du système interne d'Ygg, et la troisième planète tellurique la plus massive avec un rayon proche de 1.6 terre et une masse de 4 fois celle de la Terre.

La planète est cependant vivable malgré sa forte gravité, et habitée par de nombreuses espèces. On y trouve majoritairement des Nains, des Lycions et des D'Kraiens, et il est entièrement recouvert par l'Empire Hrothgar.

Cette planète possède un satellite, Grendel, la lune Rouge.

## Description

C’est également une planète assez inhospitalière, à cause de sa gravité faisant 1.7 fois celle de la terre en surface, mais également à cause du climat, bien plus froid et aride que sur (L’eau ne couvre que 13% de la surface de la planète) Terre, Jullius ou Crassus. De plus, si la planète à un champ magnétique fort protégeant du vent solaire, elle ne filtre que mal les ultraviolet du soleil, ce qui participe également à rendre la vie difficile sur la planète. Même la zone tempérée de la planète est très sèche, et les écarts de température y sont fort. Seule quelques forets tropicales situées dans des vallées spécifiques offrent des climats plus clément et régulier.

Cela fait que la faune et la flore Tibèrienne est assez différente de celles de la Terre et de Crassus, composé en grande partie de créatures assez reptillienne, se remarquant par de fortes carapace avec peu de diversité générique, mais tout de mêmes différentes. La faune a en effet évoluée pour survivre dans le climat bien plus rude de la planète, et un climat très homogène. Étant donné la forte concentration en glace d’eau du satellite naturel de Heorot, il est supposé que la planète a perdue son eau d’une manière ou d’une autre.

On suppose que la planète est cependant le fruit d’une terraformation par la civilisation Romulienne, puisque son atmosphère présente des traces prouvant qu’elle était toxique à l’origine, trop dense et bien trop froide. Les traces indiqueraient une terraformation vieille de 70 millions d’années et ayant duré plusieurs millions d’annés, confirmant la théorie Romulienne. Les différentes espèces vivantes intelligentes sur la planète n’en ont cependant aucun souvenir ni aucune archive, la planète ayant eut de nombreux changement de civilisation.

La principale théorie est que cette terraformation serait liée à la catastrophe qui a provoqué la fin de la civilisation Yggienne, et la destruction de presques toutes formes de vie sur Grimm. Il y a 70 millions d’année, face à la catastrophe, des groupes auraient tenté de survivre.

Habitable et habité, la plupart des grandes villes se concentrent sur les bords des zones maritimes, plus hospitalière.

## Grendel

Grendel, aussi nommée la Lune Rouge est un grand satellite orbitant autour de Heorot. Ce satellite est grandement vénéré par des Lycions.
