# L'étoile Asaheim

Asaheim est une naine rouge en orbite autour d'Ygg, situé à environ 0,8 année lumière de l'étoile principale. Cette étoile est l'endroit ou vivent de nombreux dragons et wyvern.

## Wotun

Wotun est la première des planètes d'Asaheim, et la deuxième planète la plus massive d'Ygg-Asaheim (elle est pas très loin d’être deux fois moins volumineuse que Saturne tout en étant proche d’être une fois et demi plus lourde que cette même planète) et vue de l’extérieur, elle ressemble fortement à Jupiter niveau couleurs, quoique plus orangée.

Cette planète jovienne (c’est-à-dire ayant une atmosphère proche de celle de Jupiter) possède deux satellites massifs (et une vingtaine d’autres plus anecdotiques).
