# Grimm la corrompue

Grimm est la quatrième planète du système d'Ygg. Vue de l'extérieur, cette planète massive (presque deux fois le rayon de notre terre) ressemble à une sphère d’un noir d’encre, d’ou son visible depuis l’espace des arcs éléctriques violets.

Elle est la seconde plus massive des planète tellurique d'Ygg, et possède un unique Satellite, Drosselbart.

## Description

L'apparence de Grimm est due à sa haute atmosphère, composé en très grande quantité d’une matière corrompu, ce qui lui donne cette apparence extrêmement sombre, et surtout l’étrange couleur violette des éclairs qui la traverse. Cette haute atmosphère à un climat et une composition assez différente de la surface, bien qu’il y ait de la corruption dans les deux zones. En effet, la portion haute de l’atmosphère Grimmienne est extrêmement dense, et très agitée, par des vents violents. Cette haute atmosphère est opaque, et empêche toute lumière d’atteindre la surface.

La surface de la planète est froide (malgré une distance à l'étoile qui devrait la rendre bien trop chaude) et possède une pression atmosphèrique bien moins fortes. Ce sont des paysages désolés qui s’y trouvent, balayés par des pluies violettes de corruption et de très fréquents orages, qui ont éradiqué toute forme de vie et érodé complètement la surface. Quelques lacs de corromption pure forment d’immenses flaques violettes, mais la planète possède très peu de liquides à la surface. Les seuls types de structures datant d’avant la grande corruptions sont ceux qui se trouvent profondément ensevelis sous terre. La seule structure situé en surface est un immense monolithe, datant de l’époque ou des civilisations existait dessus. Il est impossible à dater.

Désormais totalement invivable, seul quelques espèce de vie minérale ayant évolué pour incorporer l'adamant dans leur structure – Grimm était quand même habité jadis par une forme de vie dont on n’a aucune trace sur la planète même, mais qu’on suppose être la première civilisation. On estime que la totalité des espèces de la planète ont été éradiqués lors d’une catastrophe ayant engendré la forte présence de la corruption sur la planète, voir sur d’autres.

## Drosselbart

Le satellite Drosselbart est un satellite sans athmosphère, mais ou des traces montre qu'elles en aurait eu une. Il semblerait que la catastrophe Grimmienne à été la cause de la disparition de son athmosphère.

Il se trouve beaucoup d'adamant sombre ou corrompu à la surface du satellite, puisque de nombreux cristaux et minerai contaminés par de la corruption ou peuplent la croûte terrestre du satellite, on suppose qu’il s’agit de roche qui ont réagit à l’événement ayant provoqué la "corruption" de Grimm. Il ne reste plus de trace de corruption en tant que telle, le satellite s’étant purifiée depuis 11 millions d’année. Quelques lac d’adamant bleu, une substance liquide ayant la propriété de rester liquide dans des circonstances extrêmes (et également une des formes liquides d'adamant les plus abondante dans le système stellaire) parsème la surface.

Vue de loin, le satellite apparaît comme grise parsemé du bleu ciel des formes cristallines et liquide d'adamant bleu. Sa richesse en minéraux précieux et rare, en feront un parfais satellite minier et industriel. Quelques créatures cristallines minérales peuplent le satellite, les créatures cristallines étant les seules formes de vie pouvant survivre au vide spatial et aux énormes différences de températures du satellite.

## Schnee

Schnee, est un satellite à la couleur claire et froide, et à l’atmosphère épaisse. Schnee est replis d’un certains nombres de créatures qualifié de « Créatures Démoniaque ». Ces créatures minérales ont été corrompu et sont devenu de terribles prédateurs dont les propriétés électrogènes peuvent paralyser leur proie à distance.

Ce satellite est donc extrêmement dangereux, et maintenu sous un blocus.
