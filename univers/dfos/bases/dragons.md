# Dragons stellaire

Les dragons stellaire sont des créatures importantes de l'univers de *Dragons from Outer Space*, donnant leur nom à l'univers.

Ils sont une espèce vivant naturellement dans l'espace, n'étant inquiété ni par le manque d'air, ni par le manque de température. Ils vivent à une échelle complètement différente de celle des être vivant sur des planète, ayant souvent été comparé à des dieux.

En effet, les dragons stellaire sont une espèce immense, pouvant atteindre plusieurs kilomètre de longueur. Ils sont capable de voler malgré l'absence d'aile dans l'espace, pouvant se déplacer jusqu'à 1/10e de la vitesse de la lumière. Leur expérence de vie est gigantesque également, se comptant en millions, voir dizaine de milion d'année.

Les dragons se nourrissent sont de l'énergie de l'étoile ou il sont proche, ou bien d'hydrogène, allant souvent s'en délecter autour des géante gazeuses.

Ces éléments en font une espèce puissante, même si elle interagit peu avec les humains. Ils sont entre cent et deux cent dans le système d'Ygg, et plusieurs miliers autour d'Asaheim.

Les dragons peuvent cracher de véritable geyser d'énergie, destructeurs et puissants.
