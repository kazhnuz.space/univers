# La Civilisation de Graal

La Civilisation de Graal est l'ancienne civilisation de Type I (se rapprochant du type II) qui a été présente sur tout le système d'Ygg, avant la *Grande Catastrophe de Grimm*, et l'apparition de la Calamité dans tout le système solaire.

Cette civilisation regroupaient les nombreuses espèces qui vivaient dans le système et, à partir de leur arrivée, les humanoïdes (humains, nains et elfes). Sa culture est peu connue, si ce n'est d'ancienne légende.

Son centre civilisationnel était la planète Grimm.

## Technologies

Il s'agissait d'une civilisation multiplanétaire avancée, ayant construit les arches et de nombreux vaisseaux et technologie. De fait, une grande partie des technologies actuelles sont obtenues en étudiant les restes de la civilisation.

C'est eux qui ont créé les arches, qui sont les seules de leurs structures spatiales encore véritablement utilisables aujourd'hui. Cependant, plusieurs de leurs ruines existent encore…

## Effondrement

L'effondrement de la civilisation a été soudain et brutal, lorsque la Calamité s'est abattue sur Grimm, il y a de cela plus de 1000 ans.

On ne sait pas exactement comment cela est arrivé, mais l'immense catastrophe a provoqué la mort de la majorité des habitants de la planète et de ses satellites, la destruction de toute leur flotte de vaisseau stationnée autour de la planète-capitale, l'anéantissement de tout leurs systèmes de communications.

L'onde de choc a eu cependant des effets dans tout le système d'Ygg, puisque l'adamant est resté corrompu avant de retrouver ses propriétés pendant plus d'un siècle, provoquant de nombreuses pertes à cause des pertes technologiques, et celle de nombreux savoirs par la destructions des méthodes de stockages. Le reste des pertes a été faites par le temps, et par la grande guerre qui s'en est suivi pour prendre le contrôle des restes de la puissance, guerre qui provoquer notamment la chute du satellite Tot.

Aujourd'hui, seule les arches permettent de voyager à travers l'espace, et le système continue de se remettre de la catastrophe.
