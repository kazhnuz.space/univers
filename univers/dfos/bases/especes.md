# Espèces d'Ygg

Le système d'Ygg comportent de nombreuses espèces, aux moeurs et normes différentes. Ces espèces sont dispersée à travers tout le système, malgré la difficulté à traverser l'espace.

Si certaines espèces ont des astres où ils sont plus présents, dans les faits, ils sont présent un peu partout.

## Les humains

Espèce ingénieuse et fondée sur la technologie, ils sont connus pour être très sociaux et organisés, malgré le fait qu'ils ne possèdent pas véritablement d'astre, contrairement aux autres espèces.

Les humains sont dispersé à travers toutes les planètes, et possèdent également des bases sur les astres sans athmosphères.

## Les nains

Les nains sont une espèce se remarquant pour leur petite taille, et leur fort volume de cheveux et/ou de barbe, les deux poussant bien plus vite que chez les autres humanoïdes.

Les nains résistent mieux que les humains et les elfes aux fortes gravités, et aux conditions sèches et froides en général. Ils sont réputés pour êtres des mineurs, mais c'est en grande partie parce que les deux astres nains sont la Confédération du Nibelung, situé dans la ceinture d'astéroïde, ainsi que l'astre Heorot, tout deux des grandes puissances minières.

## Les elfes

Les elfes sont des humanoïdes se remarquant par leur grande taille, leur peau aux couleur froide (elle passe du blanc au noir d'encre par le gris, sans être dans des teintes beige à marron) et leurs oreilles pointues.

Ils ont une affinité naturelle pour la magie, mais sont sensible aux armes d'Adamant Sombre qui peut facilement les blesser. Ils ont une préférence pour les planètes chaudes et si possible humides. Ils se détachent également des nains et humains par leur choix technologique : ils utilisent en fait généralement plus des biotechnologies que des technologies basée sur l'éléctricité.

Ils se considèrent comme les descendants des *porteurs de lumières*, et chez certaines nobles de certaines communautés elfiques, ils voient les humains comme leurs anciens serviteurs. Ils sont naturellement sensible aux Wyrd, et peuvent avoir une compréhension et maîtrise limitée de celui-ci.

Leurs astres principaux sont *Hludana* et *Galaad*.

## Les Caith

Les Caith sont l'espèce intelligent la plus présente sur le satellite Nimue.

Ces êtres bipèdes se distingue par une fourure plus ou moins épaisse, des grandes oreilles pointues, deux longues antennes, des jambes proches de celles d'un kangourou, trois longues queues et un long museau pointues. Ils peuvent être de n'importe quelle couleur.

Ils sont ovipares et portent leurs petits dans une poche comme des marsupiaux. Ils sont dotés de capacités physiques extraordinaires. Ils sont naturellement sensible aux Wyrd, et peuvent avoir une compréhension et maîtrise limitée de celui-ci.

Généralement plus fort physiquement et plus rapide que les êtres humains, leur longévités et approximativement la même. Ils sont également présent sur d'autres astres habitée du système d'Ygg.

## Les lycions

Les Lycions sont une espèce ressemblant aux humanoïdes, mais avec des aspects "loup". On pourrait les décrire comme des sortes de "centaure canins", mais où la tête aussi est une tête de loup. Leurs chevelures peuvent souvent former de forte crinière.

C'est une espèce puissante, rapide, mais pouvant facilement être blessé. Ils utilisent souvent comme arme des arcs ou épée courte d'énergie.

## Les D'Kraiens

Les D'Kraiens sont des êtres imposants, d'une espèce proche des wyverns. Ils ont la particularité de pouvoir vivre dans des environnements extrèmes, mais sont surtout présent sur la planète Heorot. Ils sont souvent vu comme étant d'un naturel agressif et combatif, mais c'est en vérité surtout un effet des nombreuses guerres entre humains, elfes et d'kraiens dans les premiers temps du système d'Ygg.

Cette espèce est doté d'une grande force, mais est également très agile et souple. Ils peuvent rester plusieurs jours sans manger ni boire, et ont une résistence très fortes aux températures froides.
