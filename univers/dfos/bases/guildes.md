# Les guildes d'Ygg

Les guildes sont des regroupements de spécialiste de certaines pratiques, qui servent à la fois aux practiciens à s'entraider, mais aussi à parfaire la maitrise de ladite pratique. Si les tensions peuvent rester, aujourd'hui les guildes travails ensemble.

Si les guildes ne sont pas essentielles pour apprendre les professions qu'elles pratiques, elles sont souvent les lieux idéaux pour apprendre les formes les plus rafinés de ces pratiques.

Les membres des guildes commencent en tant que novice, peuvent devenir ensuite des adeptes. Après cela, ils peuvent soit devenir un *spécialiste* dans l'une des parties de la guilde, soit devenir un *Grand-Maître*, ayant une maitrise des deux domaines, pour bien plus de travail et au prix de se consacrée entièrement à son art (note : dans le JDR, cela empêche d'avoir des capacités d'autres guilde). Les Grands-Maîtres sont égalements ceux qui enseignent les arts des guildes aux jeunes.

## Guilde des Combattant

Cette armée est souvent considérée comme moins organisée que la Guilde des Chevaliers. Ils combattent généralement à l'aide d'arme légère et les arcs courts, et leur stratégie consiste souvent à foncer dans le tas et d'esquiver les coups. Cette guilde est également souvent une guilde de mercenaires et chasseurs de primes, offrant leur services pour rattraper les bandits, ou régler des soucis personnels.

La guilde met avant tout en avant l'important du combat *juste* aussi bien dans ses buts et que dans ses manières d'agir. On raconte des membres de cette guilde qu'ils ne fuient jamais un duel honorable, et qu'ils n'attaquent jamais une ennemi à terre, sauf s'il a commit des crimes impardonnable, tels que la tyrannie.

Les spécialiste de cette guilde sont nommé les *Bretteur*, expert en maniement des armes au corps à corps, et les *Grands-Archers*, expert dans le maniement des armes à distance.

## Guilde des Chevaliers

La Guilde des Chevaliers est la guilde qui regroupe tout les combattant utilisant des armes lourde, avec une importance des armures et d'un grand niveau de force. Ils se spécialise dans les armes blanches, même s'ils peuvent utiliser parfois quelques armes à feux. La Guilde des Chevaliers ont pour idéologie la défense avant tout, et ont comme idéal de n'être que des rouages dans une mécanique de protection parfaite. Ils utilisent des armes parce que pour eux, l'offense est une forme de défense, mais cette dernière est plus important. Là où les soldats du feu attaquent, ils protègent.

Leur méthode est souvent d'empêcher toute percée ennemi, et de pouvoir tenir le siège même le plus violent. Ils sont souvent les défenseurs des villes où ils se trouvent. Ils sont également de ce fait également des experts en maniement des armes, armures, bouclier et outils, et sont également des grands défenseurs de l'évolution technologiques. La guilde encourage la curiosité intellectuelle, et le pragmatisme : mieux vaut se faire attaquer en armure et avec cinquante armes que nu avec juste son courage et sa foi en son destin.

Les spécialiste de cette guilde sont nommé les *Auriges*, expert en armes et pour tenir des positions, et les *Templier*, expert pour être au front et éviter l'avancée ennemie.

## Guilde des Rodeurs

La Guilde des Rodeurs est la guilde ouverte à tout les petits criminels et aux fauteurs de troubles. Ils sont vu comme au pire la lie d'Ygg, au mieu comme "une belle bande de casse-couille".

Considérés comme étant irresponsables, ne pensant qu'à s'amuser ou à obtenir leur bien-être, quelques membres de la guilde revendique cet état de fait... mais ils sont en fait minoritaires. Beaucoup trouve un aspect plus sérieux dans cet aspect marginal, tel que de pouvoir vivre sa vie, où - dans un aspect plus "punk" - le refus de l'autorité.

Les spécialiste de cette guilde sont les *Assassins*, spécialiste pour éliminer les cibles, et les *Espion*, qui mettent tous les apprentissages de la guilde au profit d'obtenir des informations.

## La Guilde des Magiciens

La guilde des magiciens est la guilde de ceux qui cherchent à maitriser les énergies du Wyrd, ou du moins autant que possible. Leur idéologie est fondée sur la compréhension du Wyrd, et sur le fait que l'ont doit accépter ses mouvements, qui ne sont pas sous notre contrôle.

Le hasard et le destin sont des choses qu'on ne contrôle pas, et le mortel doit se concentrer sur ce qu'il peut contrôler - ici protéger ce qui est important. Ils mettent en avant la capacité à garder des secrets et à gardé caché ce qui doit l'être, tout en les fournissant à ceux qui seront de parfaire l'Art magique. Cette guilde d'érudit mettent la connaissance en avant, mais également le danger de cette connaissance. La prudence est la qualité ultime de l'érudit selon eux... cependant tous ne le sont pas toujours.

Les spécialiste de cette guilde sont les *Sorcier*, spécialiste en magie négative, et les *Archimage*, spécialiste en magie de positive.

## La Guilde des Négociateur

La Guilde des Négociateurs est une guilde qui met avant tout en avant l'importance de la parole et de la discussion. Leur vision est que chaque chose s'échange, et qu'il faut le plus possible échanger ce qui peut l'être, afin d'atteindre "l'équilibre dans un système en mouvement". Cela vaut aussi bien pour les matières, que les richesses, que les paroles et le fait de rendre des services.

Ils ont pour principe de parfaire à la fois de langage et ses découvertes, mais également les manières d'obtenir des choses et de négociers. Leurs services sont beaucoup demandé par la Compagnie de ce fait.

Les spécialiste de cette guilde sont les *Marchand*, spécialiste dans le fait d'écchanger des biens, et les *Diplomate*, spécialiste en négociation par le dialogue.

## La Guilde des Voyageurs

La Guilde des Voyageurs est la guilde mettant en avant le voyage et la survie dans les différents environnement d'Ygg, ainsi que des différents secrets qui s'y trouve. Cette guilde est décentralisée, et encourage autant que possible le nomadisme et l'expression des vocations artistiques. Comme les mages, les voyageurs ont une utilisation légère du Wyrd, et ont le maitrise de quelques armes, généralement l'arc ou les dagues.

Les voyageurs sont souvent particulièrement bons avec les animaux.

Les spécialiste de cette guilde sont les *Druides*, spécialiste dans l'utilisation des plantes et des éléments naturels, et les *Troubadour*, spécialiste dans l'utilisation des chants et de l'art.

## La Guilde des Héros

La guilde des héros est la guilde dédié à ceux qui décident d'utiliser avant tout leur charisme et leur présence. Il s'agit de ceux ne pouvant pas ne pas être présent. Cette guilde n'a pas forcément une orientation précise, les héros savent faire un peu de tout : soin, un brin de magie, de la parole, etc.

Les spécialiste de cette guilde sont les *Barbare*, spécialiste dans la survie l'utilsiation de leur force et charisme brute pour gérer seuls des situations, et les *Généraux*, spécialiste dans l'utilisation de de ces mêmes capacités pour gérer des situations sociales et implémenter des stratégies.

## La Guilde des Domestiques

La Guilde des Domestiques est la guilde dédié à ceux qui sont au service de gens, qui ne veulent pas être sur le devant de la scene. En général discret, cette guilde cependant apprend aussi à avoir une carrière courronnée de succès, parce que les domestiques de chaque célébrité ont souvent un pouvoir plus grand qu'on le croit.

Les spécialiste de cette guilde sont les *Majordome*, spécialiste dans le fait d'amener le plus loin possible ceux qu'ils servent, et les *Bouffon*, spécialiste dans la dérision et les possibilités imprévues.
