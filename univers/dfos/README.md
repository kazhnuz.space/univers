# Dragons from Outer Space

*Il y a longtemps, la grande calamité s'est abattue sur nous. On raconte qu'avant elle, nos ancêtres voyageaient librement entre les planètes, sans craindre les porteurs de désastres. Aujourd'hui, seuls nous, les équipages des Arches peuvent encore voyager. Nous sommes les liens du système de Ygg*

Dragons from Outer Space est un univers de space-fantasy, mélangeant des éléments de high/heroic fantasy dans un environnement de space-opera. Il se déroule dans un system stellaire (le système d'Ygg) avec voyage interplanétaire ou existent des humains, nains, elfs, centaure-loup (les lycions) et autres espèces magique ou aliens.

La magie y est présente grâce au Wyrd, une connexion magique mystique entre tout les êtres vivants. Un élément importants de cet univers sont les *dragons stellaires* des créatures spatiales de plusieurs kilomètres se nourrissant de l'énergie des étoiles et les *wyverns* créatures ayant la possibilité de voler dans l'espace et protéger les autres êtres vivants du vide spatial.

Cette univers se passe 1000 ans après l'effondrement d'une civilisation de Type I~II nommé la *Civilisation de Graal*, faisant que seuls les arches, des vaisseaux vivant, restent comme manière de voyager entre les astres. L'espace y est également peuplé par des créatures dangereuses, les porteurs de désastres.

Le but de cet univers est d'avoir une orientation très "pulp", offrant la possibilité de mélanger des tropes de divers univers, avec des inspirations à la fois de science-fiction à la Pern, de space-opera et de High Fantasy plus classique.
