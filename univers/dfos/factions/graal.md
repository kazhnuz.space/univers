# La république de César.

La république de César est une union multiculturelle et multiespèce, fusion politique des satellite Artus, Peredur et Bors. Il s'agit d'une république puissante, dirigé par un *Préteur* et un Sénat.

Sa capitale est la ville Artusienne de Suprapolis et sa force militaire sont les Chevalier de César.

## Formation

La république de Graal est le résultat de la Révolution de Artus, lorsque la planète, à l'époque morcellé en plusieurs royaumes possédant des astéroïdes entiers comme colonie s'est révoltée, ainis que les satellite colonisés. Un par un, tout les royaumes et vice-royaume sont tombé, et il a été décidé de former une république ou chaque planète serait égale.

## Les chevaliers de Graal

Les Chevalier de Graal sont la principale force militaire de la république Planétaire, et ont comme rôle de protéger à la fois la république, et les satellites alliés. Cette armée est dirigé par un Connétable.
