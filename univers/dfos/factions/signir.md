# Le Royaume de Signir

Le Royaume de Signir est le royaume regroupant les. Royaume principalement elfique à l'origine, ce royaume est aujourd'hui à la fois elfique et lycions, .

Ce royaume est connu à la fois pour sa diversité, pour la puissance qu'y représente les wyverns et pour sa complexité extrème. En effet, ce royaume est le résultat de plusieurs fusions consecutive, et possède énormément d'exceptions et de manière de fonctionner locale.

C'est la faction possédant le plus de territoire, avec les planète Hludana, Galaad, Sandraudiga, Baduhenna et Tot.

## Formation

Le Royaume de Signir est né de deux royaumes : le Royaume Elfique Unifié et la Fédération Lycionne.

### Le Royaume Elfique unifié

Le royaume elfique unifié est la fusion des royaume de Hludana et Galaad, après la fin de la Guerre Elfique, quand les trois royaume elfique se sont fait la guerre, la destruction quasi-complète du troisième Royaume - Tot - provoqua une colère chez les elfes, furieux de s'entretuer pour des magouilles politiques.

Ce fut cependant une grande partie cela qui sauva la situation, avec le marriage des deux nouveaux rois de chaque royaume (les précédant étant mort sur le champ de bataille), et l'adoption du fils survivant de la lignée Totienne, Signir. Une fois devenu roi, le fils devint roi des trois royaumes à la fois, qu'il unifia en le Grand Royaume Elfique.

Il fut également celui qui acheta à la Fédération Lycionne l'astre peu habité de Gersemi, que ces descendant perdront au Grand-Culte.

### La Fédération Lycionne

La Fédération Lycionne est l'union de toute les tribue Lycionne de. Cette union s'est faite sur plusieurs siècle, via des compromis entre les tribus, et la création d'une conseil des doyens de chaque tribue.

C'est quelques années avant la fusion qu'un nouveau rôle sera ajouté pour renforcer le pouvoir central, celui du Grand-Guerrier, chef de tout les guerrier Lycions. La nouvelle armée inquiéta pas mal le Grand Royaume Elfique, ainsi que même Graal.

### La fusion

L'arrière-petit fils de Signir sera celui qui unifiera ces deux pouvoirs.

En effet, suite à une crise majeure dans la Fédération, il réussi à établir un pacte lui offrant le pouvoir civil sur les deux entité, tout en offrant le pouvoir militaire aux chef de la fédération, le Grand-Guerrier Lycions. Le parlement elfique et le conseil des doyens lycions devirent les deux chambres du pouvoir legislatif, chacun accueillant des membres de l'autre espèce tout en conservant leur spécificité.

Cela demanda des adaptations, notamment l'établissement de tribues elfique, et l'établissement d'élection dans les territoires lycions.

Le nouveau royaume fut nommé Royaume de Signir, en hommage à l'ancêtre du roi.
