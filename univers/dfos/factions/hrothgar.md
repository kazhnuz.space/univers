# L'Empire Hrothgar

L'Empire Hrothgar est un empire puissant stiué sur la planète Heorot. Même si cet empire est planétaire, son influence est multiplanétaire.

Dirigé par un empereur et un Conseil comprenant des représentants de toutes les espèces de la planète, il est connu pour son armée puissante et son isolationnisme. Il se considère comme l'héritier de l'ancienne civilisation romulienne, qui proviendrait de Grimm.

Ils forme avec Signir et Graal l'une des principale puissance militaire du royaume.

Son armée est souvent dite la plus inoccupée du système d'Ygg, parce que même si elle est incroyablement puissant, l'Empire n'a jamais connu de gros conflits. L'armée de Hrothgar a donc évolué vers un rôle de protection, intervenant lors des nombreux catastrophe que Heorot peut subir.
