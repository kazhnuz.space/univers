# Le Nibelung

Le Nibelung est le nom donnée au régime se situant dans la Ceinture du Nibelung. Dirigé par l'union des mineurs du Nibelung, représentant différents intérêts dans la ceinture, ce régime vit en grande partie du minage des nombreux astéroïdes qui s'y trouve.

Les planète naine de Sigurd, Hagen et Kriemhild sont les astres principaux du Nibelung, mais celui-ci couvre aussi toute la ceinture d'astéroïde. Il est souvent en lutte avec les Pirates pour le contrôle de la ceinture.

Le Nibelung vit en grande partie des exportation d'Adamant d'Or ou d'Argent, deux métaux très réputés.

## Formation

Le Nibelung a eut une origine un peu mouvementé. À l'origine, il s'agissait de quelques pays se trouvant dans la ceinture, mais qui ont été petit à petit passé sous contrôle de Graal.

Petit à petit cependant, c'est la Compagnie des Planète d'Ygg qui en a eu le contrôle de la Ceinture… jusqu'à une révolution, dirigée majoritairement les Nains - très nombreux dans Nibelung - qui mis les mineurs au coeur du pouvoir, et forma le Nibelung en tant qu'état indépendant ouvrier.
