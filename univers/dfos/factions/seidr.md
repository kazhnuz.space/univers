# Le Seidr

Le Seidr est un ensemble de coutumes et pensées en vigueur dans le système d'Ygg, et forme une faction un peu à part des grandes puissances multi-planétaire ou planétaire que sont Graal, Signir ou Hrothgar.

Il est grandement suivit par les Caith et les Eirons, mais existe sur un peu toute les planètes, dans un peu tous les peuples. Ses principales lieux important sont la zone vivable constantinienne, les profondeur de Nehalennia, et le satellite Nimue.

## Pensée

Le Seidr ne cherche pas à être une puissance, et est plus un mouvement cherchant l'humilité et à ne pas. Ils sont souvent moqué comme étant "trop optimiste", et la cause de la chute en importance des Eirons, et de "l'incapacité des Caith à jouer le grand jeu de la politique".

La pensée Seidr est une pensée d'entraide, et de vie en petit groupe, en effet peu compatible avec l'idée d'avoir un grand empire multi-planétaire puissant.
