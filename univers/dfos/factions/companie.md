# La Compagnie des Planète d'Ygg

La compagnie des Planète d'Ygg (nommée souvent juste "La Compagnie", parce que l'ont sait de laquelle on parle) est une compagnie d'exploitation commerciale fondée à l'origine dans Graal, ayant pour but l'exploitation des resources lié au Wyrd et à l'Adamant.

Holding puissant et tentaculaire, la société recherche dans le domaine des énergies, de la robotique, des vaisseau spaciaux. Sa forme est celle d'une fédération de nombreux commerçant, dirigée par une organisation centrale elle même controlée par une forme d'actionnariat.

## Historique

Fondée originellement sur Artus, elle a progressivement grandit et gagné en puissance. La Compagnie est productrice des nombreuses technologies utilisant le wyrd ou l'adamant, ainsi que de centrale pouvant exploiter directement le Wyrd.

Depuis la fondation de la ville d'Irea sur le satellite de Meliagaunt, la corporation est devenu une entité indépendante.

## Territoire

La Compagnie possède de nombreux comptoir commerciaux dans tout le sytème d'Ygg, mais également deux satellite : Meliagaunt et Nehalennia.
