# Les pirates

Les pirates sont le nom donné aux nombreux malfrats qui voyage entre les planètes, commettent des larcins ou de la contrebande, et vivent leur vie en dehors de la loi de toute les autres factions.

Ils sont souvent composé d'équipage plus ou moins indépendant, et répondent surtout à un code d'honneur. Ne pas le respecter peut entrainer de se faire trainer - physiquement - jusqu'à la Cours des Pirate, unique instance à laquelle les pirates obéisse, situé sur un dragon stellaire.

Leur vision du monde est assez simple : le monde ne nous fait pas de cadeau, et les gens qui possèdent le possèdent souvent de manière injuste, ou en tout cas grâce à un système injuste. De ce fait, ils estiment leurs activités, souvent essentielles à leur survie, comme étant un "mal nécessaire". Ils ne se voient pas comme des "bonnes personnes", mais comme une partie nécessaire du monde.

Plus qu'une "mafia", cette guilde est un réseau d'entraide pour petit voleurs, avec tout de même un code d'honneur : pas d'attaque envers les enfants, éviter de tuer sauf si nécessaire. Tuer un tyran peut cependant être parfaitement acceptable.

Les pirates accueille souvent ceux dans le besoin.
