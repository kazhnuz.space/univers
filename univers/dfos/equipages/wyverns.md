# Wyverns spatiales

Les Wyverns sont une espèce proche des dragons, mais bien plus petite. Ils vivent principalement dans les planètes, et n'ont pas la vitesse ou la longévité de leurs illustres cousins.

Leur longévité se rapproche plus des 80~100 ans, et leur vitesse dans le vide ne peut atteindre que 0.001% de la vitesse de la lumière, ce qui les rends peut utile pour les voyages trop lointain, mais efficace pour voler d'un astre à l'autre si on est partant pour un voyage un peu long.

Les Wyverns produisent naturellement un champ d'énergie autour d'elle, protégeant aussi bien elles et leur cavalier du vide spatiale. Elles peuvent également emmagasiner de l'énergie solaire et la recracher sous forme d'énergie pure.

Les wyverns d'un équipage sont liée à l'arche dans laquelles elles sont nées. Elles sont d'une certaine manière des membres à part entière de l'équipage.
