# Les Arches

Les Arches sont le seul moyen de déplacement interplanétaire du système de Roma. Ces vaisseaux vivant sont en fait des créatures de l'espèce des dragons, qui ont été via bioingéniérié modifié pour devenir des dragons vivants. Ils ont une longévité comparable à celle des dragons, et forme des liens émotionnels avec leur équipages, dont ils veulent la protection (que ce soit les êtres "civilisé" ou les wyverns)

Plus petits que les dragons, ils ont souvent de quoi habiter un équipage de quelques centaines de personnes, qui forment de véritables petites communautés.

## Apparences

Les Arches ressemblent à des dragons-tortues, avec de grands pics sur la carapaces, des petites ailes, pattes et une petite tête. Au dessus de la tête, sur la carapace, se trouvent généralement la cabine du commandant ou il peut voir l'espace devant lui, et diriger l'Arche.

L'intérieur de la carapaces contient des couloirs et des pièces, là ou vivent l'équipage. Ces pièces naissent naturellement lors de la croissance de l'Arche, avec les instruments nécessaires. L'intérieur d'une arche ressemble à de long couloir d'écailles ocre, avec les technologies intérieures ajoutées. L'ajout de ces technologie ne font pas mal à l'arche.

## Capacités

Les arches ont toutes les capacités nécessaire pour être de bons vaisseaux, et notamment les seules possibles. Leur carapaces sont d'une solidité extrême, et peuvent protéger des chocs, du vide spatiale mais également des radiations. Cependant, elles ne sont pas indestructible et peuvent être abimée, voir détruite, ce qui sera généralement fatal pour l'équipage.

Les arches peuvent cracher de l'énergie comme une wyvern ou un dragons, mais cela peut la fatiguer.

Leur liens psychique puissant permet aux membres d'un équipage sur une planète de communiquer avec l'Arche (et donc les membres présents dessus) si celle-ci est en orbite autour (cela peut être plus difficile si elle est de l'autre côté de l'astre), et les arches peuvent communiquer entre elle à des plus grande distances.

## Les Kobolds

Les kobolds sont des petites créatures naissant naturellement dans les arches. Les kobolds sont des sortes petits "dragonnets" bipèdes peu intelligent, mais pouvant se rendre utile et aider.

Ils ont tendance à penser avec leur estomacs.
