# Les équipages

Les équipages sont une des notions les plus importante de l'univers de *Dragons From Outer Space* : Ces groupes multiculturels sont ceux qui vivent dans les arches, et sont les seuls à pouvoir faire des voyages interplanétaires.

Les équipages ayant un liens télépathique avec leur arche généralement dans leur jeunesse, il est souvent difficile pour un membre d'une arche de la quitter. C'est souvent pour cela que - pour éviter la consanguinité, les enfants partent vivre dans une différente arche durant leur enfance, devenant des *novices* dans la nouvelle arche.

## Rôle et importance

Les équipages ayant des arches, ils sont les seules à pouvoir traverser l'espace, et donc les seules à pouvoir transporter des êtres et marchandises à travers le système solaire, et également à pouvoir affronter les menaces spatiales, telles que les créatures née de la Corruption.

Ils ont de ce fait une influence certaines, étant souvent les bienvenus à des cours, des lieux importants, etc.

## Rôles d'équipages

Chaque membre d'équipage à un rôle possible, généralement pouvnat participer à la fois aux combats et à la vie civile. Un des rôles les plus important sont celui de capitaine. Le Capitaine est celui formant un lien privilégié avec l'Arche, ayant le pouvoir de communiquer avec. À sa mort, les autres membres doivent essayer de faire une connection avec l'Arche pour être choisi.

Les Grands-Maîtres sont d'autres rôles importants : Chaque équipage tente d'avoir un Grand-Maître de chaque guilde parmis eux, qui a pour but d'apprendre aux jeunes les arts de la guilde.
