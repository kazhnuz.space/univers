# Les lycions

Les Lycions sont une espèce ressemblant aux humanoïdes, mais avec des aspects "loup". On pourrait les décrire comme des sortes de "centaure canins", mais où la tête aussi est une tête de loup. Leurs chevelures peuvent souvent former de forte crinière.

C'est une espèce puissante, rapide, mais pouvant facilement être blessé. Ils utilisent souvent comme arme des arcs ou épée courte d'énergie. La plupars des Lycions vivent soient dans l'Empire Hrothgar ou dans le royaume de Signir. Dans ce dernier, les "duo elfes-lycionss" sont une des armes les plus redoutable, avec un elfes chevauchant un lycion.
