## Les Caith

Les Caith sont l'espèce intelligent la plus présente sur le satellite Nimue.

Ces êtres bipèdes se distingue par une fourure plus ou moins épaisse, des grandes oreilles pointues, deux longues antennes, des jambes proches de celles d'un kangourou, trois longues queues et un long museau pointues. Ils peuvent être de n'importe quelle couleur.

Ils sont ovipares et portent leurs petits dans une poche comme des marsupiaux. Ils sont dotés de capacités physiques extraordinaires. Ils sont naturellement sensible aux Wyrd, et peuvent avoir une compréhension et maîtrise limitée de celui-ci.

Généralement plus fort physiquement et plus rapide que les êtres humains, leur longévités et approximativement la même. Ils sont également présent sur d'autres astres habitée du système d'Ygg.
