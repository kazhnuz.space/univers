# Les D'Kraiens

Les D'Kraiens sont des êtres imposants, d'une espèce proche des wyverns. Ils ont la particularité de pouvoir vivre dans des environnements extrèmes, mais sont surtout présent sur la planète Heorot. Ils sont souvent vu comme étant d'un naturel agressif et combatif, mais c'est en vérité surtout un effet des nombreuses guerres entre humains, elfes et d'kraiens dans les premiers temps du système d'Ygg.

Cette espèce est doté d'une grande force, mais est également très agile et souple. Ils peuvent rester plusieurs jours sans manger ni boire, et ont une résistence très fortes aux températures froides.
