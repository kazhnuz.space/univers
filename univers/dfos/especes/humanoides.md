# Les humanoïdes

Les humanoïdes sont le nom donné à un ensemble d'espèce arrivé dans le système d'Ygg il y a de cela 10 000 ans lors de l'arrivée des mythiques *Porteurs de Lumières*, peu reste cependant de cette époque. Espèces ingénieuses et créatives, elles ont rapidement pris une place importante parmis les nombreuses espèces de ce systèmes. Ils sont composé de trois principales espèces.

## Les humains

Espèce ingénieuse et fondée sur la technologie, ils sont connus pour être très sociaux et organisés, malgré le fait qu'ils ne possèdent pas véritablement d'astre, contrairement aux autres espèces.

Les humains sont dispersé à travers toutes les planètes, et possèdent également des bases sur les astres sans athmosphères.

## Les nains

Les nains sont une espèce se remarquant pour leur petite taille, et leur fort volume de cheveux et/ou de barbe, les deux poussant bien plus vite que chez les autres humanoïdes.

Les nains résistent mieux que les humains et les elfes aux fortes gravités, et aux conditions sèches et froides en général. Ils sont réputés pour êtres des mineurs, mais c'est en grande partie parce que les deux astres nains sont la Confédération du Nibelung, situé dans la ceinture d'astéroïde, ainsi que l'astre Heorot, tout deux des grandes puissances minières.

## Les elfes

Les elfes sont des humanoïdes se remarquant par leur grande taille, leur peau aux couleur froide (elle passe du blanc au noir d'encre par le gris, sans être dans des teintes beige à marron) et leurs oreilles pointues.

Ils ont une affinité naturelle pour la magie, mais sont sensible aux armes d'Adamant Sombre qui peut facilement les blesser. Ils ont une préférence pour les planètes chaudes et si possible humides. Ils se détachent également des nains et humains par leur choix technologique : ils utilisent en fait généralement plus des biotechnologies que des technologies basée sur l'éléctricité.

Ils se considèrent comme les descendants des *porteurs de lumières*, et chez certaines nobles de certaines communautés elfiques, ils voient les humains comme leurs anciens serviteurs. Ils sont naturellement sensible aux Wyrd, et peuvent avoir une compréhension et maîtrise limitée de celui-ci.

Leurs astres principaux sont *Hludana* et *Galaad*.
