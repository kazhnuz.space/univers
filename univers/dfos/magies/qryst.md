# Les Qrysts

Les Qrysts sont d'immense créature minérale de glace, vivant en majeure partie sur la planète Jotuns. Ces créatures à l’espérance de vie très longue (ils peuvent vivre jusqu’à quelques dizaines de milliers d’année !) existe depuis très longtemps, et a créé des formes de société souvent difficile à comprendre pour les créatures intelligentes organiques.

Tout semble lent chez les Qryst à des êtres vivants vivant moins longtemps, et leur langage même est unique. Leur mode de communication principal est une langue des signes, les Géants de Glace ne pouvant pas parler de manière orale.

Espèce ancienne et sage, leur compréhension du monde et du wyrd est plus grande que la plupars des espèces "mortelles".
