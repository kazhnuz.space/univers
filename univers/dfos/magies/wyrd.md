# Le Wyrd

Le Wyrd, aussi nommé la "grille magique" est une sorte de tissu de connection magique se faisant entre tout les êtres vivants et les astres du système d'Ygg. Source d'énergie puissante et incroyable, elle peut être à la base de la magie, mais aussi de certaines technologies, dans tout le système.

Le Wyrd n'est pas en tant que soi une matière, mais peut en créer en interagissant avec la matière. On nomme alors ça les Adamants

Les Caith et Elfes ont une capacité à utiliser le Wyrd et à le ressentir et les Eirons y sont naturellement connectés. Les d'kraiens et les humains y sont peu connecté, et sont donc en grande partie immunisé à ses effets néfastes (cependant, ils peuvent quand même apprendre à la maitriser en devenant mage).

## L'utilisation du Wyrd

Outre la magie, le Wyrd peut être utilisée à de nombreuse fin, à faire de la divination, des techniques, ou communiquer sur des distances supérieure à ce que permet la vitesse de la lumière.

Le Wyrd est aussi utilisé comme source d'énergie, ou pour former des portails permettant de se déplacer au delà des possibilités physiques. C'est également ce qu'utilise les arches pour transmettre leur pensée.
