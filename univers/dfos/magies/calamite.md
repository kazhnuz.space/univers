# La Calamité

La Calamité est une énergie corrompue dérivée du wyrd, aussi nommée parfois la *corruption* ou la "Grande Peste" de par le caractère épidémique qu'elle peut avoir. Née sur Grimm suite à un incident provoquant la chute de l'ancienne civilisation il y a de ça des millénaires, la calamité prend la forme d'une forme d'énergie liquide pouvant naitre lorsque le Wyrd s'emballe.

Elle "fuite" du Wyrd et provoque de gros dégats dans les environnements naturel et chez les êtres vivants. Elle est en grande partie ce qui rend le voyage spatiale très dangereux dans Ygg.

La calimité a des effets mutagène important, et est extrèmement dangereux.

## Porteurs de désastres

Les porteurs de désastres sont des créatures puissantes, affectés voir créés par la calamités. Ils ont la particularités de pouvoir voler dans le vide (comme les wyverns et les dragons) et d'être infatigable.

## Culte de la calamité

Le culte d'A'tolk est un culte de la calamité, utilisant les forces magiques unique née de cette susbtance dangereuse.

Composé en grande partie de mage, ils pratiquent une magie corrompue, parlant souvent avec véhémance du "potentiel" d'une telle magie. Les hautes sphere du culte cependant pratique peu cette magie, et est composé surtout de nobles et évèques voulant renforcer leur puissance grâce à l'utilisation de ce culte.

## A'tolk, le dragon corrompu

A'tolk est un dragons ancien et puissant, contaminé par la calamité. Il s'agit d'un dragon qui a été transformé par la corruption lorsque l'incident de Grimm s'est produit.

Beaucoup de rumeurs parlent de lui, mais il n'a jamais été vu.
