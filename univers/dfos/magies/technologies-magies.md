# La technologie et la magie

L'univers d'Ygg est un univers ou coexiste technologie et magie de manière importante. Le but d'Ygg est de mélanger les éléments d'un univers de high-fantasy un peu fun avec des elfes et dragons, avec des éléments de space-opéra, du coup technologie et magie y coexiste.

Il n'y a pas vraiment dans ce monde de "combat" entre les deux, ce sont vue comme deux choses complémentaire.

## Les différentes technologies

L'univers d'Ygg possède trois grandes technologies différentes :

- Les humains et nains utilisent grandement des technologies basé sur l'éléctronique ou la mécanique.

- Les elfes et les caith utilisent grandement des biotechnologies.

- Les d'kraiens et les lycions utilisent grandement des technologies cristalines, remontant à avant la grande catastrophe.

## La magie

Deux grandes magies existent dans l'univers d'Ygg :

- La magie de la lumière, magie grandement utilisée pour la guérison, la purification et la protection.

- La magie des ombres, magie grandement utilisée pour le combat, la discretion et la recherche des secrets.

## Les reliques élémentaires

Les reliques élémentaires sont des sortes de disques d'adamant, qui offrent un pouvoir élémentaire considérable à la personne les utilisant, tout en étant assez dangereux pour la santé si utilisé trop longtemps. On dit qu'ils offrent le pouvoir de maîtriser dans une certaine mesure l'élément lui-même, au point ou leur utilisation pouvait renverser des guerres entières.

Ils sont tous gravé du symbole de l'élément, et sont dit venir des dieux eux-même (d'où leur nom de relique).

Les reliques sont au nombre de 16, une par élément, et sont les suivantes :

|  | Guérison | Combat | Protection | Malice |
|:-|:--------:|:------:|:----------:|:------:|
| **Guérison** | Lumière | Néant | Ordre | Air |
| **Combat** | Energie | Foudre | Metal | Glace |
| **Protection** | Eau | Feu | Terre | Espace |
| **Malice** | Nature | Ombre | Temps | Chaos |
