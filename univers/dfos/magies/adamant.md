# L'adamant

L'adamant est la matière formée par l'intéraction d'énergie venant du Wyrd avec la matière ordinaire.

Les atomes composant la matière sont transformer, devenant une matière en dehors de tout cce que connait et peut reconnaitre la science. L'étude de l'adamant est fait par de nombreux mages, scientifiques et experts en matériaux magiques, et sa maitrise est considéré comme d'une importance stratégique capitale.

Les adamants sont de formes et substences variées, mais ont toujours des aspects magiques soit appréciable… soit moins appréciable.

## Les types d'adamants

Les différents types d'adamant sont noté par couleurs, et se forment par les différentes matières qui le compose.

Les plus célèbre sont les adamants suivant:
- L'Adamant bleu est liquide, et est une source d'énergie très potente.
- L'Adamant Sombre est un adamant très légèrement corrompu, formant des armes particulièrement puissante contre les êtres magique. Il est plus crystallin que metallique.
- L'Adamant d'Or est un métal efficace et ayant la particularité de repousser la magie, d'y être hyperméable.
- L'Adamant d'Argent est un métal aux propriété exceptionnel, pouvant devenir extrèmement solide et puissant. Il est grandement utilisé pour former des armes et armures, mais aussi des appareils divers.

## Adamant corrompu

Une forme d'Adamant, nommé *adamant corrompu* nait de l'interaction entre la corruption et la matière ordinaire. Il change constamment de couleur et est particulièrement instable mais énergétique.

Il est la source des *bombes corruptrices*, l'une des armes antique les plus dangereuses existant dans Ygg.
