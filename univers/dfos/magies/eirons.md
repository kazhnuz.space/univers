# Les Eirons

Les eirons sont une espèce mystérieuse, fortement magique et presque mystique vivant dans les zones vivable de Edda et sous les mer de Nehalennia. Iels ressemblent à des sortes "d'oiseau-poissons", constitué d'un long corps armé de 6 "bras-ailes-nageoires".

Les eirons ont une espérence de vie infinie, sauf accident.

Cette espèce peut flotter dans les airs ou dans les mers, cette espèce à un mode de déplacement grandement différent des autres espèces, et sont même capable d'effectuer des déplacements parfaits, fluide et d'ignorer les obstacles. Iels sont capable également d'effectuer des cris stridents. Mais leur plus grand pouvoir est leur connection au Wyrd, les rendants lié à tout ce qui est magique, mais *extrèmement* sensible à l'adamant sombre.

## Les Aesirs

Les aesirs sont les "dieux" protecteurs du système d'Ygg, et des êtres puissants, ayant une maitrise totale du Wyrd. Iels n'ont cependant aucun pouvoir de commandement sur les sociétés Eirons, les protégeants juste de loin.

Les huit Aesirs sont les suivants:

- **Sigel** est l'aesir de la lumière. Guérisseur et doux, iel est avant tout un⋅e soigneur⋅euse et un guérisseur⋅euse.

- **Feh** est l'aesir de l'énergie. Épris⋅e de marchandage, toute sa vie est fondé sur le principe d'échange, de transfert.

- **Peorth** est l'aesir du néant. Selon la légende, iel aurait plongé dans le néant entre les mondes et vu les horreurs qui s'y trouve.

- **Yr** est l'aesir des ombres. Voleur⋅euse et tricheur⋅euse, iel est cependant le protecteur de tout⋅e⋅s celleux qui sont en marge de la société.

- **Ethel** est l'aesir de l'ordre. Iel est droit⋅e et vertueux⋅se, et n'accepte aucun écart moral.

- **Gēr** est l'aesir du temps. Mystérieux⋅se et peu bavard⋅e. Iel est cellui qui cache les secrets mais diffuse l'histoire.

- **Rād** est l'aesir de l'espace. Explorateur⋅ice, voyageur⋅se et scientifique, iel a pour but de comprendre le plus possible l'univers et ses secrets.

- **Wynn** est l'aesir du chaos. Espiègle et joueur⋅se, iel joue des tours à celleux qui tentent de la défier, et qu'iel l'inventeur⋅ice d'une grande partie des jeux anciens
