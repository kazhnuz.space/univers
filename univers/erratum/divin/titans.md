# Les titans

Les titans, aussi nommé dieux, sont des êtres tellement imprégné par la puissance de l'éclat qu'ils en ont doté une maitrise pure. Ils sont parmi les formes de vie les plus puissantes de l'univers.

C'est à dire en plus d'avoir la capacité d'utiliser l'éclat directement à volonté et quand ils veulent (sans même avoir à esquisser le moindre mouvement), ils peuvent l'utiliser de manière bien plus poussée et ont des réserve d'éclat qui s'auto-alimentent. On comprend ainsi pourquoi ils sont souvent considéré comme des dieux.

Les titans vivent généralement entre eux, dans des cités coupés du monde nommés les "cités célèstes". Ils sont coupé du monde depuis la fin de *l'ère des dieux*.

Les titans ont une longévité impressionnante, presque infinie, cependant, ils ne sont qu'en partie immortel, c'est à dire qu'ils ne meurent définitivement que très difficilement. Il leur faut cependant parfois quelques siècles pour pouvoir se reformer complètement après une destruction quasi-complète.

Les titans et leur descendant sur quelques générations possède un gêne unique, le "gêne divin" produisant une protéine ayant peu d'effet sur le corps humain, mais dont la trace a été utilisée comme marqueur des technologies "divines".

Leur principale faiblesse est une très faible capacité à évoluer et à changer, comme la majorité des espèces immortelles.

Les titans sont divisé en trois générations.

## La première génération

La première génération représente uniquement quatre titans, ainsi qu'un être supplémentaire parfois compté dedans.

- **Les quatres grands dieux** (Mabon, Oresta, Lith et Yule) sont les quatres dieux protecteurs des *quatres grandes civilisations* et lié aux quatres saisons. On raconte qu'ils se seraient opposé d'abord à la cité d'Eden, première grande Cité Céleste, puis aux dieux de seconde et troisième génération qui ont peuplé le monde.

- **Lux** aussi nommé *l'arbitre* est un être mystérieux, ayant la forme d'un jeune adolescent humain à l'age difficilement définissable, marchant à travers le noumène. Il est la personne que les gens voient en obtenant un nouveau signe au cours de leur vie, un phénomène nommé *l'apothéose*. Personne, pas même lui, ne sait ce qu'il fait ici. Son status en tant que dieu de première génération est sujet à débat chez les érudits.

## La seconde génération

La seconde génération de dieu est apparu au tout début de l'ère des dieux, dans ce qu'on appellait les "Terres Sauvages". Ils seraient des êtres préhistoriques de toutes espèces ayant subitement obtenu. Certaines légendes racontent qu'un *porteur de lumière* leur aurait apporté leur pouvoir. Extrèmement puissant, ils ne peuvent cependant pas évoluer, et son bloqué toujours à la même limitation de pouvoir.

Ils se sont battus contre les quatres grands dieux ET contre les dieux de troisième génération, et auraient perdu cette guerre. Ils sont aujourd'hui extrèmement rare parmi les dieux, supplanté par les dieux de troisième génération. Cependant, la situation est quelque peux plus compliqué que ça : nombreux sont les panthéons de dieux composé de dieu de troisième génération ayant été créé par un dieu de deuxième génération.

Parmis les dieux célèbres de deuxième génération, il y a le Roi Divin du Temps Akash, et le Grand-Canin, dieu des loup-garous. D'autres dieux comme Ouranos, Kronos, Odin, Viracocha... sont des dieux de deuxième génération regnant ou ayant regné sur des dieux de troisième.

## La troisième génération

Les titans de troisième génération sont assez différents des dieux de seconde en ce qu'ils sont plus proches d'esprits divin et tirent leur puissance de l'influence qu'ils ont sur les autres. En effet, ils ont le pouvoir de convertir le prestige en éclat, et sont particulièrement inspiré par deux sentiments : le respect et la confiance.

S'ils sont au départ plus faibles, ils ont rapidement pris le dessus sur les dieux de deuxième génération grace à la force de leur prestige. La prise de pouvoir de Zeus lors de la Révolution Olympienne est l'un des éléments les plus probant de la prise d'importance des dieux de troisièmes générations.

Ces dieux sont cependant ceux qui ont vécu le plus de plein fouet la fin de l'ère des dieux, évenement terribles après lequel les titans ont décidé de moins influencer directement sur le monde des mortels, mais plus à l'aide des *héros*.
