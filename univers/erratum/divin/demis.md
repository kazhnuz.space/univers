# Demi-titans

À côté des titans, il existe plusieurs espèce de "non-titans". Les trois espèces naissant naturellement sont les *Héros*, les *Anges* et les *Diablons*

Les Héros sont des hybrides de dieux, tandis que les Anges et les diablons sont les serviteurs des dieux.

## Demi-dieu / Héros

Les demi-dieux sont des êtres issues de l'union d'un⋅e dieu/déesse et d'un.e mortel. Ces êtres ont des pouvoirs d'éclat très puissant, mais restent des membres de leur espèce, avec toute les particularité que cela entend.

Cependant, cela à quand même des effets assez nombreux : lors longévité est bien supérieurs à celle de leur espèce, pouvant dépasser les plusieurs siècles ! Ils étaient relativement encore nombreux durant *l'Ère des Héros*, la dernière ère ou leur infleunce était encore forte sur terre.

Parmi les célèbres demi-dieux, il y a Héraclès, Merlin,

## Ange

Les anges sont des êtres créé de toute pièces par les dieux (ou titans). On raconte que leur apparence d'être humains assexués dotés d'ailes de plumes seraient une moquerie envers les anostiens. D'autres légendes raconteraient qu'ils seraient des anostiens "séparé en deux principes différents". Toujours est-il que le lien entre Ange et Anostiens a souvent été fait.

Les anges sont des serviteurs naturels des dieux, créé artificiellement par ceux-ci afin de les servir, et pas spécialement pour combattre.

### La république des anges

La république des anges est une rébellion d'ange du Moyen Orient, s'étant formé suite à la "Révélation", lorsque qu'un ange à provoqué une révolte des anges de la cité celeste ou il travaillait, après avoir préché que les "Titans" n'était pas des vrai dieu, et qu'un Dieu ne pouvait être que unique, omnipotant, omniscient et omniprésent. Il est le premier à avoir émis la différence entre des Dieux qui seraient *vraiment* transcendant, et les titans qui ne seraient que des "être surpuissant".

Sa rébellion est devenu de plus en plus forte, et à réussi à conquérir et capturer plusieurs cité célèste de la région. La république des anges sont les protecteurs de toute les religions du livre, avec qui ils partagent leur croyances. Leur religion est un théisme, et ils ont même provoque la conversion de plusieurs titans.

C'est désormais une république divine puissance, que les dieux ne cherchent plus à combattre. La république est une aristocratie, ou le pouvoir reviens aux anges supérieurs, les "plus proche de Dieux". Ils sont divisé en neuf "choeur", qui sont les différents rôles que peuvent avoir les
- La *première hiérarchie* représente la caste religieuse et spirituelle de la République des Anges. Il s'agit des Séraphim, des Chérubins et des Thrones, dont le rôle est de contempler le divin, et d'établir les grandes lignes des croyances des anges.
- La *seconde hiérarchie* représente la caste militaire des anges. Contrairement à ce qu'on pourrait croire, ceux-ci n'affrontent généralement pas les humains, participant surtout à défendre la cité célèste contre les autres dieux. Ils sont composé des Dominations, Vertueux et Puissances.
- La *troisième hiérarchie* contient la majorité des anges, et représentent la caste "civile" et messagère des anges. Elle est constitué des Principautés (dirigeant et gestionnaire), des Archanges (grand messagers) et des Anges.

## Diablon

Les diablons sont le pendant guerrier des anges. Ces êtres rouges assexués, dôtés de cornes et d'une queue très similaires à celles des anostiens, sont également des êtres créé par les dieux pour les servir, mais cette fois dans le domaine guerrier.

Les diablons forment le gros de l'armée des dieux, et leur obéïssent et son prêt à combattre jusqu'à la mort pour leur dieu. Ils sont puissant et dangereux.
