# Arcane de la Malice

*La malice* représente le pouvoir de manipuler, de changer, de transformer. Elle est souvent l'arcane apportant le plus de surprise, bonne comme mauvaise, de par son pouvoir de changement.

Ses signes sont les gémeaux, le capricorne et le poisson.

## Gémeaux (l'ubiquité)

Les gémeaux sont à la fois un seul être et plusieurs, un corps et un esprit dédoublé. Ce que cela donne en pratique est très différent des personnes : parfois les gémeaux seront deux personnes très différentes, avec des gouts et des visions du monde différentes, mais lié pour la vie. Parfois, il s'agit d'une seule personne, vivant avec deux corps et devant faire avec (avec plus ou moins de contrôle simultané des deux). Parfois, il s'agit d'une parmie toutes les possibilités entre ces deux extrèmes.

Le dédoublement provoque cependant toujours un effet : l'esprit étant étendu entre deux corps (que ce soit sous la forme d'un ou deux esprits), il a le pouvoir de s'étendre encore plus sur tout ce qui entoure, et d'affecter tout ce qui entoure.

Leur hérault est Xolotl, frêre jumeau du dieu Quetzacoatl.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :gemini: | ??? (asie), ??? (égypte) | Malice et Guérison (Trickster Healer) | Chaos | Commun (15.0%) | VOL et INT | Indigo |

## Le capricorne (les émotions)

Les capricornes tirent leur pouvoir des émotions et de leur puissance. En effet, ils sont naturellement capable de capter et modifier les flux émotionnels qui traversent l'esprit. S'ils ne sont pas télépathe, ils sont naturellement empathes.

Leur pouvoir les permet de se sortir de bien des situations, mais aussi d'être les plus à même à attaquer sur l'esprit. Politiciens, espions... Mais ils peuvent aussi être des aides quand ils le veulent. Telle est toute l'ambivalence de ceux qui contrôlent les émotions.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :capricorn: | ??? (asie), ??? (égypte) | Malice et Force (Trickster DPS) | Chaos | Peu commun (10.0%) | REL et CHA | Orange |

## Poisson (les illusions)

Les poissons tirent leur pouvoir des apparences, des mystères et du spectacle. Ils peuvent être n'importe qui, n'importe quoi. Aucune personnalité ne défini le poisson, aucune règle. Après tout, ce que vous voyez n'est-il pas qu'une illusion ?

Leur pouvoir est celui des illusions. Ils se protègent en n'étant pas présent là où vous attaquez. Ils vous font voir des choses fausses... Mais est-ce forcément offensif ? Ne peuvent-ils pas aussi être les aêdes dont les récit peuples votre imaginaire ?

Leur hérault est un⋅e parfait⋅e inconnu. On raconte que son temple est entièrement une illusion, disponible partout où on recherchera les illusions et les secrets.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :pisces: | Singe (asie), Seth (égypte) | Malice et Protection (Trickster Tank) | Chaos | Rare (1.9%) | DIS | Pourpre |
