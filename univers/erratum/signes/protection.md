# Arcane de la Protection

*La protection* représente le pouvoir de défendre son entourage des menace, de garantir la sécurité et le bien-être de tous. Cela peut être une protection physique, mentale, ou métaphorique (tel qu'une protection contre les maux).

Ses signes sont les bélier, le taureau et le verseau.

## Bélier (le groupe)

Les béliers tirent leur pouvoir des autres, et des responsabilités qu'iels ont envers leur entourage. Avec la capacités de protéger les autres et de se placer en travers de ce qui pourrait les atteindre, les Béliers ont le pouvoir de se sacrifier eux pour les autres.

Chef ou protecteur de l'ombre, ils sont des gardiens du troupeaux, ceux qui peuvent faire en sorte que c'est tous qui réussissent, et pas juste certains. Leurs pouvoir les aide aussi à bien se rendre compte de la situation d'un groupe, et à diriger un conflit.

Leur hérault est la reine Tasi Hangbé, fondatrice du corps des « Amazones » dans le royaume de Dahomey en 1711.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :aries: | ??? (asie), ??? (égypte) | Protection et Guérison (Tank Soigneur) | Ordre | Peu commun (9.5%) | CON et REL | Or |

## Taureau (le corps)

Les taureaux tirent leur pouvoir de leur propre corps. Ils sont capable de l'utiliser pour se défendre où pour combattre, tirant partie de tout ce qui le compose. Ils ont le pouvoir de tirer le maximum de ce que leur corps permet.

Combattants émérites, ils sont souvent aussi bien capable de protéger que d'attaquer. Signe le plus commun, mais en aucun cas le plus faible.

Leur hérault est *Shaka*, fondateur du royaume Zoulou.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :taurus: | Boeuf (asie), ??? (égypte) | Protection et Force (Tank DPS) | Ordre | Commun (22.0%) | FOR et CON | Marron |

## Le verseau (le temps)

Les verseaux tirent leur pouvoir du temps. Leur signe leur confère une affinité naturelle avec la puissance naturelle du temps qui passe, leur offrant un pouvoir minime d'influence de celui-ci. Cependant, même une capacité d'influence minime du temps est un pouvoir immense.

Ils peuvent modifier les évenements, effacer une action, agir avant que les conséquences de quelques chose se produise... Cependant, le coup de ces pouvoirs est souvent haut.

Leur hérault est le savant *Albert Einstein*, ayant découvert de nombreuses propriétés de l'espace-temps.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :aquarius: | Dragon (asie), ??? (égypte) | Protection et Malice (Tank Trickster) | Chaos | Rare (00.7%) | INT et SAG | Bleu |
