# Arcane de la Guérison

*La force* représente la puissance, l'attaque, la capacité brute d'action. Cette arcane est souvent dangereuse, mais n'est pas forcément qu'une arme : un couteau peut servir à attaquer, mais aussi d'outil.

Ses signes sont le scorpion, le lion et le saggitaire.


## Scorpion (le poison)

Les scorpions tirent leurs pouvoirs des poisons. Ils deviennent capables d'en sécreter naturellement et de les injecter aux autres personnes, pour pouvoir obtenir différents effets.

Ils sont aussi bien médecins comme attaquants ; ne dit-on pas que les médecines ne sont que des poisons bien utilisés ?

Son herault est Baba Yaga, fondatrice des sorcières de l'est, vers les années -1200.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :scorpius: | Serpent (asie), ??? (égypte) | Force et Guérison (DPS Soigneur) | Chaos | Commun (15.0%) | HAB | Violet sombre |

## Lion (l'héroïsme)

Les lions tirent leur pouvoir de l'héroïsme et du courage. Héros aux armes légendaires uniques forgée directement de leur âme, ils sont voués à occire le mal et à protéger l'innocent.

Ils sont plus fort quand ils sont en danger, et leur pouvoirs permettent de se battre plus facilement contre des ennemis et de faire des duels.

Leur herault est Gilgamesh, le roi-héros, aux exploits légendaires qui ont transcendé l'histoire et les légendes, vers -3000.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :leo: | Tigre (asie), ??? (égypte) | Force et Protection (DPS Tank) | Ordre | Rare (02.2%) | CHA, FOR et VOL | Rouge |

## Sagittaire (le mouvement)

Les sagittaire tirent leur pouvoir du mouvement et de la vitesse. Capable d'effectuer des actions particulièrement rapide, ils sont capable de manipuler la vélocité même comme une arme, prenant souvent la forme d'un arc (mais pouvant avoir une apparence libre).

Rapide, précis, cette maitrise les rendent généralement cependant plutôt fragile et ils privilégie un grand nombre d'action de faible puissance comparé à une seule action extrèmement forte.

Leur hérault est Jinmu, fondateur légendaire du japon, célèbre pour son arc divin.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :sagittarius: | ??? (asie), ??? (égypte) | Force et Malice (DPS Trickster) | Ordre | Commun (12.0%) | HAB et PER | Turquoise |
