# Cas particuliers

Si les règles des signes sont suivie par la pluaprs des gens possédant un signe, il existe quelques particularités qui peuvent transformer la manière dont un signe agis.

## Signe anomique

Un signe anomique est un signe qui a été atteint par l'anomie, une variante corruptrice et dangereuse de l'éclat. Un signe anomique ne fonctionnera pas bien, et provoquera des effets secondaire à chaque utilisation. Il peut aussi répendre l'anomie chez d'autre personne.

## Signe corrompue

Un signe corrompu est une variante d'un signe, qui possède des effets différents de celui du signe normal. Les effets peuvent être inversé, ou peuvent être modifié pour une variation du pouvoir du signe. Les signes corrompu sont extrèmement rare, et peuvent être aussi bien positifs que négatifs que pour le détenteur du signe, contrairement au signe anomique qui ne peut être que négatif.

Les signes corrompu se remarquant le plus sont les *gémeaux corrompu*, puisqu'ils produisent souvent des modifications de l'apparition du gémeau, qui peut par exemple apparaitre sous forme spirituelle au lieu d'apparaitre sous une apparence charnelle.

## Le serpentaire

Le serpentaire est le 13 signes, le signe inconnu, celui qui ne devrait pas exister. Il est le pouvoir de la vertu du serpentaire. On raconte que certains tenteraient de le synthétiser.

Le serpentaire n'est en fait pas un signe. Il est un méta-signe. Il permet de contrôler les flux même de la magie.

Le serpentaire n'a pas de hérault.

| Symb | Autre nom | Arcanes | Alignement | Rareté | Statistiques privilégié | Couleur |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
| :ophiuchus: | ??? (asie), ??? (égypte) | Hors du système | Éclat | Exceptionnel | SAG et VOL | Toutes/blanc. |
