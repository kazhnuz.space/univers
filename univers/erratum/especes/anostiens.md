## Anostiens

| Classification | Masses de populations | Reproduction | Protecteur | Espérance de vie |
|:--:|:--:|:--:|:--:|:--:|
|  4 Grand Peuple / Magie pure | Structures sociales rigides | Nombre fixe (Assexués) | Yule | Incalculable |

Les anostiens étaient le quatrième des quatre grand peuple, vivant sur les Terres de Lémurie, aujourd'hui connu sous le nom de plateau des Kerguelen, partagé entre la Neustrie et l'Australie. Êtres bleu.e.s assexué.e.s, à la queue de "démon" et avec des cornes extrèmement sensibles (pouvant notamment connaître la pression de l'air et tout) ainsi que d'ailes d'éclat pur, les Anostiens sont auréolés de mystères.

En effet, iels avaient la particularité d'exister en nombre fixe : à la disparition d'un Anostiens, un autre. Le temps ne fonctionnait pas de la même manière, leur espérance de vie ayant un caractère pouvant sembler aléatoire pour les autres peuples. De plus, si leur vieillissement fonctionne avec les mêmes phase que les autres peuples, la durée semble erratique.

Les anostiens ont disparu il y a 10 000 ans, environs en même temps que se sont effondré les civilisations antiques.
