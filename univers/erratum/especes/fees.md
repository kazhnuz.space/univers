## Danaïte et petit peuple

Les fées sont des créatures humanoïdes magiques, vivants principalement dans les territoires naturels, en grande parties les forêts et les jungles mais également en montagne, dans les oasis, dans les steppes… Elles sont présentent un peu partout dans le monde, et leurs cultures sont tout autant varié que les humains.

Elles sont généralement divisée en deux grand groupe, les Danaïtes, ou "grandes fées", et le "petit peuple" composé de tout les êtres de la nature de petites tailles vivant caché dans les territoires naturels (tel que les elfes, lutins, farfadet, scintillante)…

Malgré leur apparence presque humaine, ces êtres sont plus… des végétaux. En plus de leurs caractères sexuels "humains" et de leur genre social, les fées ont également une sexuation "végétale", pouvant soit être pistil, soit androcée, soit "sans fleur" (50% de la plupars des populations fée). C'est pour ça que certaines population semblant être uniquement d'un sexe (les lutin ou scintillante) peuvent sans soucis se reproduire.

Leur reproduction se produit alors par butination des fleures pistiles par un.e fée androcée, ce qui produit des graines de danaïtes, formant ensuite une grande fleur d'où naitra l'enfant fée.

Les fées et le petit peuple ne sont pas interfertiles.

## Danaïtes

Les Danaïte sont un autre des quatres grands autres peuples, surnommé le *peuple du printemps*. La légende des Danaïtes racontent qu'elles ont été forgées à l'image de la déesse Oresta, protectrice du continent de Mu. Leur corps aurait été fondé à l'imge des *quatres premiers dieux*, ce qui expliqueraient le fait qu'iels soient sexué.e.s sans avoir besoin de cela pour la reproduction, de même pour le petit peuple..

Les Danaïte ont des ailes de papillons (et des petites antennes), elles ne sont ni mammifère, ni insecte ! Comme la plupars des fées, leurs chevelures ont une plus grande gamme de couleur que les humains, pouvant prendre des teintes vives tel que le rouge pur, le bleu, le vert, etc… Leur espérence de vie est plus longue que celles des humains, pouvant aller à 110 pour les danaïtes "ordinaires" voir 150 pour les danaïtes fées.

Les danaïte forment un système de la ruche. En effet, les danaïtes forment une sorte de lien mentale entre elles, leur permettant de partager des informations ou des sentiments, appellée "ruche". Cette ruche est assez importante pour la stabilité des danaïtes les plus fragiles, même si iels peuvent vivre sans. La ruche existe sous forme d'une orbe magique, et pour fonctionner, un.e danaite pistile prend le role de "Reine", devenant le liant de la ruche. Leur organisation est très liée à cela de ce fait, les enfants sont généralement élevée en communauté, et les danaïte formant des sociétés fondée sur un rapport important à la collectivité.

Contrairement à ce qu'on pourrait croire, toutes les ruches ne sont pas forcément des monarchies : De nombreuses ruches ont adopté d'autres systèmes, et la "reine" danaïte n'est pas forcément une "reine" politique. En effet, une reine danaïte est souvent plus une protectrice, ou un ciment social

Les danaïtes sont souvent vu comme un peuple fier et isolationiste dans la littérature, mais en vérité c'est surtout une vision occidentale lié à la *Guerre Larvée*, les danaïtes comme les fées pouvant être très différentes suivant les régions. En effet, en Europe, les relations entre les humains et les fées étaient parfois très complexe à cause de la déforestation et du contrôle du territoire. Des bébés humains (toujours de famille noble ou puissante) étaient enlevé pour être élevé par les danaïte et devenir des *enfants des forets* tandis qu'ils étaient remplacé par des danaïtes sans-fleur, nommé dans ce genre de contexte *changeling*. Cependant, à côté de cela, de nombreuses danaïtes étaient engagée comme préceptrice des enfants, selon la tradition des *marraines fées*.

Ce phénomène peut être notamment vu en amériques, ou cette vision des danaïtes est en grande partie importée de l'europe.

## Petit peuple

Le petit peuple est le nom donnée à toutes les fées de petites tailles. Ces êtres sont fortement lié aux danaïtes ou "grande fée", puisque le petit peuple est généralement 10 à 15 fois plus nombreux autour des ruches danaïtes, et ils sont affecté par les danaïtes, en particulier les reines. Si ce liens à longtemps été comparé à un lien de suzeraineté par les humains, dans les faits il est plus complexe : le petit peuple ne doit aucune obéissance aux danaïtes, mais les deux s'entraident, ayant besoin l'un de l'autre.

Le petit peuple peut avoir des apparences et des particularités diverses, suivants les lieux.

Les plus connus sont le petit peuple des forêts tels que les scintillante (aussi appelée "petite fée") ou les lutins. Souvent des petites tailles, excellent pour se cacher et être discrets, ces petites êtres sont des experts de la survie en forêt, parfois de ce fait un peu cruels. Les peuples des plaines (tels qu'en terre celtique les korrigans et les farfadets) sont plus farceurs, vive en petite colonies et sont connus pour piéger les humains pour leur amusement. Les peuples des montagnes (tels que les gnomes ou les nains) vivent souvent dans de grande galeries et exploitent des filons de minerais, dont certains qui ne leur sont visible que d'eux.
