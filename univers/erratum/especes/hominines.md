## Hominine

Les homininé, ou homininiens sont une sous-tribu d'hominidés qui inclut le genre Homo et les genres apparentés, tels que les éteints Australopithèques ou les Paranthropes ou d'autres créatures dérivées du genre Homo. Ces derniers sont les seules créatures autres que les humains existant encore, depuis l'extinctions du reste du genre Homo hors des Homo Sapiens.

Les homininé sont des créatures biologiques, de sang, et se reproduisent de manière sexuées. Ils ont une capacités faibles à produire de la magie, ne possédant aucune magie "naturelle" propre à leur espèce, et dont seul quelques pourcent peuvent pratiquer la magie (dont 0,7% de la magie signée).

Tout les homininés sont interfertiles.

## Humains

Les humains sont l'espèce la plus commune sur terre, de loin, au point où l'être humain est considéré comme l'espèce dominante, et la population la plus puissante politiquement.

Cet animal ayant évolué de la branche des homininé est à l'origine un chasseur cueilleur dont l'inventivité et la capacité d'adaptation à aidé à évoluer. Il a aussi la particularité d'avoir peu de faiblesses magiques, et d'être très polyvalent.

Considéré comme l'une des *quatre grandes espèces*, descendant de l'Atlantide, les humains sont aussi appellé le *peuple de l'automne* et *la grande espèce guerrière* (titre partagé avec les dragons). Depuis la révolution industrielles, ils sont considéré aussi comme les peuples de la *technique*, utilisant des technologies à base d'assemblement et de construction.

## Lycanthropes

Les lycanthropes (ou loup-garou) sont une espèce dérivée de l'espèce humaine. D'après les légendes, ils seraient les descendant des suiveurs du *Dieu-Loup*, maudits à être mi-humains mi-loup, il y a de cela plus de 10 000 ans. Ils sont grand, fort, et généralement poilu.

Espèce sociale vivant en meute, deux types de loups garous spéciaux existent : les Régal (qui ont un pouvoir de contrôle sur les autres), et les libres (qui sont des loups qui ont un pouvoir de neutralité, d'être des arbitres). Un dernier type de loup-garou, les sans-meutes, est très rare, et correspond à des loup-garou qui sont entièrement hors du système même de meutes.

Les loup-garou ne se transforment pas à la pleine lune, mais les meutes ont souvent eut l'habitude de mener de grande chasse et de grands combats vers cette période de l'année. Une grande partie des légendes autour du loup. Dans les mythes loup-garou, le pouvoir du Dieu-Loup serait le plus présent à cette période de l'année.

Malgré leur force et leur compétence physique, suite à la disparition d'une partie de leurs territoires naturels, les loup-garou vivent une crise, leur rendant de plus en plus difficile d'élever leurs enfants. L'espérence de vie est aussi plus faible chez les loup-garou que chez les humains, se rapprochant plus des 65 ans. Cependant, cela est affecté par un plus grande nombreux de loup garou mourant jeunes.

## Hybrides

Une espèce créé artificiellement par les alchimistes lors des 15e et 16e siècles. Ces créatures sont quasiment à 100% humaines, mais avec cependant des "gènes magiques" (noms modernes, avant ils étaient appelé "essences diluées d'espèce") venant d'animaux.

Ils ont été créé afin d'éviter la maltraitance d'autre humains, mais c'était sans compter l'incroyable capacité qui peut exister à maltraiter son prochain : ils ont juste eux plus d'être à maltraiter.

Les hybrides ont depuis gagnés la liberté, notamment grace à des révoltes tels que la *commune de Lutecia* en France, mais sont souvent mal vu dans les populations.

## Ondine

Les ondines sont des humains s'étant rapproché du poisson par convergence évolutive - sans doute amplifiée magiquement par l'Océan lui-même. Ce peuple des mers, suiveuses de l'Océan, se divisent grandement entre les ondines nomades et sédentaires

Les ondines sédentaires vivent dans de grandes cités sous-marines, et s'occupent peu des affaires de la surfaces, tandis que les ondines nomades forment des tribus arpentant les océans.

Elles sont amphibie, capable de résister sous l'eau et étonnamment immunisée à la pression de l'eau. Cependant, comme les loup-garou, elles ont une espérence de vie plus faible que les êtres humains, se rapprochant des 65 ans.

Longtemps considérée comme étant à la limite entre "l'humanité" (au sens large) et l'animalité, elle n'ont vraiment des liens diplomatiques avec les états humains que depuis la révélation du secret magique.

> [!TIP]
> Chez les ondines, on genre les groupes au féminin au pluriel si y'a une femme ou plus.
