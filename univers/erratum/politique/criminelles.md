# Organisation criminelle

## Le schisme

Le Schisme est un groupe qui s'est séparé des cultistes et de frontière suite à la guerre des vertues, parce qu'ils estimaient qu'il faudrait être plus radical dans l'utilisation de la magie et avaient trop peur d'utiliser les forces les plus dangereuses (mais énergétique) tel que le Néant et l'Anomie.

Peu est connu cependant du schisme : il s'agit d'un groupe extrèmement secret, et dont bien des membres ont été corrompus par les énergies qu'ils utilisent.

Les quelques éléments les plus connus de leurs actions sont :

- Ils sont dirigé par un mystérieux homme portant toujours un masque de plâtre.

- Le fait qu'ils aient une forte propention à s'attaquer aux dieux et autre créations

- Des rumeurs disent que leur objectif serait de permettre à toute l'humanité de s'élevé aux rangs de dieux.

Ils sont ennemis à la fois des cultistes et de frontière.

## Les chasseurs

*Les chasseurs* sont un groupe criminelle présent partout à travers le monde. Il s'agit d'un groupe initialement composé de non-mage choqué de la révélation magique, ayant décidé de s'organiser en groupe armée pour se défendre contre « les créatures monstrueuses du monde magique. »

Ce groupe se voit comme les chasseurs de dragons de jadis, et sont persuadés que les créatures magiques risque de finir par faire la guerre aux humains et les exterminer. Ils sont d'autant plus actifs contre les créatures considérées comme « presque humains » tels que les Lycanthropes et les Hybrides.

Ils fonctionnent de manière clandestine, et sont officiellement totalement illégaux et combattus. Leurs noms de codes sont généralement basé sur des grands chasseurs/héros/chevaliers... Aux ex-USA, les noms de grands héros de l'ouest sauvage sont particulièrement appréciés.

## Hermès

*Hermès* est le nom donné à un regroupement d'ancienne mafias, dont le contrôle a été pris par *Mercure Flavius-Claudius*, un des princes de l'Empire Romain.

Cette organisation fait du trafic d'armes magique, de personnes, et de créatures magiques. Ils ont notamment mené plusieurs attaques afin de récupérer des armements puissants, qu'ils proviennent d'organismes comme Frontière ou de dépôts gouvernementaux.

Ils emploient tout particulièrement des membres rapides et discrèts, et sont particulièrement présents en Europe. Ce groupe se fait la guerre avec la Nouvelle Armée Impériale.

## La nouvelle armée impériale.

*La nouvelle armée impériale* est le nom donné à un groupe fascisant pan-européen dirigé par également un ancien prince de l'Empire Romain *Mars Flavius-Claudius*.

Cette organisation à pour objectif de prendre le contrôle de toute l'Europe, pour recréer un Empire Romain tel qu'ils le fantasme, comparé à l'Empire Romain actuel centré sur la partie Ouest de la méditerrannée (Région d'Occitanie, Italie, Catalogne).

Ils apprécient tout particulièrement les membres forts et résistant, et sont particulièrement présents en Europe. Ce groupe se fait la guerre avec Hermès.
