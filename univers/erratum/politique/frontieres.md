# Frontières

Frontière est une organisation mondiale, ayant pour objectif d'utiliser les forces magiques, surnaturelles et l'alchimie afin de faire avancer et évoluer le monde. Leurs objectifs concrets cependant ont changé à travers les ages et les situations, et les orientations politiques du moment. Cette organisation porte de nombreux noms à travers le monde, étant à la base plus un mouvement qui s'est fédéré autour d'une organisation moderne.

Ils sont rivaux avec les cultistes, sans forcément en être toujours des ennemis. C'est surtout depuis les arrivées des chefs Bérénice Flamel et Paracelse que les deux groupes sont devenu relativement ennemis et les membres de frontières se souvenant de l'époque d'avant n'ont pas forcément une vision négative des cultistes, pouvant être vu comme un garde-fou.

Comme les cultistes, le mouvement est né à la fois d'un ancien mouvement de vénérateur de Lux nommé les arbitres, et d'autres mouvements s'y étant greffé.

## Orientation globale

L'objectif de Frontière est de "faire avancer le monde grâce à la magie". Ayant traversé le monde à travers les ages, ce mouvement à parfois tenter de légèrement infléchir les événements en leur faveur, par le financement et la doctrine philosophique. Ils ont été souvent mécène de membre des lumières, et fait ce qu'ils pouvaient pour soutenir les inventeurs. Ils ont souvent également fourni ce qu'ils pouvaient comme force aux "visionnaires", parfois au dépit des populations les subissant.

Cependant, il serait faux de dire qu'ils sont à l'origine des événements. Ils ont généralement plus suivi les courants quand ceux-ci permettraient un changement qu'ils voyaient comme positif.

## Le frontière moderne

Le frontière moderne est le résultat du travail entre Wilfrid von Hohenheim (Paracelse 13), résistant allemand durant la Seconde Guerre Mondiale et Jonathan Battle, ancien vétéran de la première dont les avancée scientifico-magique aidèrent à combattre les nazi durant la seconde. Ceux-ci, déjà membre du mouvement qui deviendra Frontière travailleront ensemble durant 15 ans après la chute de celle-ci pour unifier les mouvements locaux et en faire un organisme international, plus ou moins ouvert et transparent.

Le nom actuel vient de la volonté de dépasser les frontières, et d'être l'entité qui se situe entre le possible et l'impossible. Au delà du possible, il y aurait Frontière, *puis seulement* l'impossible.

Ils ont énormément bénéficié des événements de ces dernières décennies : l'économie mondialisée et les découvertes magiques ont renforcé progressivement leur financement par les différents gouvernements. Cependant, la rivalité Paracelse/Bérénice à eu des effets négatifs, leur provoquant une baisse de réputation.

## Chefs et dirigeants

Une des formes les plus puissantes de frontières ont été le groupe d'alchimiste fondée par *Nicolas Flamel* suite à la Guerre des Vertues. Il laissera cependant sa place à l'aube du XXe siècle. A partir de la fondation, c'est Wilfrid von Hohenheim qui a dirigé jusqu'à sa mort, puis Joseph Battle.

Le chef actuel de frontière est *Paracelse XV*.

### Composants

Frontières possède de nombreux composants à travers le monde, qui ont pour objectif d'améliorer l'utilisation de la magie. Sous généralement le contrôle des organismes internationaux, les principaux financeurs du groupe. Leur organisation est centralisée à New York depuis les années 50, et ils possèdent des centres dans la plupars des capitales du monde.

Chaque section locale possède un certain niveau d'autonomie, mais doit répondre aux ambitions du groupe. Frontière est dirigé par un conseil d'administration, comportant des représentants d'ONG mondiales et des membres du conseil de sécurité de l'ONU.

### L'unité métaphysique

L'unité métaphysique de Frontière, actuellement dirigée par Justine de Villier-Côteret, une jeune scientifique connue pour ses grandes compétences en transmutations alchimique et en métaphysique appliquée, qui a l'autorité totale sur cette section. Elle a été recruté par Paracelse XV lui-même, impressionné par son talent.

Contraiement à Halle Industries, elle se spécialise dans des solutions purement magique, sans une trace de technologie. Elle ne commercialise aucune de ses inventions. Elle est la créatrice des signes artificiels, de l'amplification élementaire, et de nombreuses autres magies extraordinaire de Frontière.

### Halle Industries

Halle Industries, est une entreprise de technologie financé par frontière. Elle produit des homoncules, sorte de robots alchimique de céramique basé sur des technologies atlantes, ainsi que de nombreuses autres technologies et armes utilisées par frontière, souvent basé sur des technologies antiques. Elle est présente dans de nombreux domaines, mais toujours de la technologie de pointe, de l'informatique à l'aerospatiale. Halle Industrie ne commercialise cependant pas des inventions de Justine.

De la part du public, elle reçoit de forte critique à la fois pour sa taille et sa puissance, mais aussi par le fait de n'être pas forcément commercialement viable, profitant fortement des finances de frontières et des gouvernements mondiaux.

Magitech en était une filliale avant son rachat par la BBBB.

### La ligue des Héros

Cette unité d'élite fondée et financée par Frontière a pour objectif de servir de super héros à travers le monde, grace aux améliorations et aux modifications faites par le groupe. Il s'agit d'un des projets de coeur de Joseph Battle puis de son successeur Paracelse XV. Doté de nombreux combattant, dont plusieurs signes artificiels, cette unité d'élité combat à travers le monde. Elle est d
dirigé par *7 héros d'élite*, souvent considéré comme une forme de réponse par Frontière aux vertues.

Cette unité à grandement été créer aux États Unis, lors du projet *Witch Eyes*, faisant partie de la course à la magie entre les deux grandes puissances lors de la guerre froide. De ce fait, une grande partie de ces héros sont américain⋅e⋅s. De nombreux héros ont porté les manteaux de ces héros, et ceux actuels sont des successeurs de ceux originaux.

Parmi les technologies utilisée pour former des Super Héros, il y a *l'amplification élémentaire*, renforçant les pouvoirs d'un élémentariste, mais ne lui permettant de maîtriser un seul élément, les armures de combats, les signes artificiels ou l'Alosun, un sérum apportant des pouvoirs atomique surpuissant inventé par le défunt scientifique Josiah Rhonne. Certain⋅es sont juste très fort⋅e.

> ![NOTE]
> Les héros décrit ci-dessous sont repris/inspiré de comics du Golden Age et sont désormais dans le domaine public. Ils ont régulièrement été réutilisé dans des films nanardesque ou dans des comics inspiré/modernisant cette période.

- **Capitain Battle** (June Battle, Humaine) : Cette super-héroïne profite avant tout de nombreux gadget, mais également d'un entrainement physique exceptionnel, supplémenté par des capacité de Taureau. La Capitain Battle actuelle est la petite-fille de Joseph Battle, lui-même fils de Johnatan Battle, premier détenteur du titre. Ce manteau est de ce fait généralement porté par les Battle.

- **Souvenir** (Gabrielle Urdi, Danaïte) : Titre orignellement porté par Nathan Halle, sidekick de Johnatan Battle, ce titre est désormais utilisé généralement pour désigner le second de l'équipe - que læ chef⋅fe soit un⋅e Battle ou non. Il est actuellement porté par l'illusionniste Gabrielle Urdi, jeune femme afro-américaine maître des esprits. Elle est cependant plus connue comme la stratège de l'équipe.

- **The Flame** (Misha Averianov, Hybride, neutrois) : Combattant⋅e de Frontière, ayant acquis des pouvoir de pyromancie via le programme de super-pouvoir du groupe. De nombreux combattant de Frontière ont endossé ce manteau, Frontière ayant vite maitrisé l'amplification élémentaire. Parmi les prédécesseur célèbre de Misha, il y a Gary Preston, Kip Adams et Kandy Wilson.

- **Lightning** (Erika Kuni, Humaine) : Maitresse de la foudre, elle a pris l'un des deux titre souvent utilisé par les utilisateurs de la foutre, l'autre étant souvent Electro-Man/Woman. Elle porte une combinaison conductrice, qui lui permet de magnétiser ses pouvoirs. Elle peut également voler en utilisant le champ magnétique terrestre.

- **Le Casse-Cou** (Bart Hill, Loup-Garou) : Orphelin australien spécialiste du boomerang et des acrobatie. Son titre n'a pas souvent été distribué. Il aimerait prendre la traduction anglaise de son nom, mais a peur pour des questions de droit d'auteur.

- **Miracle** (Anostiens, fluide homme/neutre) : Miracle est un anostiens ramené à la vie par Frontière. Il a appris petit à petit les langues moderne, et gardait souvent un aspect détaché du reste… puis a été rendu adicte à la culture populaire par ses camarades. Il utilise principalement ses pouvoir d'entropie pour provoquer des réactions en chaine.

- **Atomic Thunderbolt** (Alim Rhode, Hybride souris) : Jeune recrue de frontière ayant pris l'alosun, il a obtenu les incroyable pouvoir d'Atomic Man. Il peut voler, possède une force surhumaine, peut provoquer des explosion ou des vagues de vent. Il est également complètement invulnérable aux radiations. Il utilise le nom d'Atomic Thunderbolt, ayant une préférence pour cela par rapport au nom Man. Il s'entends particulièrement bien avec Erika, et fait partie de sa croisade contre les noms de super héros en -Man/-Woman, qu'il trouve pas ouf.

- **Hercule** (Demi-Dieu, Demi-Cyborg) : Fils de Zeus et d'Alcmène, un héros antique ayant accompli 12 travaux après une série d'embrouille. Il est mort en revetant une tenue enduite du poison mortel de ses flèches. Après être "mort" et avoir rejoint son père, il a eu une engueulade avec, et n'a pu devenir complètement un dieu, et a été condamné à érer et faire des travaux, pris par la douleur de sa peau brulée par le poison. Lors de la fondation de Frontière, il a été soignée par Wilfrid von Honhenhaim et à obtenu des bouts d'armures antique et des greffes pour le rendre plus viable. Malgré toute sa vie, il est un grand blagueur. Il s'auto-surnomme "Cybercule", mais personne ne l'appelle comme ça.

- **Adamant** (Sophie von Hohenheim, Humaine) : Nièce de l'actuel Paracelse XV, cette jeune fille est une spécialiste de la robotique et de l'utilisation de l'éclat comme source d'énergie. Sa mère est une fan de la contesse de Segur et lui a fait une éducation assez rigide. Elle possède plusieurs armures de combat, ainsi qu'un mécha.
