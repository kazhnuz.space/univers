# L’éclat

L'éclat est le nom donnée à la magie, ou plus précisément à la forme pure de la magie tel qu'elle existe naturellement. Se comportement comme un gaz supercritique, ou la différence entre liquide et gazeux est difficilement compréhensible.

Cette particule est particulière, parce qu'elle n'entre pas dans le cadre du modèles standard des particules, ne correspondant à aucune particule connue. Cependant, on pense que le "champ" auquel elle est lié est le champ entropique, ce qui en ferait un boson au même titre que le photon ou le gluon.

## Éclat naturel

L'éclat est extrêmement difficile à trouver à l'état naturel (qui sont créateur d'information). Il est en grande partie produit par les êtres vivants, plantes, êtres. L'éclat est toujours légèrement concentré dans l'ère : plus il y a d'être vivant dans la zone, plus il y a d'éclat.

Il circule également sous forme d'immense courant de magie dans la terre, courants nommées les *courants telluriques*. Quelques grandes sites magiques sont situés dans des *nœuds telluriques*.

Les créatures magiques ont tendance à produire plus d'éclats que les autres.

## Batterie à éclat

Les batteries à éclat sont des batteries capable de générer de l'éclat, servant à "stocker" de l'énergie magique via une réaction réversible permettant à la fois de consommer puis produire de l'énergie magique, à la manière dont une batterie normale "stocke" l'énergie éléctrique via une réaction.
