# Magies

En plus de la magie naturelle des signes, les êtres vivants peuvent apprendre un certains nombres de techniques magiques.

## Magie élémentaire

La magie élémentaire est une magie se basant sur les pouvoirs des grands élémentaires (aussi nommée incarnations élémentaires), qui fourni aux gens la puissance de leurs éléments.

Les utilisateurs font un pacte avec certains nombres d'éléments qui pourront ensuite être utilisé.

| Sources | Rareté |
|:--:|:--:|:--:|
| Grands élémentaires | Commune |

## Arts martiaux

Les arts martiaux sont une magie tirée de la force martiale. La force martiale se concentre dans le corps pour transformer les capacités utilisés.

Les arts martiaux existent en de nombreuses écoles, et peut être apprise avec des maitres martialistes.

| Sources | Rareté | Statistiques privilégié |
|:--:|:--:|:--:|
| La Grande Force Martiale | Peu Commun | FOR |

## Magie créatrice

La magie créatrice consiste à maîtriser une matière nommée le sidérale, composée d'une forme d'éclat solidifié perdant une partie de ses propriétés, qui peut être transformée pour créer l'apparence parfaite de n'importe quelle matière.

C'est une des magies les plus rares et difficile à maitriser.

| Sources | Rareté | Statistiques privilégié |
|:--:|:--:|:--:|
| La puissance créatrice | Rare | INT, HAB |

## Enchantement

Les enchantements sont une magie plutôt rare, consistant à appliquer des effets magiques sur des objets différents, où a modifié les propriétés de la matière.

Cette magie se fonde sur les circuits de magie existant partout, formé par la circulation naturelle de l'éclat dans tout ce qui existe.

| Sources | Rareté | Statistiques privilégié |
|:--:|:--:|:--:|
| Les circuits magiques naturels | Rare | HAB |

## Magie naturelle

La magie naturelle est la magie consistant à utiliser à son avantage les forces de la nature et des animaux. Cette magie est grandement appréciée des gens n'ayant pas forcément de fort pouvoir magique, et peut s'apprendre en autodidacte par un fort contact avec la nature.

En Neustrie, les mages de la nature les plus réputés sont les *druides*.

| Sources | Rareté | Statistiques privilégié |
|:--:|:--:|:--:|
| Mère nature | Peu commun | REL |

## Magie spirituelle / Shamanisme

La magie spirituelle, ou shamanisme, est la magie consistant à utiliser les pouvoirs des spectres, des êtres entièrement mentaux. Magie proche de la magie élémentaire, mais portant sur la magie métaphysique, et de la magie naturelle, on peut imaginer qu'elle s'est formé par un mélange des deux.

| Sources | Rareté | Statistiques privilégié |
|:--:|:--:|:--:|
| Les spectres et esprits | Peu commune | INT, HAB |

## Les invocations

Les invocations sont un type de magie consistant à utiliser son encrage dans le monde physique pour invoquer un démon, une espèce proche des demi-dieux lié au *chaos* possédant un ou plusieurs pouvoirs démoniaques uniques.

| Sources | Rareté | Statistiques privilégié |
|:--:|:--:|:--:|
| Démons | Peu commune | VOL+CHA |

## L’herboristerie

L'herboristerie est sans doute la magie la moins magique. Elle tire son pouvoir de plantes particulières, les plantes élémentaires. Ces plantes sont alors mélangées pour créer des effets spéciaux dans des potions.

| Sources | Rareté | Statistiques privilégié |
|:--:|:--:|:--:|
| Les plantes | Commune | INT, HAB |

## Alchimie

L'Alchimie est la magie visant à obtenir la compréhension du monde tel qu'il est (le noumène) comparé à ce qui nous est apparant et accessible (le phénomène).

Cette magie est extrèmement dangereuse pour l'esprit, parce qu'elle porte dans des choses que notre esprit n'est pas fait pour comprendre.

Les plus célèbre alchimistes sont Nicolas Flamel, et Paracelse Ier.
