# CONTRIBUTING

Like every projects on chlore.net, this project follow the [Contributor Conevant Code of Conduct](https://www.contributor-covenant.org/). You can find a markdown copy of the CoC on this repository.

This project follow the following conventions: [Semantic Versioning 2.O.0](https://semver.org/), [Keep a Changelog](https://keepachangelog.com/en/1.0.0/) and [Conventional Commits](https://www.conventionalcommits.org/).
